import Navigation from "./Settings/router";
import axios from "axios";
import { baseurl } from "./Settings/common";
import { getStoarge, setStoarge } from "./Datas/storage";
import { SWRConfig } from "swr";
import { UserProvider } from "./Datas/storage";
import { useEffect, useCallback } from "react";
import "./Settings/glStyles.css";
import { useState } from "react";

axios.defaults.baseURL = baseurl;
axios.interceptors.request.use(
  async (config) => {
    const jwt = await getStoarge("jwt");

    if (jwt) {
      config.headers.Authorization = `Bearer ${jwt}`;
    }
    return config;
  },
  (err) => Promise.reject(err)
);

const App = () => {
  useEffect(() => {
    document.addEventListener("message", async (post) => {
      let fcm = JSON.parse(post.data).fcmToken;
      if (
        (await setStoarge("fcmToken")) === undefined ||
        setStoarge("fcmToken") !== fcm
      ) {
        await localStorage.setItem(
          "fcmToken",
          JSON.stringify({ fcmToken: fcm })
        );
      }
    });
    window.addEventListener("message", async (post) => {
      let fcm = JSON.parse(post.data).fcmToken;
      if (
        (await setStoarge("fcmToken")) === undefined ||
        setStoarge("fcmToken") !== fcm
      ) {
        await localStorage.setItem(
          "fcmToken",
          JSON.stringify({ fcmToken: fcm })
        );
      }
    });
  });

  /* const [capture, setCapture] = useState(false);
  let prevKey = {
    first: "",
    second: "",
  };

  useEffect(() => {
    const handleEsc = (event) => {
      if (event.key === "Shift" || event.key === "Meta") {
        if (prevKey.first !== "") {
          prevKey.second = event.key;
        } else {
          prevKey.first = event.key;
        }
      } else if (event.key === "4" || event.key === "3") {
        console.log(prevKey);
        if (
          (prevKey.first === "Shift" || prevKey.first === "Meta") &&
          (prevKey.second === "Shift" || prevKey.second === "Meta")
        ) {
          alert("캡쳐가 불가합니다");
          prevKey = {
            first: "",
            second: "",
          };
        }
      }
    };

    window.addEventListener("keydown", handleEsc);

    return () => {
      window.removeEventListener("keydown", handleEsc);
    };
  }, []); */

  return (
    <SWRConfig
      value={{
        fetcher: (path, options) =>
          axios(path, options).then((res) => res.data),
      }}
    >
      <UserProvider>
        <Navigation />
      </UserProvider>
    </SWRConfig>
  );
};

export default App;
