import useSWR from "swr";

const GetBannerList = (offset) => {
  const { data, error, mutate } = useSWR(
    () => `banner/userBannerList/${offset}`
  );

  return {
    BannerListData: data,
    BannerListMutate: mutate,
    BannerListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetStoreList = (offset, search) => {
  const { data, error, mutate } = useSWR(
    () => `user/storeList/${offset}/${search}`
  );

  return {
    StoreListData: data,
    StoreListMutate: mutate,
    StoreListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetPrTelecom = (name, offset) => {
  const { data, error, mutate } = useSWR(
    () => `category/prTelecomList/${name}/${offset}`
  );

  return {
    PrTelecomData: data,
    PrTelecomMutate: mutate,
    PrTelecomError: !data && !mutate,
    isLoading: !data && !error,
  };
};
const GetTelecomList = () => {
  const { data, error, mutate } = useSWR(() => "category/telecomList");
  return {
    TelecomListData: data,
    TelecomListMutate: mutate,
    TelecomListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetTelecomCategoryList = (id, offset) => {
  const { data, error, mutate } = useSWR(
    () => `category/typeTeleComCategryList/${id}/${offset}`
  );
  return {
    TelecomCategoryData: data,
    TelecomCategoryMutate: mutate,
    TelecomCategoryError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetuserProfile = (id) => {
  const { data, error, mutate } = useSWR(() => `user/userProfile/${id}`);

  return {
    UserProfileData: data,
    UserProfileMutate: mutate,
    UserProfileError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetAdminData = () => {
  const { data, error, mutate } = useSWR(() => `user/AdminCheck`);

  return {
    AdminsData: data,
    AdminDataMutate: mutate,
    AdminDataError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetStoreInfo = (id) => {
  const { data, error, mutate } = useSWR(() => `user/storeInfo/${id}`);

  return {
    StoreInfoData: data,
    StoreInfoMutate: mutate,
    StoreInfoError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetCardList = (telecomname, offset) => {
  const { data, error, mutate } = useSWR(
    () => `product/cardList/${telecomname}/${offset}`
  );

  return {
    CardListData: data,
    CardListMutate: mutate,
    CardListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetuserList = (storeid, access, offset, filter, search) => {
  const { data, error, mutate } = useSWR(
    () => `user/userList/${storeid}/${access}/${offset}/${filter}/${search}`
  );

  return {
    userListData: data,
    userListMutate: mutate,
    userListError: !data && !mutate,
  };
};

const GetproductList = (type, promotionid, categoryid, offset, filter) => {
  const { data, error, mutate } = useSWR(
    () =>
      `product/productList/${type}/${promotionid}/${categoryid}/${offset}/${filter}`
  );
  return {
    ProductListData: data,
    ProductListMutate: mutate,
    ProductListError: !data && !mutate,
  };
};

const GetCategoryList = (offset) => {
  const { data, error, mutate } = useSWR(() => `category/listCategory`);

  return {
    CategoryListData: data,
    CategoryListMutate: mutate,
    CategoryListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetPlanList = (telecomname) => {
  const { data, error, mutate } = useSWR(
    () => `product/planList/${telecomname}`
  );

  return {
    PlanListData: data,
    PlanListMutate: mutate,
    PlanListError: !data && !mutate,
    isLoading: !data && !error,
  };
};
const GetPlanListCate = (id, offset) => {
  const { data, error, mutate } = useSWR(
    () => `product/planListCate/${id}/${offset}`
  );

  return {
    PlanListCateData: data,
    PlanListCateMutate: mutate,
    PlanListCateError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetUserOrderList = (id) => {
  const { data, error, mutate } = useSWR(() => `user/userOrderList/${id}`);

  return {
    GetUserOrderListData: data,
    GetUserOrderListMutate: mutate,
    GetUserOrderListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetUsageCate = (id) => {
  const { data, error, mutate } = useSWR(
    () => `category/typeTelecomCateTelList/${id}`
  );

  return {
    GetUsageCateData: data,
    GetUsageCateDataMutate: mutate,
    GetUsageCateDataError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GettypeTelecomListDatas = (type, offset) => {
  const { data, error, mutate } = useSWR(
    () => `category/typeTelecomListDatas/${type}/${offset}`
  );

  return {
    typeTelecomListDatas: data,
    typeTelecomListMutates: mutate,
    typeTelecomListtErrors: !data && !mutate,
    isLoadings: !data && !error,
  };
};
const GetPromotionList = (offset) => {
  const { data, error, mutate } = useSWR(
    () => `category/listPromotion/${offset}`
  );

  return {
    PromotionListData: data,
    PromotionListMutate: mutate,
    PromotionListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetAllOrderList = (offset, type, option, start, end, text, store) => {
  const { data, error, mutate } = useSWR(
    () =>
      `user/allOrderList/${offset}/${type}/${option}/${start}/${end}/${text}/${store}`
  );

  return {
    allOrderListData: data,
    allOrderListMutate: mutate,
    PallOrderListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetInforMation = () => {
  const { data, error, mutate } = useSWR(() => `banner/getInformation`);

  return {
    InfoData: data,
    InfoDataMutate: mutate,
    InfoDataError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetOrderInfoData = (id) => {
  const { data, error, mutate } = useSWR(() => `user/orderInfoData/${id}`);

  return {
    OrderInfoData: data,
    OrderInfoMutate: mutate,
    OrderInfoError: !data && !mutate,
    isLoading: !data && !error,
  };
};
const GetBoardList = (offset) => {
  const { data, error, mutate } = useSWR(() => `user/boardList/${offset}`);

  return {
    BoardListData: data,
    BoardListMutate: mutate,
    BoardListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetBoardrecList = (offset) => {
  const { data, error, mutate } = useSWR(() => `banner/listBoardrec/${offset}`);

  return {
    BoardListrecData: data,
    BoardListrecMutate: mutate,
    BoardListrecError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetTermList = (offset, termstype) => {
  const { data, error, mutate } = useSWR(
    () => `product/termsGet/${offset}/${termstype}`
  );

  return {
    TermListData: data,
    TermListMutate: mutate,
    TermListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetTermsGetById = (id) => {
  const { data, error, mutate } = useSWR(() => `product/termsGetById/${id}`);

  return {
    TermsData: data,
    TermsMutate: mutate,
    TermsError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetUserProfile = (id) => {
  const { data, error, mutate } = useSWR(() => `user/userProfile/${id}`);

  return {
    UserProfileData: data,
    UserProfileMutate: mutate,
    UserProfileError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetProductInfo = (id) => {
  const { data, error, mutate } = useSWR(() => `product/productInfo/${id}`);

  return {
    ProductInfoData: data,
    ProductInfoMutate: mutate,
    ProductInfoError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetAllTypeTelecom = () => {
  const { data, error, mutate } = useSWR(() => `product/getAllTypeTelecom`);

  return {
    TypeTelecomListData: data,
    TypeTelecomListMutate: mutate,
    TypeTelecomListError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetByIdTypeTelecom = (id) => {
  const { data, error, mutate } = useSWR(
    () => `product/getByIdTypeTelecom/${id}`
  );

  return {
    ByIdTypeTeleComData: data,
    ByIdTypeTeleComMutate: mutate,
    ByIdTypeTeleComError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetPointAct = (id, offset, stores) => {
  const { data, error, mutate } = useSWR(
    () => `user/getPointAct/${id}/${offset}/${stores}`
  );

  return {
    PointActData: data,
    PointActMutate: mutate,
    PointActError: !data && !mutate,
    isLoading: !data && !error,
  };
};

const GetPushLogs = (offset, limit) => {
  const { data, error, mutate } = useSWR(
    () => `user/getPushLogs/${offset}/${limit}`
  );

  return {
    PushLogsData: data,
    PushLogsMutate: mutate,
    PushLogsError: !data && !mutate,
    isLoading: !data && !error,
  };
};

export {
  GetPushLogs,
  GetProductInfo,
  GetPointAct,
  GetByIdTypeTelecom,
  GetTermsGetById,
  GetBannerList,
  GetStoreList,
  GetAllTypeTelecom,
  GetuserList,
  GetuserProfile,
  GetTermList,
  GetStoreInfo,
  GetCardList,
  GetInforMation,
  GetBoardrecList,
  GetCategoryList,
  GetPlanList,
  GetPromotionList,
  GetPrTelecom,
  GetOrderInfoData,
  GetAdminData,
  GetBoardList,
  GetproductList,
  GetUserProfile,
  GetUserOrderList,
  GetAllOrderList,
  GetTelecomList,
  GetTelecomCategoryList,
  GetPlanListCate,
  GettypeTelecomListDatas,
  GetUsageCate,
};
