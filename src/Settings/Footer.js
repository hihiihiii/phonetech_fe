import styled from "styled-components";

const breakpoints = [600, 868, 992, 1200]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const FooterContainer = styled.div`
  width: 100%;
  background-color: #212121;
  margin-bottom: 100px;

  position: ${(props) => {
    if (props.footer) {
      return "";
    }
    return "absolute";
  }};
  z-index: ${(props) => {
    if (props.footer) {
      return "";
    }
    return "-1";
  }}; ;
`;

const FooterBox = styled.div`
  width: 80%;
  margin: 0px 13%;
  padding: 40px 0px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;

  ${mq[0]} {
    flex-direction: column;
  }

  ${mq[1]} {
    flex-direction: column;
  }
`;

const FooterImg = styled.img`
  width: 268px;
  height: 48px;
  object-fit: contain;
  ${mq[1]} {
    width: 258px;
    object-fit: contain;
  }

  ${mq[0]} {
    width: 238px;
    object-fit: contain;
  }
`;

const FooterContentBox = styled.div`
  margin-left: 30px;
  font-size: 16px;
  ${mq[1]} {
    margin-left: 0px;
    margin-top: 10px;
    font-size: 16px;
  }

  ${mq[0]} {
    flex-direction: column;
    font-size: 14px;
  }
`;

const FooterContent = styled.div`
  color: white;
  display: flex;
  flex-wrap: wrap;

  ${mq[0]} {
    flex-direction: column;
  }
`;

const FooterText = styled.div`
  margin: 2px;
`;

const ImageView = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

const ConfirmImage = styled.img`
  width: 132px;
  height: 132px;
  margin-top: 48px;
  object-fit: contain;
  //background-color: #a7a7a7;
`;

export {
  ConfirmImage,
  ImageView,
  FooterContainer,
  FooterBox,
  FooterImg,
  FooterContentBox,
  FooterContent,
  FooterText,
};
