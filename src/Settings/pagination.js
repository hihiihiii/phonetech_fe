import React, { useEffect } from "react";
import { Pagination } from "react-bootstrap";

const CustomPagination = ({ data, setOffset, page, setPage }) => {
  const _hadleOffset = (value) => {
    setOffset(value * 10);
  };

  useEffect(() => {
    let Pages = [];
    let counts;
    if (data?.count > 10) {
      counts = data?.count / 10;
      for (let i = 0; i < Number(counts); i++) {
        Pages.push(i);
      }
    }
    setPage(Pages);
  }, [data]);

  return (
    <>
      <div
        style={{
          width: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          margin: "24px 0px",
        }}
      >
        <Pagination>
          {page?.map((el, idx) => {
            return (
              <Pagination.Item onClick={() => _hadleOffset(el)}>
                {el + 1}
              </Pagination.Item>
            );
          })}
        </Pagination>
      </div>
    </>
  );
};

export default CustomPagination;
