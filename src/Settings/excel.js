import XLSX from "xlsx";

export const exportDataToXlsx = ({ data, filename }) => {
  const worksheet = XLSX.utils.json_to_sheet(data); // excel sheet하단의 worksheet에 해당
  const new_workbook = XLSX.utils.book_new(); // excel 파일에 해당
  XLSX.utils.book_append_sheet(new_workbook, worksheet, "SheetJS"); // excelsheet를 excel파일에 넣음
  XLSX.writeFile(new_workbook, filename + ".xlsx");
};
