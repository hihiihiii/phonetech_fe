const useConfirm = (message = null, onConfirm, onCancel) => {
  if (!onConfirm || typeof onConfirm !== "function") {
    return;
  }
  if (onCancel && typeof onCancel !== "function") {
    return;
  }

  const confirmAction = () => {
    if (window.confirm(message)) {
      onConfirm();
    } else {
      onCancel();
    }
  };

  return confirmAction;
};

const handlerTopMovement = () => {
  window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
};
export { useConfirm, handlerTopMovement };
