const validateEmail = (email) => {
  let message;
  if (!email) {
    message = "이메일을 입력해주세요";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    message = "이메일 형식이 아닙니다.";
  }
  return message;
};

const validatePassword = (Password) => {
  let message;
  if (!Password) {
    message = "패스워드를 입력하여 주세요";
  } else if (Password.length < 3) {
    message = "최소 3자리이상의 패스워드를 입력해주세요";
  }
  return message;
};

const validatePasswordConfirm = (Password, PasswordConfirm) => {
  if (Password !== PasswordConfirm) {
    return "비밀번호를 확인하여주세요";
  }
  return false;
};
const CheckPhone = (Number) => {
  Number = Number.split("-").join("");
  let result = /^((01[1|6|7|8|9])[1-9]+[0-9]{6,7})|(010[1-9][0-9]{7})$/;
  return result.test(Number);
};

// const baseurl = "http://192.168.0.22:3001/api"; //베이스
// const baseurl = "http://172.30.1.11:3001/api"; //베이스
const baseurl = "http://52.79.194.195:3001/api"; //베이스

export {
  baseurl,
  CheckPhone,
  validateEmail,
  validatePassword,
  validatePasswordConfirm,
};
