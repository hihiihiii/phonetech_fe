import FingerprintJS from "@fingerprintjs/fingerprintjs";

// Initialize an agent at application startup.
const fpPromise = FingerprintJS.load();

const GetFingerPrint = async () => {
  const fp = await fpPromise;
  const result = await fp.get();
  const visitorId = result.visitorId;
};

export { GetFingerPrint };
