const phoneProducts = [
  {
    id: 1,
    name: "아이폰 12 프로맥스",
    price: "678,000원",
    detailPrice: {
      ktMove: "549,000원",
      ktDevice: "449,000원",
      sktMove: "257,000원",
      sktDevie: "760,000원",
      lgMove: "475,000원",
      lgDevice: "125,000원",
    },
    main: "https://xn--6i0bp8gn47a.com/data/phone/phone0_20201021004448.png",
    detail:
      "https://xn--6i0bp8gn47a.com/data/editor/2010/3e49ce1da2726be93042f0785a507ada_1603430919_9915.jpg",
  },
  {
    id: 2,
    name: "아이폰 12 프로맥스",
    price: "478,000원",
    detailPrice: {
      ktMove: "549,000원",
      ktDevice: "449,000원",
      sktMove: "257,000원",
      sktDevie: "760,000원",
      lgMove: "475,000원",
      lgDevice: "125,000원",
    },
    main: "https://xn--6i0bp8gn47a.com/data/phone/phone0_20210414151114.jpg",
    detail:
      "https://xn--6i0bp8gn47a.com/data/editor/2106/edb068972f2f1594812682e6216703ff_1623034287_5767.png",
  },
  {
    id: 3,
    name: "아이폰 12 프로맥스",
    price: "908,000원",
    detailPrice: {
      ktMove: "549,000원",
      ktDevice: "449,000원",
      sktMove: "257,000원",
      sktDevie: "760,000원",
      lgMove: "475,000원",
      lgDevice: "125,000원",
    },
    main: "https://xn--6i0bp8gn47a.com/data/phone/phone0_20201021005539.png",
    detail:
      "https://xn--6i0bp8gn47a.com/data/editor/2010/3e49ce1da2726be93042f0785a507ada_1603430919_9915.jpg",
  },
  {
    id: 3,
    name: "아이폰 12 프로맥스",
    price: "108,000원",
    detailPrice: {
      ktMove: "549,000원",
      ktDevice: "449,000원",
      sktMove: "257,000원",
      sktDevie: "760,000원",
      lgMove: "475,000원",
      lgDevice: "125,000원",
    },
    main: "https://xn--6i0bp8gn47a.com/data/phone/phone0_20200907030405.png",
    detail:
      "https://xn--6i0bp8gn47a.com/data/editor/2010/3e49ce1da2726be93042f0785a507ada_1603430919_9915.jpg",
  },
];

export { phoneProducts };
