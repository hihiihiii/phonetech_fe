import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import { withRouter } from "react-router";
import { GetBoardList } from "Datas/swr";

import moment from "moment";
import { Fade } from "react-reveal";

const Wrapper = styled.div``;

const Post = styled.div`
  margin: 0 auto;
  margin-top: 150px;
  max-width: 1610px;
  width: 100%;
  padding: 0 20px;
  @media screen and (max-width: 768px) {
    margin-top: 130px;
  }

  @media screen and (max-width: 450px) {
    margin-top: 100px;
  }
`;

const PostTop = styled.div`
  align-items: flex-end;
  display: flex;
`;

const PostTopMetadata = styled.div`
  display: flex;
  flex-direction: column;
`;

const PostTopText = styled.div`
  color: #feb43c;
  font-size: 25px;

  @media screen and (max-width: 768px) {
    font-size: 22px;
  }

  @media screen and (max-width: 450px) {
    font-size: 18px;
  }
`;

const PostTopTitle = styled.div`
  font-size: 40px;
  @media screen and (max-width: 768px) {
    font-size: 32px;
  }
  @media screen and (max-width: 450px) {
    font-size: 26px;
  }
`;

const PostTopDate = styled.div`
  font-size: 25px;
  color: #989898;
  margin-left: auto;
  @media screen and (max-width: 768px) {
    font-size: 15px;
  }
  @media screen and (max-width: 450px) {
    font-size: 12px;
    font-weight: 600;
  }
`;

const PostContent = styled.div`
  margin-top: 60px;
  font-size: 25px;

  @media screen and (max-width: 768px) {
    font-size: 22px;
    margin-top: 50px;
  }
  @media screen and (max-width: 450px) {
    font-size: 17px;
    margin-top: 30px;
  }
`;

const AnnouncementPost = ({ match, location }) => {
  const { title, contents, createdAt } = location.state;
  console.log(location);

  //SWR

  return (
    <Fade Button>
      <Wrapper>
        <Post>
          <PostTop>
            <PostTopMetadata>
              {/* <PostTopText>폰테크 공지사항</PostTopText> */}
              <PostTopTitle>{title}</PostTopTitle>
            </PostTopMetadata>
          </PostTop>
          <PostTopDate>
            {moment(createdAt).format("YYYY년 MM월 DD일")}
          </PostTopDate>
          <PostContent>{contents}</PostContent>
        </Post>
      </Wrapper>
    </Fade>
  );
};

export default withRouter(AnnouncementPost);
