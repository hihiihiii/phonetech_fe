import React, { useState } from "react";
import styled from "styled-components";
import DaumPostcode from "react-daum-postcode";
import PhoneImgPng from "phone.png";
import { Link } from "react-router-dom";
import { Fade } from "react-reveal";
import { GetInforMation, GetUserOrderList, GetUserProfile } from "Datas/swr";
import { createContract, createPost } from "Datas/api";
import TermsAgree from "Components/TermsAgree/TermsAgree";
import moment from "moment";

const Wrapper = styled.div``;

const Inner = styled.div`
  max-width: 1610px;
  padding: 0 20px;
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  margin-top: 100px;
`;

const GuideLabel = styled.div``;

const GuideLabelBig = styled.div`
  font-size: 45px;
  margin-top: 40px;
  font-weight: 700;
  color: rgb(34, 37, 49);
`;

const GuideLabelSmall = styled.div`
  color: #838383;
`;

const PhoneImg = styled.div`
  & > img {
    width: 100%;
  }
  max-width: 111px;
  width: 100%;
  @media screen and (max-width: 450px) {
    max-width: 92px;
  }
  margin-right: 40px;
`;

const ContractInfo = styled.div`
  display: flex;
  margin-top: 75px;
  @media screen and (max-width: 450px) {
    margin-top: 50px;
  }
  flex-wrap: wrap;
  width: 100%;
  justify-content: space-between;
`;

const PhoneInfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% / 4 - 32px - 0.01px);

  @media screen and (max-width: 1600px) {
    width: 100%;
  }
`;

const PhoneInfoLabel = styled.div`
  border-bottom: 2px solid #d1d1d1;
  font-size: 30px;
  font-weight: 500;
  padding-bottom: 5px;
  display: flex;
  @media screen and (max-width: 450px) {
    font-size: 22px;
  }
`;

const MiddleWrapper = styled.div`
  display: flex;
  @media screen and (max-width: 1600px) {
    width: 100%;
    flex-wrap: wrap;
    margin: 10px 0 30px 0;
  }
  width: calc(100% / 2 - 32px - 0.01px);

  & > div {
    width: calc(100% / 1 - 32px - 0.01px);
    margin-left: calc(32px / 1);
    margin-right: calc(32px / 1);
    @media screen and (max-width: 1600px) {
      width: 100%;
      margin: 0;
    }
  }
  & > div:last-child {
    @media screen and (max-width: 1600px) {
      margin-top: 30px;
    }
  }
`;

const PhoneInfo = styled.div`
  display: flex;
  margin-top: 20px;
`;

const PhoneInfoRowWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: auto;
`;

const PhoneInfoRow = styled.div`
  display: flex;
  align-items: center;
  margin: 2px 0;
`;

const PhoneInfoRowKey = styled.div`
  font-size: 18px;
  margin-right: 20px;
  color: #838383;

  @media screen and (max-width: 450px) {
    font-size: 16px;
  }
`;

const PhoneInfoRowValue = styled.div`
  margin-left: auto;
  font-size: 20px;
  @media screen and (max-width: 450px) {
    font-size: 15px;
  }
`;

const PhoneInfoMonthlyPrice = styled.div`
  margin-top: 20px;
`;

const PhoneInfoMonthlyPriceOriginal = styled.div`
  display: flex;
  & > span {
    font-size: 20px;

    @media screen and (max-width: 450px) {
      font-size: 18px;
    }
  }
  & > span:last-child {
    margin-left: auto;
  }
`;

const PhoneInfoMonthlyPriceRate = styled.div`
  margin-top: 20px;
  display: flex;
  & > span {
    font-size: 20px;
    @media screen and (max-width: 450px) {
      font-size: 18px;
    }
  }
  & > span:last-child {
    margin-left: auto;
  }
`;

const PhoneInfoReleasePrice = styled.div`
  display: flex;
  padding: 0 10px;
  margin: 5px 0;
  & > span {
    color: #ef522a;
    font-size: 16px;
  }
  & > span:last-child {
    margin-left: auto;
  }
`;

const PhoneInfoReleasePriceDiscount = styled.div`
  display: flex;
  padding: 0 10px;
  margin: 5px 0;
  & > span {
    color: #ef522a;
    font-size: 16px;
  }
  & > span:last-child {
    margin-left: auto;
  }
`;

const PhoneInfoSuperDC = styled.div`
  display: flex;
  padding: 0 10px;
  margin: 5px 0;
  & > span {
    font-size: 16px;
  }
  & > span:last-child {
    margin-left: auto;
  }
`;

const PhoneInfoMonthlyPriceTotal = styled.div`
  background: #ef522a;
  margin-top: 10px;
  display: flex;
  padding: 12px 12px;
  font-weight: 500;
  border-radius: 10px;
  color: #fff;
  & > span {
    font-size: 16px;
  }
  & > span:last-child {
    margin-left: auto;
  }
`;

const PhoneInfoMonthlyCharge = styled.div`
  margin-top: 20px;
  font-size: 20px;
  @media screen and (max-width: 450px) {
    font-size: 18px;
  }
`;

const PhoneInfoMonthlyChargeReal = styled.div`
  font-size: 40px;
  color: #ef522a;
  font-weight: 500;
  margin-left: auto;
  @media screen and (max-width: 450px) {
    font-size: 32px;
  }
`;

const PhoneInfoMonthlyChargeMessage = styled.div`
  color: #5f5f5f;
  font-size: 15px;

  & > span {
    display: block;
  }
  & > span:first-child {
    margin-top: 25px;
  }
`;

const Phone5G = styled.div`
  display: flex;
  margin-left: auto;
  background: #ffb43c;
  font-weight: 700;
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  padding: 3px 16px;
  font-size: 20px;
  color: #fff;

  @media screen and (max-width: 450px) {
    font-size: 11px;
  }
`;

const CostumerInfo = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1300px;
  margin: 0 auto;
  margin-top: 80px;
  width: 100%;
`;

const CostumerInfoLabel = styled.div`
  font-size: 35px;
  color: rgb(34, 37, 49);
  margin-bottom: 40px;
`;

const AddrInfo = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1300px;
  margin: 0 auto;
  margin-top: 80px;
  width: 100%;
`;

const AddrInfoLabel = styled.div`
  font-size: 35px;
  color: rgb(34, 37, 49);
  margin-bottom: 40px;
`;

const TwoInputWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
`;

const DefaultInputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 10px 0;
  width: 100%;
  max-width: 550px;
`;

const DefaultInputLabel = styled.div`
  font-size: 25px;
  font-weight: 700;
  white-space: nowrap;
  color: #838383;

  @media screen and (max-width: 450px) {
    font-size: 18px;
  }
`;

const DefaultInput = styled.input`
  width: 100%;
  margin-top: 5px;
  height: 65px;
  border: 2px solid #f0f0f0;
  border-radius: 10px;
  color: #000;
  font-size: 16px;
  padding: 10px;

  @media screen and (max-width: 450px) {
    height: 45px;
  }
`;

const AdultOrNot = styled.div`
  display: flex;
  justify-content: space-between;
  @media screen and (max-width: 450px) {
    height: 45px;
  }

  & > div {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    margin-top: 5px;
    height: 65px;
    color: #636363;
    font-weight: 700;
    border: 2px solid #f0f0f0;
    border-radius: 10px;
    font-size: 16px;
    max-width: 250px;
    padding: 10px;
  }
  @media screen and (max-width: 450px) {
    & > div:first-child {
      margin-right: 25px;
    }
    & > div {
      height: 50px;
    }
  }
`;

const AddrInputWrapperLabel = styled.div`
  font-size: 25px;
  font-weight: 700;
  white-space: nowrap;
  margin-top: 10px;
  color: #838383;

  @media screen and (max-width: 450px) {
    font-size: 18px;
  }
`;

const AddrInputWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  flex-direction: column;
  max-width: 1200px;
  margin: 0 auto;
  width: 100%;
`;

const AddrInputTop = styled.div`
  display: flex;
  // align-items: center;
`;

const AddrInput = styled.input`
  width: 100%;
  margin-top: 5px;
  height: 65px;
  border: 2px solid #f0f0f0;
  border-radius: 10px;
  color: #000;
  font-size: 16px;
  padding: 10px;

  @media screen and (max-width: 450px) {
    height: 45px;
  }
`;

const AddrSearch = styled.div`
  background: #ef522a;
  color: #fff;
  padding: 8px 16px;
  border-radius: 10px;
  white-space: nowrap;
  font-weight: 700;
  display: flex;
  margin-left: 20px;
  align-items: center;
  justify-content: center;
`;

const AddrDetailed = styled.div``;

const CostumerRequire = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1200px;
  width: 100%;
  margin: 0 auto;
`;

const CostumerRequireLabel = styled.div`
  font-size: 25px;
  font-weight: 700;
  color: #838383;
  margin-top: 15px;

  @media screen and (max-width: 450px) {
    font-size: 18px;
  }
`;

const CostumerRequireTextarea = styled.textarea`
  border: 2px solid #f0f0f0;
  border-radius: 10px;
  resize: none;
  height: 200px;
  font-size: 16px;
  padding: 8px 20px;
  margin-top: 5px;
`;

const BigAgree = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 15px;
`;

const BigAgreeAgreeText = styled.div`
  margin-left: 5px;
`;

const BigAgreeAgree = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
`;

const BigAgreeAgreeInput = styled.input`
  -webkit-appearance: auto;
`;

const Agree = styled.div`
  display: flex;
  align-items: center;
  margin: 5px 0;
`;

const AgreeText = styled.div`
  display: flex;
  align-items: center;
  & > span:first-child {
    font-size: 20px;
    margin-left: 5px;
    @media screen and (max-width: 450px) {
      font-size: 14px;
    }
  }
  & > span:last-child {
    font-size: 15px;
    @media screen and (max-width: 450px) {
      font-size: 11px;
    }
    color: #969696;
    margin-left: 5px;
  }
`;

const AgreeAgree = styled.div`
  display: flex;
  align-items: center;
  margin-left: auto;
`;

const AgreeAgreeInput = styled.input`
  -webkit-appearance: auto;
`;

const AgreeAgreeText = styled.div`
  margin-left: 5px;
  @media screen and (max-width: 450px) {
    font-size: 14px;
  }
`;

const BigAgreeText = styled.div`
  font-size: 28px;
  font-weight: 500;
  color: rgb(34, 37, 49);

  @media screen and (max-width: 450px) {
    font-size: 24px;
  }
`;

const AgreeSection = styled.div`
  max-width: 1300px;
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 0 auto;
  margin-top: 100px;
`;

const SignupBtn = styled.div`
  width: 100%;
  height: 65px;
  max-width: 1300px;
  margin: 0 auto;
  color: #fff;
  background: #ef522a;
  border-radius: 10px;
  margin-top: 45px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 25px;
  @media screen and (max-width: 450px) {
    height: 45px;
    font-size: 18px;
  }
  margin-bottom: 80px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Contract = ({ location, profile, history }) => {
  console.log(location?.state);

  console.log(profile);

  const {
    mainDatas,
    cardSelected,
    ChooseSales,
    StoreInfoData,
    productData,
    allPrice,
    telCount,
    PayPrice,
    teleComPrice,
    DCprice,
  } = location?.state;

  const [acct, setAcct] = useState([]);
  const [datas, setDatas] = useState({
    name: "",
    birthday: "",
    tel: "",
    tel1: "",
    email: "",
    child: "",
    receiver: "",

    receivercall: "",
    zip: "",
    zipinfo: "",
    isvalidate: "",
    validate: "",
    pairbirth: "",
    pairtel: "",
    pairname: "",
    pairinfo: "",
    memo: "",
    usim: "",
  });

  const [isOpenPost, setIsOpenPost] = useState(false);
  const { GetUserOrderListMutate } = GetUserOrderList(profile?.userId);
  const { InfoData, InfoDataMutate } = GetInforMation();
  const onChangeOpenPost = () => {
    setIsOpenPost(!isOpenPost);
  };

  const onCompletePost = (data) => {
    let fullAddr = data.address;
    let extraAddr = "";

    if (data.addressType === "R") {
      if (data.bname !== "") {
        extraAddr += data.bname;
      }
      if (data.buildingName !== "") {
        extraAddr +=
          extraAddr !== "" ? `, ${data.buildingName}` : data.buildingName;
      }
      fullAddr += extraAddr !== "" ? ` (${extraAddr})` : "";
    }

    setDatas({ ...datas, zip: fullAddr });

    setIsOpenPost(false);
  };

  const _handleSubmit = async () => {
    //가맹점 수수료 계산

    /* const stackprice =
      Number(
        allPrice +
          telCount +
          mainDatas.storagePrice -
          mainDatas.Sales -
          (cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0) -
          (StoreInfoData !== undefined && StoreInfoData?.dc !== null
            ? Number(StoreInfoData?.dc)
            : 0)
      ) * Number(StoreInfoData?.salea / 100); */
    if (
      datas.name === "" ||
      datas.zip === "" ||
      datas.zipinfo === "" ||
      datas.receiver === "" ||
      datas.tel === "" ||
      datas.tel1 === "" ||
      datas.birthday === "" ||
      datas.email === ""
    ) {
      alert("정보를 제대로 입력해주세요");
      return;
    }
    if (datas.child) {
      if (
        datas.pairbirth === "" ||
        datas?.pairtel === "" ||
        datas?.pairname === "" ||
        datas.pairinfo === ""
      ) {
        alert("법정 대리인 정보를 입력하여주세요.");
        return;
      }
    }
    if (acct.length !== 0) {
      let res = acct.findIndex((item, idx) => {
        return item.agreed === !true;
      });

      if (res !== -1) {
        alert("약관을 동의해주세요");
        return;
      }
    }
    //데이터 형식 정리
    let body = {
      storeid: profile?.storeid,

      orderData: {
        userid: profile?.userId,
        image: productData?.images,
        name: datas?.name,
        usim: mainDatas?.sim === "새 유심" ? true : false,
        productid: productData?.id,
        tel: datas?.tel,
        birthday: datas?.birthday,
        tel1: datas?.tel1,
        email: datas?.email,
        child: datas?.child,
        pairbirth: datas?.pairbirth,
        pairtel: datas?.paritel,
        pairname: datas?.pairname,
        pairinfo: datas.pairinfo,
        receiver: datas?.receiver,
        receivercall: datas?.receivercall,
        zip: datas?.zip,
        validateOrderDay: moment(new Date()).format("YYYY-MM-DD"),
        zipinfo: datas?.zipinfo,
        memo: datas?.memo,
      },
      ProductData: {
        image: productData?.images,
        nowtel: mainDatas?.oldtelecom,
        willtell: mainDatas?.newtelecom,
        saletype: mainDatas?.discount,
        pricetype: teleComPrice?.name,
        pricetypeprice: teleComPrice?.pricemonth,
        monthuse: mainDatas?.checkout,
        salemonth:
          mainDatas.checkout !== "일시불"
            ? Math.round(
                Number(Number(allPrice)) /
                  mainDatas.checkout.replace("개월", "")
              )
            : Number(allPrice),
        marketprice: productData?.marketprice,
        openprice: `${mainDatas?.discount} ${ChooseSales.toString().replace(
          /\B(?=(\d{3})+(?!\d))/g,
          ","
        )}원`,
        dcprice: DCprice,
        monttype: mainDatas?.checkout,
        cardsale: cardSelected?.discount,
        cardname: cardSelected?.name,
        monthprice: PayPrice,

        productname: productData?.name,
        colorname: mainDatas?.color,
        hard: mainDatas?.storage,
      },
    };

    //실제 API POST
    await createContract(body);
    await GetUserOrderListMutate();
    history.push("/success");

    console.log(body);
  };

  const postCodeStyle = {
    display: "block",
    position: "relative",
    top: "0%",
    width: "100%",
    height: "400px",
    padding: "7px",
  };

  return (
    <Fade Button>
      <Wrapper>
        <Inner>
          <GuideLabel>
            <GuideLabelBig>개통신청서</GuideLabelBig>
            <GuideLabelSmall>
              신청정보 및 배송정보 입력 후 온라인 공식신청서 작성을 완료해주시기
              바랍니다.
            </GuideLabelSmall>
          </GuideLabel>
          <ContractInfo>
            <PhoneInfoWrapper>
              <PhoneInfoLabel>
                {productData?.name}
                <Phone5G>{productData?.giga}</Phone5G>
              </PhoneInfoLabel>
              <PhoneInfo>
                <PhoneImg>
                  <img alt="" src={productData?.images[0]} />
                </PhoneImg>
                <PhoneInfoRowWrapper>
                  <PhoneInfoRow>
                    <PhoneInfoRowKey>사용중인 통신사</PhoneInfoRowKey>
                    <PhoneInfoRowValue>
                      {mainDatas?.oldtelecom}
                    </PhoneInfoRowValue>
                  </PhoneInfoRow>
                  <PhoneInfoRow>
                    <PhoneInfoRowKey>사용할 통신사</PhoneInfoRowKey>
                    <PhoneInfoRowValue>
                      {mainDatas?.newtelecom}
                    </PhoneInfoRowValue>
                  </PhoneInfoRow>
                  <PhoneInfoRow>
                    <PhoneInfoRowKey>할인방법</PhoneInfoRowKey>
                    <PhoneInfoRowValue>{mainDatas?.discount}</PhoneInfoRowValue>
                  </PhoneInfoRow>
                  <PhoneInfoRow>
                    <PhoneInfoRowKey>요금제</PhoneInfoRowKey>
                    <PhoneInfoRowValue>{teleComPrice?.name}</PhoneInfoRowValue>
                  </PhoneInfoRow>
                  <PhoneInfoRow>
                    <PhoneInfoRowKey>구매방법</PhoneInfoRowKey>
                    <PhoneInfoRowValue>{mainDatas?.checkout}</PhoneInfoRowValue>
                  </PhoneInfoRow>
                  <PhoneInfoRow>
                    <PhoneInfoRowKey>유심선택</PhoneInfoRowKey>
                    <PhoneInfoRowValue>{mainDatas?.sim}</PhoneInfoRowValue>
                  </PhoneInfoRow>
                </PhoneInfoRowWrapper>
              </PhoneInfo>
            </PhoneInfoWrapper>
            <MiddleWrapper>
              <PhoneInfoWrapper>
                <PhoneInfoLabel>월 할부금</PhoneInfoLabel>
                <PhoneInfoMonthlyPrice>
                  <PhoneInfoMonthlyPriceOriginal>
                    <span>할부원금</span>
                    <span>{allPrice}원</span>
                  </PhoneInfoMonthlyPriceOriginal>
                  <PhoneInfoReleasePrice>
                    <span>출고가</span>
                    <span>
                      {productData?.marketprice
                        ?.toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      원
                    </span>
                  </PhoneInfoReleasePrice>
                  <PhoneInfoReleasePriceDiscount>
                    <span>{mainDatas?.discount}</span>
                    <span>{ChooseSales.toLocaleString()}원</span>
                  </PhoneInfoReleasePriceDiscount>
                  <PhoneInfoSuperDC>
                    <span>
                      {InfoData !== undefined && InfoData[0].pagename} 슈퍼 D.C
                    </span>
                    <span>{Number(DCprice).toLocaleString()}원</span>
                  </PhoneInfoSuperDC>
                  <PhoneInfoMonthlyPriceTotal>
                    <span>
                      {mainDatas?.checkout !== "일시불"
                        ? `월 (${mainDatas?.checkout})`
                        : mainDatas?.checkout}
                    </span>
                    <span>
                      {mainDatas.checkout !== "일시불"
                        ? Math.round(
                            Number(Number(allPrice)) /
                              mainDatas.checkout.replace("개월", "")
                          )
                            ?.toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        : Number(allPrice)
                            ?.toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      원
                    </span>
                  </PhoneInfoMonthlyPriceTotal>
                </PhoneInfoMonthlyPrice>
              </PhoneInfoWrapper>
              <PhoneInfoWrapper>
                <PhoneInfoLabel>월 통신요금</PhoneInfoLabel>
                <PhoneInfoMonthlyPriceRate>
                  <span>요금제</span>
                  <span>
                    {teleComPrice?.pricemonth
                      ?.toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    원
                  </span>
                </PhoneInfoMonthlyPriceRate>
                <PhoneInfoMonthlyPriceTotal>
                  <span>월 납부액</span>
                  <span>
                    {teleComPrice?.pricemonth
                      ?.toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    원
                  </span>
                </PhoneInfoMonthlyPriceTotal>
              </PhoneInfoWrapper>
            </MiddleWrapper>
            <PhoneInfoWrapper>
              <PhoneInfoLabel>월 실 납부액</PhoneInfoLabel>
              <PhoneInfoMonthlyCharge>월 청구액</PhoneInfoMonthlyCharge>
              <PhoneInfoMonthlyChargeReal>
                {PayPrice.toLocaleString()}원
              </PhoneInfoMonthlyChargeReal>
              <PhoneInfoMonthlyChargeMessage>
                <span>* 부가세·할부이자 연 5.9% 포함 금액</span>
                <br />
                <span>
                  실제로 청구되는 금액으로 추가로 청구되실 금액 없습니다.
                </span>
              </PhoneInfoMonthlyChargeMessage>
            </PhoneInfoWrapper>
          </ContractInfo>
          <CostumerInfo>
            <CostumerInfoLabel>가입자 정보</CostumerInfoLabel>
            <TwoInputWrapper>
              <DefaultInputWrapper>
                <DefaultInputLabel>명의자 성함</DefaultInputLabel>
                <DefaultInput
                  placeholder="이름을 입력해주세요"
                  value={datas.name}
                  onChange={(e) => setDatas({ ...datas, name: e.target.value })}
                />
              </DefaultInputWrapper>
              <DefaultInputWrapper>
                <DefaultInputLabel>생년 월일</DefaultInputLabel>
                <DefaultInput
                  placeholder="생년월일을 입력해주세요"
                  value={datas.birthday}
                  type="date"
                  onChange={(e) => {
                    setDatas({ ...datas, birthday: e.target.value });
                  }}
                />
              </DefaultInputWrapper>
            </TwoInputWrapper>
            <TwoInputWrapper>
              <DefaultInputWrapper>
                <DefaultInputLabel>개통할 휴대폰번호</DefaultInputLabel>
                <DefaultInput
                  placeholder="입력해주세요"
                  value={datas.tel}
                  onChange={(e) => setDatas({ ...datas, tel: e.target.value })}
                />
              </DefaultInputWrapper>
              <DefaultInputWrapper>
                <DefaultInputLabel>추가연락처</DefaultInputLabel>
                <DefaultInput
                  placeholder="입력해주세요"
                  value={datas.tel1}
                  onChange={(e) => setDatas({ ...datas, tel1: e.target.value })}
                />
              </DefaultInputWrapper>
            </TwoInputWrapper>
            <TwoInputWrapper>
              <DefaultInputWrapper>
                <DefaultInputLabel>이메일</DefaultInputLabel>
                <DefaultInput
                  placeholder="입력해주세요"
                  value={datas.email}
                  onChange={(e) =>
                    setDatas({ ...datas, email: e.target.value })
                  }
                />
              </DefaultInputWrapper>
              <DefaultInputWrapper>
                <DefaultInputLabel>성인 / 미성년자 여부</DefaultInputLabel>
                <AdultOrNot>
                  <div
                    onClick={() => setDatas({ ...datas, child: false })}
                    style={
                      !datas?.child
                        ? {
                            backgroundColor: "orange",
                            border: "none",
                            color: "#fff",
                            cursor: "pointer",
                          }
                        : { cursor: "pointer" }
                    }
                  >
                    성인
                  </div>
                  <div
                    onClick={() => setDatas({ ...datas, child: true })}
                    style={
                      datas?.child
                        ? {
                            backgroundColor: "orange",
                            border: "none",
                            color: "#fff",
                            cursor: "pointer",
                          }
                        : { cursor: "pointer" }
                    }
                  >
                    미성년자
                  </div>
                </AdultOrNot>
              </DefaultInputWrapper>
            </TwoInputWrapper>
            {datas?.child && (
              <>
                <TwoInputWrapper>
                  <DefaultInputWrapper>
                    <DefaultInputLabel>법정대리인 성함</DefaultInputLabel>
                    <DefaultInput
                      placeholder="대리인의 성함을 입력해주세요"
                      value={datas.pairname}
                      onChange={(e) =>
                        setDatas({ ...datas, pairname: e.target.value })
                      }
                    />
                  </DefaultInputWrapper>
                  <DefaultInputWrapper>
                    <DefaultInputLabel>법정대리인 생년월일</DefaultInputLabel>
                    <DefaultInput
                      placeholder="대리인의 생년월일을 입력해주세요"
                      value={datas.pairbirth}
                      type="date"
                      onChange={(e) =>
                        setDatas({ ...datas, pairbirth: e.target.value })
                      }
                    />
                  </DefaultInputWrapper>
                </TwoInputWrapper>
                <TwoInputWrapper>
                  <DefaultInputWrapper>
                    <DefaultInputLabel>법정대리인 연락처</DefaultInputLabel>
                    <DefaultInput
                      placeholder="대리인의 연락처를 입력해주세요"
                      value={datas.paritel}
                      onChange={(e) =>
                        setDatas({ ...datas, pairtel: e.target.value })
                      }
                    />
                  </DefaultInputWrapper>
                  <DefaultInputWrapper>
                    <DefaultInputLabel>명의자와의 관계</DefaultInputLabel>
                    <DefaultInput
                      placeholder="명의자와의 관계를 입력해주세요"
                      value={datas.pairinfo}
                      onChange={(e) =>
                        setDatas({ ...datas, pairinfo: e.target.value })
                      }
                    />
                  </DefaultInputWrapper>
                </TwoInputWrapper>
              </>
            )}
          </CostumerInfo>
          <AddrInfo>
            <AddrInfoLabel>배송지 정보</AddrInfoLabel>
            <TwoInputWrapper>
              <DefaultInputWrapper>
                <DefaultInputLabel>받으시는 분</DefaultInputLabel>
                <DefaultInput
                  placeholder="입력해주세요"
                  value={datas.receiver}
                  onChange={(e) =>
                    setDatas({ ...datas, receiver: e.target.value })
                  }
                />
              </DefaultInputWrapper>
              <DefaultInputWrapper>
                <DefaultInputLabel>배송 연락처</DefaultInputLabel>
                <DefaultInput
                  placeholder="입력해주세요"
                  value={datas.receivercall}
                  onChange={(e) =>
                    setDatas({ ...datas, receivercall: e.target.value })
                  }
                />
              </DefaultInputWrapper>
            </TwoInputWrapper>
            <AddrInputWrapper>
              <AddrInputWrapperLabel>배송지 주소</AddrInputWrapperLabel>
              <AddrInputTop onClick={() => setIsOpenPost(!isOpenPost)}>
                <AddrInput
                  placeholder="배송지 주소를 입력해주세요"
                  value={datas.zip}
                  disabled={true}
                  onChange={(e) => setDatas({ ...datas, zip: e.target.value })}
                />

                <AddrSearch onClick={() => setIsOpenPost(!isOpenPost)}>
                  검색
                </AddrSearch>
              </AddrInputTop>
              {isOpenPost ? (
                <DaumPostcode
                  style={postCodeStyle}
                  autoClose
                  onComplete={onCompletePost}
                />
              ) : null}
              <AddrDetailed style={{ marginTop: 15 }}>
                <AddrInput
                  placeholder="상세주소를 입력해주세요"
                  value={datas.zipinfo}
                  onChange={(e) =>
                    setDatas({ ...datas, zipinfo: e.target.value })
                  }
                />
              </AddrDetailed>
            </AddrInputWrapper>
          </AddrInfo>
          <CostumerRequire>
            <CostumerRequireLabel>고객요청사항</CostumerRequireLabel>
            <CostumerRequireTextarea
              onChange={(e) => setDatas({ ...datas, memo: e.target.value })}
              placeholder="요청사항을 입력해주세요"
            />
          </CostumerRequire>
          <TermsAgree
            acct={acct}
            setAcct={setAcct}
            SignUp
            termstype={1}
            style={{ marginTop: 24 }}
          ></TermsAgree>
          {/* <AgreeSection>
            <BigAgree>
              <BigAgreeText>약관 및 이용 동의</BigAgreeText>
              <BigAgreeAgree>
                <BigAgreeAgreeInput type="checkbox" />
                <BigAgreeAgreeText>전체 동의</BigAgreeAgreeText>
              </BigAgreeAgree>
            </BigAgree>
            <Agree>
              <AgreeText>
                <span>회선유지가 기간 및 기간 내 금지사항</span>
                <span>전체보기</span>
              </AgreeText>
              <AgreeAgree>
                <AgreeAgreeInput type="checkbox" />
                <AgreeAgreeText>동의</AgreeAgreeText>
              </AgreeAgree>
            </Agree>
            <Agree>
              <AgreeText>
                <span>요금제 유지기간 안내 및 금지사항</span>
                <span>전체보기</span>
              </AgreeText>
              <AgreeAgree>
                <AgreeAgreeInput type="checkbox" />
                <AgreeAgreeText>동의</AgreeAgreeText>
              </AgreeAgree>
            </Agree>
            <Agree>
              <AgreeText>
                <span>위약금 및 잔여할부금 관련 안내</span>
                <span>전체보기</span>
              </AgreeText>
              <AgreeAgree>
                <AgreeAgreeInput type="checkbox" />
                <AgreeAgreeText>동의</AgreeAgreeText>
              </AgreeAgree>
            </Agree>
            <Agree>
              <AgreeText>
                <span>개인정보 조회 및 동의사항</span>
                <span>전체보기</span>
              </AgreeText>
              <AgreeAgree>
                <AgreeAgreeInput type="checkbox" />
                <AgreeAgreeText>동의</AgreeAgreeText>
              </AgreeAgree>
            </Agree>
          </AgreeSection> */}
          <SignupBtn onClick={() => _handleSubmit()} style={{ marginTop: 50 }}>
            <Link onClick={() => _handleSubmit()}>개통하기</Link>
          </SignupBtn>
        </Inner>
      </Wrapper>
    </Fade>
  );
};

export default Contract;
