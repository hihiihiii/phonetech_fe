import Slider from "react-slick";
import { useEffect } from "react";
import styled from "styled-components";
import { GetBannerList, GetPromotionList } from "Datas/swr";
import mockBanner from "../../mockBanner.png";
import {
  isTablet,
  isMobile
} from "react-device-detect";
import { useState } from "react";

const Leaderboard = styled.img`
  width: 100%;
  margin-top: 100px;
  height: 550px;
  background: #f0f0f0;

  @media screen and (max-width: 768px) {
    height: 320px;
    margin-top: 80px;
  }

  @media screen and (max-width: 375px) {
    height: 207px;
    margin-top: 70px;
  }
`;

const Sliders = () => {
  const { BannerListData, isLoading } = GetBannerList(0);
  const [BannerData, setBannerData] = useState([]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  };

  useEffect(() => {
    if (!isLoading && BannerListData?.status !== 400) {
      setBannerData(BannerListData?.rows);
    }
  }, [BannerListData]);
  
  console.log(BannerListData);
  return (
    <Slider {...settings}>
      {BannerData !== undefined && BannerData?.length !== 0 ? (
        BannerData?.map((el, idx) => {
          if (isTablet || isMobile && el.mobileon)
          {
            return (
              <a href={el.url}>
                <Leaderboard src={el?.mobileimage[0]}></Leaderboard>
              </a>
            );
          }
          else
          {
            return (
              <a href={el.url}>
                <Leaderboard src={el?.image[0]}></Leaderboard>
              </a>
            );  
            }
          
        })
      ) : (
        <Leaderboard src={mockBanner} />
      )}
    </Slider>
  );
};
export default Sliders;
