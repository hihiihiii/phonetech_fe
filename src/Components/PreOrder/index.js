import React, { Component } from "react";
import styled from "styled-components";

import { GetBoardList, GetBoardrecList, GetInforMation } from "Datas/swr";
import { Link } from "react-router-dom";
import Sliders from "Components/Sliders";
import { Fade } from "react-reveal";
import { useState } from "react";
import { useEffect } from "react";
import CustomPagination from "Settings/pagination";

const Wrapper = styled.div``;

const Leaderboard = styled.div`
  width: 100%;
  margin-top: 100px;
  height: 550px;
  background: #f0f0f0;

  @media screen and (max-width: 768px) {
    height: 320px;
  }

  @media screen and (max-width: 375px) {
    height: 207px;
  }
`;

const AnnouncementWrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1610px;
  padding: 0 20px;
  width: 100%;
  margin: 0 auto;
  margin-top: 80px;
  @media screen and (max-width: 768px) {
    margin-top: 60px;
  }
  @media screen and (max-width: 375px) {
    margin-top: 30px;
  }
`;

const AnnouncementText = styled.div`
  font-size: 25px;
  color: #feb43c;
  @media screen and (max-width: 768px) {
    font-size: 18px;
    font-weight: 600;
    margin-left: 2%;
  }
  @media screen and (max-width: 375px) {
    font-size: 14px;
    font-weight: 600;
    margin-left: 2%;
  }
`;

const AnnouncementSubText = styled.div`
  font-size: 32px;
  font-weight: 500;
  color: #000;
  @media screen and (max-width: 768px) {
    font-size: 26px;
    font-weight: 600;
    margin-left: 2%;
  }
  @media screen and (max-width: 375px) {
    font-size: 22px;
    font-weight: 600;
    margin-left: 2%;
  }
`;

const AnnouncementBoxWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  margin-bottom: 110px;
  margin-top: 50px;
  @media screen and (max-width: 768px) {
    margin-top: 45px;
  }
  @media screen and (max-width: 375px) {
    margin-top: 30px;
  }
`;

const AnnouncementBox = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
  max-width: 735px;
  background: #fff;
  width: 100%;
  margin-bottom: 40px;
`;

const BoxThunbmail = styled.img`
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  /* background-image: ${(props) => {
    let url;
    if (props.url) {
      let link = props.url[0];
      console.log(link);
      url = `url(${link})`;
    } else {
    }
    return url;
  }};
  background-repeat: no-repeat;
  background-size: cover; */
  width: 100%;
  object-fit: cover;
  height: 225px;
`;

const BoxTitle = styled.div`
  margin: 10px 20px 0 20px;
  font-size: 35px;
  font-weight: 500;
  @media screen and (max-width: 768px) {
    font-size: 28px;
    font-weight: 500;
  }
  @media screen and (max-width: 375px) {
    font-size: 22px;
    font-weight: 500;
  }
`;

const BoxDesc = styled.div`
  margin: 10px 20px 25px 20px;
  color: #a7a7a7;
`;

const PreOrder = () => {
  //SWR
  const [offset, setOffset] = useState(0);
  const { BoardListrecData, isLoading } = GetBoardrecList(offset);
  const { InfoData, InfoDataMutate } = GetInforMation();
  const [page, setPage] = useState([]);
  const [BoardData, setBoardData] = useState([]);

  useEffect(() => {
    if (!isLoading) {
      setBoardData(BoardListrecData?.rows);
    }
  }, [BoardListrecData]);
 
  return (
    <Fade Button>
      <Wrapper>
        <Sliders />
        <AnnouncementWrapper>
          <AnnouncementText>{InfoData !== undefined && InfoData[0].pagename} 사전예약</AnnouncementText>
          <AnnouncementSubText>사전예약</AnnouncementSubText>
          <AnnouncementBoxWrapper>
            {BoardData?.map((el, idx) => {
              return (
                <>
                  <AnnouncementBox>
                    <Link
                      to={{
                        pathname: "/preorder/detail",
                        state: {
                          id: el?.id,
                          title: el?.title,
                          createdAt: el?.createdAt,
                          contents: el?.contents,
                          url: el?.url,
                          images: el?.images,
                        },
                      }}
                    >
                      <BoxThunbmail src={el?.titleimages[0]} />
                      <BoxTitle style={{ marginBottom: 15 }}>
                        {el?.title}
                      </BoxTitle>
                      {/* <BoxDesc
                          style={{ marginBottom: 25 }}
                          dangerouslySetInnerHTML={{
                            __html: el?.contents,
                          }}
                        ></BoxDesc> */}
                      {/* <BoxDesc style={{ marginBottom: 15 }}></BoxDesc> */}
                    </Link>
                  </AnnouncementBox>
                </>
              );
            })}
            <CustomPagination
              data={BoardListrecData}
              setOffset={setOffset}
              page={page}
              setPage={setPage}
            />
          </AnnouncementBoxWrapper>
        </AnnouncementWrapper>
      </Wrapper>
    </Fade>
  );
};

export default PreOrder;
