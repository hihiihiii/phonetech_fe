import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import { withRouter } from "react-router";
import { GetBoardList, GetBoardrecList } from "Datas/swr";

import moment from "moment";
import { Fade } from "react-reveal";

const Wrapper = styled.div``;

const Post = styled.div`
  margin: 0 auto;
  margin-top: 150px;
  max-width: 1610px;
  width: 100%;
  padding: 0 20px;
  @media screen and (max-width: 768px) {
    margin-top: 130px;
  }

  @media screen and (max-width: 450px) {
    margin-top: 100px;
  }
`;

const PostTop = styled.div`
  align-items: flex-end;
  display: flex;
`;

const PostTopMetadata = styled.div`
  display: flex;
  flex-direction: column;
`;

const PostTopText = styled.div`
  color: #feb43c;
  font-size: 25px;

  @media screen and (max-width: 768px) {
    font-size: 22px;
  }

  @media screen and (max-width: 450px) {
    font-size: 18px;
  }
`;

const PostTopTitle = styled.div`
  font-size: 40px;
  @media screen and (max-width: 768px) {
    font-size: 32px;
  }
  @media screen and (max-width: 450px) {
    font-size: 26px;
  }
`;

const PostTopDate = styled.div`
  font-size: 25px;
  color: #989898;
  margin-left: auto;
  @media screen and (max-width: 768px) {
    font-size: 15px;
  }
  @media screen and (max-width: 450px) {
    font-size: 12px;
    font-weight: 600;
  }
`;

const PostContent = styled.div`
  margin-top: 60px;
  font-size: 25px;

  @media screen and (max-width: 768px) {
    font-size: 22px;
    margin-top: 50px;
  }
  @media screen and (max-width: 450px) {
    font-size: 17px;
    margin-top: 30px;
  }
`;

const ReviewBox = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
  background: #fff;
  width: calc(100% / 4 - 45px);
  height: 250px;
  margin-bottom: 40px;
  border-radius: 7px;
  margin-right: 15px;
  @media screen and (max-width: 768px) {
    width: calc(100% / 2 - 45px);
  }

  @media screen and (max-width: 375px) {
    width: 100%;
  }
`;

const PreOrderDetail = ({ location }) => {
  const { title, createdAt, contents, url, images } = location.state;

  return (
    <Fade Button>
      <Wrapper>
        <Post>
          <PostTop>
            <PostTopMetadata>
              <PostTopTitle>{title}</PostTopTitle>
            </PostTopMetadata>
          </PostTop>
          <PostTopDate>
            {moment(createdAt).format("YYYY년 MM월 DD일")}
          </PostTopDate>
          <PostContent
            dangerouslySetInnerHTML={{
              __html: contents,
            }}
          ></PostContent>
          <div style={{ marginTop: 50, fontSize: 25, fontWeight: "600" }}>
            사전예약 상품
          </div>
          <div style={{ marginTop: 25 }}>
            {url !== null &&
              url?.length !== 0 &&
              url?.map((el, idx) => {
                return (
                  <ReviewBox
                    style={{
                      position: "relative",
                      zIndex: 1,
                      cursor: "pointer",
                    }}
                  >
                    <a href={el} style={{ width: "100%", height: "100%" }}>
                      {images?.length !== 0 && (
                        <img
                          src={images[idx]}
                          style={{
                            width: "100%",
                            height: "100%",
                            resize: "both",
                            objectFit: "contain",
                            borderRadius: 9,
                          }}
                        />
                      )}
                    </a>
                  </ReviewBox>
                );
              })}
          </div>
        </Post>
      </Wrapper>
    </Fade>
  );
};

export default withRouter(PreOrderDetail);
