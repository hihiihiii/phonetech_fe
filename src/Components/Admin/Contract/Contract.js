import { DeleteContract, UpdateContract } from "Datas/api";
import { GetAllOrderList, GetStoreList } from "Datas/swr";
import React, { Component, useEffect, useState } from "react";
import { Fade } from "react-reveal";
import { Link } from "react-router-dom";
import Select from "react-select";
import { exportDataToXlsx } from "Settings/excel";
import styled from "styled-components";
import { Pagination } from "react-bootstrap";
import { withRouter } from "react-router";
import CustomPagination from "Settings/pagination";
import moment from "moment";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
`;

const Delete = styled.div`
  display: flex;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const SearchBox = styled.input`
  width: 100%;
  height: 40px;
  border: none;

  border-radius: 5px;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.5);
  background-color: #f8f8f8;
  padding-left: 12px;
`;
const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Contract = ({ profile }) => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  const [selected, setSelected] = useState({ label: "전체", value: "전체" });
  const [storeList, setStoreList] = useState([]);

  const { StoreListData, isLoading, StoreListMutate } = GetStoreList(0);
  const [storeSelect, setStoreSelect] = useState({
    value: "전체",
    label: "전체",
  });
  const [checked, setChecked] = useState(false);
  const [TypeSelect, setTypeSelect] = useState({
    label: "소속별 검색",
    value: 0,
  });
  const [orderList, setOrderList] = useState([]);
  const [type, setType] = useState({ label: "전체", value: "전체" });
  const [rowData, setRowData] = useState([]);
  const [SearchData, setSearchData] = useState({
    start: moment(new Date()).format("YYYY-MM-DD"),
    end: "2025-01-01",
    text: null,
  });
  const { allOrderListData, allOrderListMutate } = GetAllOrderList(
    offset, //오프셋
    selected?.value, //개통여부
    TypeSelect.value, //설정값 검색타입
    SearchData.start, //시작일
    SearchData.end, //종료일
    SearchData.text, //검색어
    storeSelect.value
  );

  useEffect(() => {
    if (profile) {
      if (profile.gm === 1) {
        setStoreSelect({
          ...storeSelect,
          value: profile.storeid,
        });
        setTypeSelect({
          label: "접수날짜 검색",
          value: 1,
        });
      }
    }
  }, [profile]);
  console.log(storeSelect.value);
  useEffect(() => {
    if (StoreListData?.rows !== undefined) {
      let Arrays = [];
      if (StoreListData?.rows?.length !== 0) {
        for (let i = 0; i < StoreListData?.rows?.length + 1; i++) {
          if (StoreListData?.rows?.length === i) {
            Arrays.push({
              value: "전체",
              label: "전체",
            });
          } else {
            Arrays.push({
              value: StoreListData?.rows?.[i]?.id,
              label: StoreListData?.rows?.[i]?.name,
            });
          }
        }
        console.log(Arrays);
        setStoreList(Arrays);
      }
    }
  }, [StoreListData]);

  const _getRow = (value) => {
    let res = value.map((item) => {
      let trueth;
      if (item.isvalidate === 1) {
        trueth = "승인";
      } else if (item.isvalidate === 0) {
        trueth = "비승인";
      } else if (item.isvalidate === 2) {
        trueth = "취소";
      }
      return {
        번호: item.id,
        이름: item.name,
        이메일: item.User.email,
        기기명: item.OrderProduct.productname,
        소속: item.User.Store.name,
        승인여부: trueth,
        memoadmin: item.memoadmin,
        validate: item.validate,
        validateAccpetDay: item.validateAccpetDay,
        validateOrderDay: item.validateOrderDay,
      };
    });

    setRowData(res);
  };

  const storedata1 = [
    { label: "접수날짜 검색", value: 1 },
    { label: "승인날짜 검색", value: 2 },
    { label: "이름 검색", value: 3 },
  ];

  const storedata2 = [
    { label: "소속별 검색", value: 0 },
    { label: "접수날짜 검색", value: 1 },
    { label: "승인날짜 검색", value: 2 },
    { label: "이름 검색", value: 3 },
  ];

  useEffect(() => {
    let checkd = rowData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [rowData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화

    let data = [...rowData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setRowData(resdata);
    setChecked(true);
  };

  useEffect(() => {
    console.log(allOrderListData);
    if (allOrderListData?.rows !== undefined) {
      /* console.log(allOrderListData?.rows);
      if (type?.label === "전체") {
        setOrderList(allOrderListData?.rows);
        _getRow(allOrderListData?.rows);
      } else if (type?.label === "승인") {
        const tempData = allOrderListData?.rows?.filter(
          (el, idx) => el?.isvalidate === 1
        );
        setOrderList(tempData);
        _getRow(tempData);
      } else {
        const tempData = allOrderListData?.rows?.filter(
          (el, idx) => el?.isvalidate !== 1
        );
        setOrderList(tempData);
        _getRow(tempData);
      } */
      //setOrderList(allOrderListData?.rows);
      _getRow(allOrderListData?.rows);
    }
  }, [allOrderListData, type]);

  console.log(allOrderListData?.count);

  const _handleSelect = (id) => {
    console.log("호출");

    let temp = [...rowData];
    temp?.map((el, idx) => {
      if (el?.번호 === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
      }
    });
    setRowData(temp);
  };

  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < rowData?.length; index++) {
        if (rowData[index]?.validate === true) {
          let body = {
            id: rowData[index]?.번호,
          };
          await DeleteContract(body);
        }
      }
      await allOrderListMutate();
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );
  const _handleDelete = async () => {
    desk();
  };

  const _handleUpdate = async (type, otype) => {
    let newArray = [];
    if (rowData.every((item) => item.validate === false)) {
      alert("선택된 상품이 없습니다.");
      return;
    }

    for (let index = 0; index < rowData?.length; index++) {
      if (rowData[index]?.validate === true) {
        let body = {
          isvalidate: otype,
          typeOrder: type,
          validateAccpetDay: moment(new Date()).format("YYYY-MM-DD"),
          storeid: allOrderListData?.rows[index].storeid,
          userid: allOrderListData?.rows[index].userid,
        };
        newArray.push(await UpdateContract(rowData[index]?.번호, body));
      }
    }
    let task = newArray.every((item) => item === true);
    if (task === true) {
      if (type === "승인") {
        alert("개통 승인성공");
      } else {
        alert("개통 취소 성공");
      }
    } else {
      alert("업데이트 실패");
    }
    await allOrderListMutate();
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <div style={{ display: "flex", alignItems: "center" }}>
            <TopLabel style={{ marginRight: 15 }}>개통관리</TopLabel>
            <div style={{ width: 200 }}>
              <Select
                value={selected}
                onChange={(e) => {
                  setSelected({ label: e.label, value: e.value });
                }}
                /* options={[
                  { label: "전체", value: "전체" },
                  { label: "승인", value: "1" },
                  { label: "취소", value: "2" },
                  { label: "진행중", value: "0" },
                ]} */
                options={[
                  { label: "전체", value: "전체" },
                  { label: "대기중", value: "0" },
                  { label: "승인 완료", value: "1" },
                  { label: "개통 취소", value: "2" },
                  { label: "해피콜 완료", value: "3" },
                  { label: "서식지 완료", value: "4" },
                  { label: "택배 완료", value: "5" },
                ]}
              />
            </div>
          </div>
          <div style={{ width: 150, marginLeft: 12 }}>
            <Select
              value={TypeSelect}
              onChange={(e) => {
                setTypeSelect({ label: e.label, value: e.value });
              }}
              options={profile.gm === 1 ? storedata1 : storedata2}
            />
          </div>
          {TypeSelect.value === 0 && (
            <div style={{ width: 120, marginLeft: 12 }}>
              <Select
                value={storeSelect}
                onChange={(e) => setStoreSelect(e)}
                options={storeList}
              />
            </div>
          )}
          {TypeSelect.value === 1 && (
            <div
              style={{
                width: 300,
                marginLeft: 12,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <SearchBox
                value={SearchData.start}
                onChange={(e) =>
                  setSearchData({ ...SearchData, start: e.target.value })
                }
                type="date"
                style={{ marginRight: 12 }}
              ></SearchBox>
              <SearchBox
                value={SearchData.end}
                onChange={(e) =>
                  setSearchData({ ...SearchData, end: e.target.value })
                }
                type="date"
              ></SearchBox>
            </div>
          )}
          {TypeSelect.value === 2 && (
            <div
              style={{
                width: 300,
                marginLeft: 12,
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <SearchBox
                value={SearchData.start}
                onChange={(e) =>
                  setSearchData({ ...SearchData, start: e.target.value })
                }
                type="date"
                style={{ marginRight: 12 }}
              ></SearchBox>
              <SearchBox
                value={SearchData.end}
                onChange={(e) =>
                  setSearchData({ ...SearchData, end: e.target.value })
                }
                type="date"
              ></SearchBox>
            </div>
          )}

          {TypeSelect.value === 3 && (
            <div
              style={{
                width: 300,
                marginLeft: 12,
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <SearchBox
                onChange={(e) => {
                  setSearchData({ ...SearchData, text: e.target.value });
                }}
                type="text"
              ></SearchBox>
            </div>
          )}
          <TopButton>
            <Add onClick={() => _handleUpdate("승인", 1)}>승인</Add>
            <Add onClick={() => _handleUpdate("취소", 2)}>취소</Add>
            <Delete onClick={() => _handleDelete()}>삭제</Delete>
            <Add
              style={{ marginLeft: 15 }}
              onClick={() =>
                exportDataToXlsx({ data: rowData, filename: "개통_추출" })
              }
            >
              엑셀 다운
            </Add>
          </TopButton>
        </Top>
        <DataTable style={{ marginBottom: 300 }}>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>아이디(휴대폰번호)</th>
              <th>이름</th>
              <th>소속</th>
              <th>제품명</th>
              <th>상태</th>
              <th>메모</th>
              <th>개통승인 날짜</th>
              <th>개통신청 날짜</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {rowData?.map((item, idx) => {
              console.log(item);
              return (
                <tr>
                  <LinedTag>
                    <input
                      type="checkbox"
                      checked={item?.validate}
                      onClick={() => _handleSelect(item?.번호)}
                    />
                  </LinedTag>
                  <LinedTag>{item?.이메일}</LinedTag>
                  <LinedTag>{item?.이름}</LinedTag>
                  <LinedTag>{item?.소속}</LinedTag>
                  <LinedTag>{item?.기기명}</LinedTag>
                  <LinedTag>{item?.승인여부}</LinedTag>
                  <LinedTag>{item?.memoadmin?.substring(0, 17)}</LinedTag>
                  <LinedTag>
                    {moment(item?.validateAccpetDay).format("YYYY-MM-DD")}
                  </LinedTag>
                  <LinedTag>
                    {moment(item?.validateOrderDay).format("YYYY-MM-DD")}
                  </LinedTag>
                  <HoverTag>
                    <Link
                      to={{
                        pathname: `/admin/contract/detail/${allOrderListData?.rows[idx]?.id}`,
                        state: allOrderListData?.rows[idx],
                      }}
                    >
                      더보기
                    </Link>
                  </HoverTag>
                </tr>
              );
            })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={allOrderListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};
export default withRouter(Contract);
