import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { Link } from "react-router-dom";
import { GetBannerList, GetPointAct, GetStoreList } from "Datas/swr";
import { deleteBanner, deletePointList } from "Datas/api";
import { Fade } from "react-reveal";
import { NavItem, Pagination } from "react-bootstrap";
import CustomPagination from "Settings/pagination";
import { withRouter } from "react-router-dom";
import Select from "react-select";
import moment from "moment";
import Banner from "../Banner/Banner";
import { useConfirm } from "Settings/util";
const Wrapper = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopLabel = styled.div``;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const Imgskeleton = styled.img`
  max-width: 200px;
  height: 100px;
  border: none;
  resize: both;
  margin: 0 auto;
  border-radius: 5px;
  object-fit: contain;
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const PointAct = ({ profile }) => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);

  // check
  const [select, setSelect] = useState([]);
  const [checked, setChecked] = useState(false);

  //SWR
  const { StoreListData, isLoading, StoreListMutate } = GetStoreList(0);
  const [storeSelect, setStoreSelect] = useState({
    value: "전체",
    label: "전체",
  });

  const [storesSelect, setStoresSelect] = useState({
    value: "전체",
    label: "전체",
  });
  const [storeList, setStoreList] = useState([]);

  const { PointActData, PointActMutate } = GetPointAct(
    storeSelect.value,
    offset,
    storesSelect.value
  );

  const storedata1 = [
    { label: "전체", value: "전체" },
    { label: "수수료", value: 1 },
    { label: "포인트", value: 0 },
  ];

  const [BannerData, setBannerData] = useState([]);

  useEffect(() => {
    if (profile) {
      if (profile.gm == 1) {
        setStoreSelect({ ...storeSelect, value: profile?.storeid });
      }
    }
  }, [profile]);

  useEffect(() => {
    let checkd = BannerData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [BannerData]);

  useEffect(() => {
    if (StoreListData?.rows !== undefined) {
      let Arrays = [];
      if (StoreListData?.rows?.length !== 0) {
        for (let i = 0; i < StoreListData?.rows?.length + 1; i++) {
          if (StoreListData?.rows?.length === i) {
            Arrays.push({
              value: "전체",
              label: "전체",
            });
          } else {
            Arrays.push({
              value: StoreListData?.rows?.[i]?.id,
              label: StoreListData?.rows?.[i]?.name,
            });
          }
        }

        setStoreList(Arrays);
      }
    }
  }, [StoreListData]);

  useEffect(() => {
    if (PointActData !== undefined) {
      let Banners = [...PointActData?.rows];
      setBannerData(Banners);
    }
  }, [PointActData]);

  const _handleSelect = (id) => {
    //셀렉트 메서드를 정렬시켜줌
    let newArray = [...select];
    let temp = [...BannerData];
    temp?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;

        if (!el?.validate) {
          if (newArray.findIndex((item) => item === el.id) === -1) {
            newArray.push(el.id);
          }
        } else {
          newArray = newArray.filter((item) => item !== el.id);
        }
      }
    });

    setBannerData(temp);
    setSelect(newArray);

    //셀렉트처리
  };

  const _handleAllSelct = () => {
    //전체샐렉트동기화
    let data = [...BannerData];
    let resdata;
    let status;
    if (checked) {
      status = false;
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      status = true;
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setBannerData(resdata);
    setChecked(true);
  };

  const deleteItem = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      await deletePointList({ id: select });
      setSelect([]);
      await PointActMutate();
    },
    () => {
      alert("삭제를 취소하였습니다.");
    }
  );

  const _handleDelete = async () => {
    if (select.length === 0) {
      alert("삭제할 상품을 선택해주세요");
      return;
    }

    deleteItem();
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>포인트/수수료 이력관리</TopLabel>
          <TopButton>
            <div style={{ width: 120, marginLeft: 12 }}>
              <Select
                value={storesSelect}
                onChange={(e) => setStoresSelect(e)}
                options={storedata1}
              />
            </div>

            {profile?.gm > 1 && (
              <div style={{ width: 120, marginLeft: 12 }}>
                <Select
                  value={storeSelect}
                  onChange={(e) => setStoreSelect(e)}
                  options={storeList}
                />
              </div>
            )}

            <div
              onClick={_handleDelete}
              style={{
                width: 100,
                height: 40,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "red",
                color: "#fff",
                borderRadius: 7,
                marginLeft: 12,
              }}
            >
              삭제
            </div>
          </TopButton>
        </Top>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>분류</th>
              <th>가맹점명</th>
              <th>사용여부</th>
              <th>포인트/수수료</th>
              <th>내용</th>
              <th>일자</th>
            </tr>
          </thead>
          <tbody>
            {BannerData?.length !== 0 &&
              BannerData?.map((el, idx) => {
                return (
                  <tr key={idx}>
                    <LinedTag>
                      <input
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                        checked={el?.validate}
                      />
                    </LinedTag>
                    <LinedTag>
                      {el?.ptype === 0 ? "포인트" : "누적수수료"}
                    </LinedTag>
                    <LinedTag>
                      {
                        StoreListData?.rows?.filter(
                          (item) => item.id === el.storeid
                        )[0]?.name
                      }
                    </LinedTag>
                    <LinedTag>{el?.otype === false ? "차감" : "부여"}</LinedTag>
                    <LinedTag>{Number(el.point).toLocaleString()}</LinedTag>
                    <LinedTag>{el?.memo}</LinedTag>
                    <LinedTag>
                      {moment(el?.createdAt).format("YYYY-MM-DD")}
                    </LinedTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={PointActData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default withRouter(PointAct);
