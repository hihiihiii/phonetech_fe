import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import { GetCardList, GetTelecomList } from "Datas/swr";
import { Link } from "react-router-dom";
import { deleteCard } from "Datas/api";
import { Fade } from "react-reveal";
import Select from "react-select";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const Imgskeleton = styled.img`
  max-width: 200px;
  height: 100px;
  border: none;
  resize: both;
  margin: 0 auto;
  border-radius: 5px;
  object-fit: contain;
`;

const SelectBox = styled.select`
  margin-left: 15px;
  width: 100px;
  height: 40px;
  padding-left: 10px;
  border: none;
  background-color: #f0f0f0;
  border-radius: 7px;
  font-size: 16px;
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Card = ({ profile }) => {
  const [cardData, setCardData] = useState([]);
  const [telecom, setTelecom] = useState();
  const [teleList, setTeleList] = useState([]);
  const [page, setPage] = useState([]);
  const [offset, setOffset] = useState(0);
  const [checked, setChecked] = useState(false);
  //SWR
  const { TelecomListData } = GetTelecomList();
  const { CardListData, CardListMutate, isLoading } = GetCardList(
    telecom?.label,
    offset
  );

  useEffect(() => {
    if (!isLoading) {
      setCardData(CardListData?.rows);
    }
  }, [CardListData]);
  useEffect(() => {
    let checkd = cardData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [cardData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화

    let data = [...cardData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setCardData(resdata);
    setChecked(true);
  };

  useEffect(() => {
    if (TelecomListData !== undefined) {
      let newArray = [];
      setTelecom({
        value: TelecomListData[0].id,
        label: TelecomListData[0].name,
      });

      if (TelecomListData.length !== 0) {
        for (let i = 0; i < TelecomListData.length; i++) {
          newArray.push({
            value: TelecomListData[i].id,
            label: TelecomListData[i].name,
          });
        }
        setTeleList(newArray);
      }
    }
  }, [TelecomListData]);

  const _handleSelect = (id) => {
    let temp = [...cardData];
    cardData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
        console.log("data");
        console.log(data);
      }
    });
    setCardData(temp);
  };

  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < cardData.length; index++) {
        if (cardData[index]?.validate) {
          let body = {
            id: cardData[index]?.id,
          };
          await deleteCard(body);
        }
      }
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );

  const _handleDelete = async () => {
    await desk();
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel style={{ marginRight: 15 }}>제휴카드</TopLabel>
          <div style={{ width: 200 }}>
            <Select
              onChange={(e) => {
                setTelecom({ label: e.label, value: e.value });
              }}
              value={telecom}
              options={teleList}
            />
          </div>
          <TopButton>
            <Link to={`/admin/card/detail/add/new`}>
              <Add>카드 추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable style={{ minHeight: 500 }}>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>카드 이미지</th>
              <th>카드 명</th>
              <th>할인 (전월 실적 / 월별 할인금)</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {cardData?.length !== 0 &&
              cardData?.map((el, idx) => {
                return (
                  <tr>
                    <LinedTag>
                      <input
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                        checked={el.validate}
                      />
                    </LinedTag>
                    <LinedTag>
                      <Imgskeleton src={el?.images[0]} />
                    </LinedTag>
                    <LinedTag>{el?.name}</LinedTag>
                    <LinedTag>
                      {el?.price?.map((item, idx) => {
                        console.log(item);
                        return (
                          <span style={{ marginRight: 15 }}>
                            {item?.last
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                            /{" "}
                            {item?.discount
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            ,{" "}
                          </span>
                        );
                      })}
                    </LinedTag>
                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/card/detail/${el?.id}/${el?.telecomname}`,
                          state: el,
                        }}
                      >
                        더보기
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={CardListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default Card;
