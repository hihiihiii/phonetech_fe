import React, { useEffect, useState } from "react";
import { sendPushNoti } from "../../../Datas/api";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import { GetPushLogs } from "../../../Datas/swr";
import { deletePushLogs } from "../../../Datas/api";
import moment from "moment";
import { useConfirm } from "../../../Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Pushnoti = () => {
  // States
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(10);
  const [checked, setChecked] = useState(false);
  const [pushData, setPushData] = useState([]);

  // SWR
  const { PushLogsData, PushLogsMutate, isLoading } = GetPushLogs(
    offset,
    limit
  );

  useEffect(() => {
    setPushData(PushLogsData?.rows);
  }, [PushLogsData]);

  useEffect(() => {
    if (pushData?.length !== 0) {
      let checkd = pushData?.every((item) => {
        return item.validate;
      });
      setChecked(checkd);
    } else {
      setChecked(false);
    }
  }, [pushData]);

  // Delete Push Logs
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < pushData.length; index++) {
        if (pushData[index]?.validate) {
          let id = pushData[index]?.id;
          await deletePushLogs({ id: id });
        }
      }
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );

  const _handleDelete = () => {
    desk();
  };

  const _handleSelect = (id) => {
    let temp = [...pushData];
    pushData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
      }
    });
    setPushData(temp);
  };

  const _handleAllSelct = () => {
    let data = [...pushData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setPushData(resdata);
    setChecked(true);
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>푸쉬알림 관리</TopLabel>
          <TopButton>
            <Link to="/admin/pushnoti/detail">
              <Add>추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>제목</th>
              <th>내용</th>
              <th>발송 날짜</th>
            </tr>
          </thead>
          <tbody>
            {pushData?.length !== 0 &&
              pushData?.map((el, idx) => {
                return (
                  <tr>
                    <LinedTag>
                      <input
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                        checked={el?.validate}
                      />
                    </LinedTag>
                    <LinedTag>{el?.title}</LinedTag>
                    <LinedTag>{el?.message}</LinedTag>
                    <LinedTag>
                      {moment(el?.createdAt).format("YYYY-MM-DD")}
                    </LinedTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        {/* <CustomPagination
          data={BoardListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        /> */}
      </Wrapper>
    </Fade>
  );
};

export default Pushnoti;
