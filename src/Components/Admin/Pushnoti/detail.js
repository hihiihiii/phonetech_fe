import React, { useState } from "react";
/* import { Fade } from "react-reveal";
import styled from "styled-components";
import { sendPushNoti } from "../../../Datas/api"; */
import { sendPushNoti } from "../../../Datas/api";
import styled from "styled-components";
import { GetBoardList } from "Datas/swr";
import { Link } from "react-router-dom";
import { deletePost } from "Datas/api";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const InputLabel = styled.div`
  margin-bottom: 5px;
  margin-top: 30px;
`;

const InputMain = styled.input`
  height: 45px;
  border: none;
  width: 100%;
  background: #f4f4f4;
  border-radius: 7px;
`;

const InputTextArea = styled.textarea`
  height: 293px;
  padding: 16px;
  width: 100%;
  border-radius: 7px;
  background-color: #f4f4f4;
  border: none;
`;

const InputMaintext = styled.textarea`
  height: 345px;
  border: none;
  width: 100%;
  background: #f4f4f4;
  border-radius: 7px;
  margin-bottom: 110px;
`;

const Content = styled.div`
  margin: 15px 0 0 20px;
  max-width: 690px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Pushnoti = () => {
  const [datas, setDatas] = useState({
    title: "",
    contents: "",
    url: "",
  });

  const _handleSend = async () => {
    await sendPushNoti(datas);
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>푸쉬알림 전송</TopLabel>
          <TopButton>
            <Add onClick={() => _handleSend()}>발송</Add>
          </TopButton>
        </Top>
        <Content>
          <InputLabel>제목</InputLabel>
          <InputMain
            value={datas?.title}
            onChange={(e) => setDatas({ ...datas, title: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="제목을 입력해주세요"
          />
          <InputLabel>내용</InputLabel>
          <InputTextArea
            value={datas?.contents}
            onChange={(e) => setDatas({ ...datas, contents: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="내용을 입력해주세요"
          />
          <InputLabel>URL</InputLabel>
          <InputMain
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            value={datas?.url}
            onChange={(e) => {
              setDatas({ ...datas, url: e.target.value });
            }}
            placeholder="URL을 입력해주세요"
          />
        </Content>
      </Wrapper>
    </Fade>
  );
};

export default Pushnoti;
