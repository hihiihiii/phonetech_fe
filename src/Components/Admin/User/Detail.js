import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import { withRouter } from "react-router";
import { uploadImage, userUpdate } from "Datas/api";
import { GetuserProfile } from "Datas/swr";
import { Fade } from "react-reveal";

const Wrapper = styled.div`
  margin-bottom: 50px;
`;

const TopLabel = styled.div``;

const Top = styled.div`
  border-bottom: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  //padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
`;

const InputLabel = styled.div`
  margin-bottom: 5px;
  margin-top: 30px;
`;

const InputMain = styled.input`
  height: 45px;
  border: none;
  width: 100%;
  border-radius: 7px;
  padding-left: 15px;
`;

const Images = styled.img`
  height: 345px;
  border: none;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  width: 100%;
  border-radius: 7px;
  //margin-bottom: 110px;
  object-fit: cover;
`;
const InputMaintext = styled.textarea`
  height: 345px;
  border: none;
  width: 100%;
  background: #f4f4f4;
  border-radius: 7px;
  margin-bottom: 110px;
`;

const Content = styled.div``;

const NameCardAdd = styled.label`
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  font-size: 17px;
  background: #6091ed;
  border-radius: 30px;
  padding: 9px 15px;
  color: #fff;
  margin-left: auto;
  @media screen and (max-width: 450px) {
    font-size: 15px;
  }
`;
const UserDetail = ({ location, history }) => {
  console.log(history);

  const { UserProfileData, UserProfileMutate } = GetuserProfile(
    location.state.userData.id
  );
  const [data, setData] = useState({
    name: "",
    email: "",
    useremail: "",
    office: "",
    rank: "",
    image: "",
  });
  const [image, setImage] = useState([]);
  const [imgBase64, setImgBase64] = useState([]);
  console.log(UserProfileData);
  const handleRemoveImage = (idx) => {
    setImage((images) => images.filter((el, index) => index !== idx));
    setImgBase64((imgBase64) => imgBase64.filter((el, index) => index !== idx));
  };
  useEffect(() => {
    if (UserProfileData !== undefined) {
      delete UserProfileData.Store;
      delete UserProfileData.id;
      delete UserProfileData.salt;
      delete UserProfileData.updatedAt;
      delete UserProfileData.tel;
      delete UserProfileData.storeid;
      delete UserProfileData.devicetoken;
      delete UserProfileData.createdAt;
      console.log(UserProfileData);
      let temp = { ...UserProfileData };
      setData(temp);
      setImgBase64([temp.image[0]]);
    }
  }, [UserProfileData]);
  console.log(imgBase64);
  /* console.log(data.image[0]); */

  const handleImage = (e) => {
    let reader = new FileReader();
    reader.onloadend = () => {
      const base64 = reader.result;
      if (base64) {
        setImgBase64([base64.toString()]);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
      setImage([e.target.files[0]]); //경로탈착
    }
  };

  const _userUpdate = async () => {
    //이미지업로드로직
    if (image.length > 0) {
      const ids = await Promise.all(
        image.map((uri) => {
          if (!!uri.id) return uri.id;
          let formData = new FormData();
          formData.append("files", uri);
          return uploadImage(formData);
        })
      );
      data.image = ids;
    }
    await userUpdate(location.state.userData.id, data);
    await UserProfileMutate();
    alert("업데이트 완료!");
    history.goBack();
  };

  return (
    <Fade Button>
      <Wrapper style={{ width: "100%" }}>
        <Top style={{ width: "80%", margin: "0px auto", marginTop: 25 }}>
          <TopLabel>회원정보</TopLabel>
          <TopButton>
            <Add onClick={() => _userUpdate()}>완료</Add>
          </TopButton>
        </Top>
        <Content
          style={{
            width: "100%",
          }}
        >
          <div style={{ width: "80%", margin: "0px auto" }}>
            <table
              id="customers"
              style={{
                marginTop: 24,
                width: "100%",
              }}
            >
              <tr>
                <td>이름</td>
                <td>
                  <InputMain
                    value={data.name}
                    onChange={(e) => setData({ ...data, name: e.target.value })}
                  />
                </td>
              </tr>
              <tr>
                <td>아이디 / 전화번호</td>
                <td>
                  <InputMain
                    value={data.email}
                    onChange={(e) =>
                      setData({ ...data, email: e.target.values })
                    }
                  />
                </td>
              </tr>
              <tr>
                <td>이메일</td>
                <td>
                  <InputMain
                    value={data.useremail}
                    onChange={(e) =>
                      setData({ ...data, useremail: e.target.value })
                    }
                  />
                </td>
              </tr>
              <tr>
                <td>소속</td>
                <td>
                  <InputMain
                    value={data.office}
                    onChange={(e) =>
                      setData({ ...data, office: e.target.value })
                    }
                  />
                </td>
              </tr>
              <tr>
                <td>직급</td>
                <td>
                  <InputMain
                    value={data.rank}
                    onChange={(e) => setData({ ...data, rank: e.target.value })}
                  />
                </td>
              </tr>
              <tr>
                <td>명함</td>
                <td>
                  {imgBase64?.length !== 0 ? (
                    imgBase64.map((item, idx) => {
                      return (
                        <label for="upload-file">
                          <Images src={item} />
                        </label>
                      );
                    })
                  ) : (
                    <div style={{ padding: 8 }}>
                      <NameCardAdd for="upload-file">이미지 추가</NameCardAdd>
                    </div>
                  )}
                </td>
              </tr>
            </table>

            <input
              type="file"
              name="upload-file"
              style={{ display: "none" }}
              onChange={handleImage}
              id="upload-file"
            ></input>
          </div>
        </Content>
      </Wrapper>
    </Fade>
  );
};

export default UserDetail;
