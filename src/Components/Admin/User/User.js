import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import Select from "react-select";

import { GetStoreList, GetuserList } from "Datas/swr";
import { userDelete, userUpdate } from "Datas/api";
import { Link } from "react-router-dom";
import { Fade } from "react-reveal";
import { withRouter } from "react-router";
import Pagination from "react-bootstrap/Pagination";
import PageItem from "react-bootstrap/PageItem";
import { useConfirm } from "Settings/util";
import CustomPagination from "Settings/pagination";
import moment from "moment";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
`;

const Delete = styled.div`
  display: flex;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const SearchBox = styled.input`
  width: 100%;
  height: 40px;
  border: none;

  border-radius: 5px;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.5);
  background-color: #f8f8f8;
  padding: 16px;
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const User = ({ profile }) => {
  const [userSelect, setUserSelect] = useState({ value: 0, label: "비승인" });
  const [page, setPage] = useState([]);
  const [filter, setFilter] = useState(null);
  const [search, setSearch] = useState(null);
  const [checked, setChecked] = useState(false);
  const [offset, setOffset] = useState(0);
  const { StoreListData, isLoading, StoreListMutate } = GetStoreList(0);
  const [storeSelect, setStoreSelect] = useState({
    value: "전체",
    label: "전체",
  });
  const { userListData, userListMutate } = GetuserList(
    storeSelect.value,
    userSelect.value,
    offset,
    filter,
    search
  );
  const [userlist, setUserList] = useState([]);
  const [storeList, setStoreList] = useState([]);

  console.log(profile);

  useEffect(() => {
    if (StoreListData?.rows !== undefined) {
      let Arrays = [];
      if (StoreListData?.rows?.length !== 0) {
        for (let i = 0; i < StoreListData?.rows?.length + 1; i++) {
          if (StoreListData?.rows?.length === i) {
            Arrays.push({
              value: "전체",
              label: "전체",
            });
          } else {
            Arrays.push({
              value: StoreListData?.rows?.[i]?.id,
              label: StoreListData?.rows?.[i]?.name,
            });
          }
        }
        console.log(Arrays);
        setStoreList(Arrays);
      }
    }
  }, [StoreListData]);

  useEffect(() => {
    if (profile?.gm === 1) {
      setStoreSelect({ value: profile?.storeid, label: "가맹주" });
    }
  }, [profile]);
  //유저리스트데이터를 관리해주는 이펙트
  useEffect(async () => {
    if (userListData?.rows !== undefined) {
      console.log("useEffect for restore");
      let temp = [...userListData?.rows];
      await setUserList(temp);
    }
  }, [userListData]);

  useEffect(() => {
    let checkd = userlist.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [userlist]);

  //엑세스승인
  const _userAccess = async () => {
    let data = userlist.filter((item) => item.validate === true);
    let data1 = userlist.filter((item) => item.validate === false);
    let resdata;

    if (data.length !== 0) {
      Promise.all(
        data.map(async (item, idx) => {
          resdata = await userUpdate(item.id, {
            access: true,
            fingertype: true,
          });
          if (resdata === undefined) {
            alert("승인 실패!");
            return;
          }
        })
      ); //요청
      await setUserList(data1);
      alert("승인 성공!");
    }
  };

  //엑세스 삭제
  const _userDelete = () => {
    const desk = useConfirm(
      "삭제하시겠습니까?",
      async () => {
        let data = userlist.filter((item) => item.validate === true);
        let data1 = userlist.filter((item) => item.validate === false);
        let resdata;

        if (data.length !== 0) {
          Promise.all(
            data.map(async (item, idx) => {
              console.log("data.map");
              resdata = await userDelete({ id: item.id });

              if (resdata === undefined) {
                alert("삭제 실패!");
                return;
              }
            })
          ); //요청
          await setUserList(data1);
          alert("삭제 성공!");
        }
      },
      () => {
        alert("삭제 를 취소하였습니다.");
      }
    );
    desk();
    /*   
    } */
    //아이디만 보내기
  };

  const userCheck = (idx) => {
    let data = [...userlist];
    data[idx].validate = !data[idx].validate;
    setUserList(data);
  };

  const _handleAllSelct = () => {
    //전체샐렉트동기화
    console.log("세팅");
    let data = [...userlist];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setUserList(resdata);
    setChecked(true);
  };
  return (
    <Fade Button>
      <Wrapper style={{ height: "100vh" }}>
        <Top>
          <div style={{ display: "flex", alignItems: "center" }}>
            <TopLabel style={{ marginRight: 12 }}>회원현황</TopLabel>

            <p style={{ marginRight: 12 }}>승인여부</p>
            <div style={{ width: 150, marginRight: 12 }}>
              <Select
                onChange={(e) => setUserSelect(e)}
                value={userSelect}
                options={[
                  { value: 1, label: "승인" },
                  { value: 0, label: "비승인" },
                  { value: 2, label: "재인증" },
                ]}
              />
            </div>

            {profile.gm > 1 && (
              <>
                <p style={{ marginRight: 12 }}>가맹점</p>
                <div style={{ width: 150 }}>
                  <Select
                    value={storeSelect}
                    onChange={(e) => setStoreSelect(e)}
                    options={storeList}
                  />
                </div>
              </>
            )}
          </div>

          <TopButton>
            <Add onClick={() => _userAccess()}>승인</Add>
            <Delete onClick={() => _userDelete()}>삭제</Delete>
          </TopButton>
        </Top>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignContent: "center",
            width: "100%",
            marginTop: 12,
          }}
        >
          <div style={{ width: "10%", marginLeft: 12 }}>
            <Select
              onChange={(e) => setFilter(e.value)}
              options={[
                { value: 0, label: "아이디" },
                { value: 1, label: "이름" },
              ]}
            />
          </div>
          <div style={{ width: "50%", marginLeft: 34 }}>
            <SearchBox
              type="text"
              onChange={(e) => {
                if (filter === null) {
                  alert("필터를 설정해주세요");
                  return;
                }
                if (e.target.value === "") {
                  setSearch(null);
                } else {
                  setSearch(e.target.value);
                }
              }}
              value={search}
            ></SearchBox>
          </div>
        </div>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>

              <th>아이디(휴대폰번호)</th>
              <th>이름</th>
              <th>이메일</th>
              <th>소속</th>
              <th>생년월일</th>
              <th>성인여부</th>
              <th>더보기</th>
            </tr>
          </thead>

          {userlist?.map((item, idx) => {
            return (
              <tbody>
                <tr>
                  <LinedTag style={{ width: 80, textAlign: "center" }}>
                    <input
                      type="checkbox"
                      checked={item.validate}
                      onClick={() => userCheck(idx)}
                    />
                  </LinedTag>

                  <LinedTag>{item.email}</LinedTag>
                  <LinedTag>{item.name}</LinedTag>
                  <LinedTag>{item.useremail}</LinedTag>
                  <LinedTag>{item.Store?.name}</LinedTag>
                  <LinedTag>
                    {moment(item.birthday).format("YYYY-MM-DD")}
                  </LinedTag>
                  <LinedTag>{item.child ? "비성인" : "성인"}</LinedTag>
                  <HoverTag>
                    <Link
                      to={{
                        pathname: "/admin/user/Detail",
                        state: {
                          userData: item,
                        },
                      }}
                    >
                      더보기
                    </Link>
                  </HoverTag>
                </tr>
              </tbody>
            );
          })}
        </DataTable>
        <CustomPagination
          data={userListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default withRouter(User);
