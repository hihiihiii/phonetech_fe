import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import {
  GetPlanList,
  GetPlanListCate,
  GetTelecomCategoryList,
  GetTelecomList,
  GettypeTelecomListDatas,
} from "Datas/swr";
import { deletePlan } from "Datas/api";
import Select from "react-select";
import { Link } from "react-router-dom";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Usage = () => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  const [numbers, setNumbers] = useState([]);
  const [PlanData, setPlanData] = useState([]);
  const [telecom, setTelecom] = useState("");
  const [teleList, setTeleList] = useState([]);
  const [cateList, setCateList] = useState([]);
  const [checked, setChecked] = useState(false);
  //SWR

  /* const { PlanListCateData, PlanListCateMutate, isLoading } = GetPlanListCate(
    telecom,
    offset
  ); */

  const _handlerSort = () => {
    const test = PlanData?.map((el, idx) => el?.pricemonth);
    test.sort();
    console.log(test);
  };

  console.log(_handlerSort());

  // console.log(
  //   PlanData?.map((el, idx) => {
  //     console.log(el?.pricemonth);
  //   })
  // );

  const { typeTelecomListDatas, typeTelecomListMutates, isLoadings } =
    GettypeTelecomListDatas(telecom?.label, offset);

  const { TelecomListData } = GetTelecomList();

  useEffect(() => {
    if (!isLoadings) {
      setPlanData(typeTelecomListDatas?.rows);
    }
  }, [typeTelecomListDatas]);

  // 텔레콤 리스트
  useEffect(() => {
    if (TelecomListData !== undefined) {
      setTelecom({
        value: TelecomListData[0]?.id,
        label: TelecomListData[0]?.name,
      });
      let newArray = [];
      if (TelecomListData.length !== 0) {
        for (let i = 0; i < TelecomListData.length; i++) {
          newArray.push({
            value: TelecomListData[i].id,
            label: TelecomListData[i].name,
          });
        }
        setTeleList(newArray);
      }
    }
  }, [TelecomListData]);

  const _handleSelect = (id) => {
    let temp = [...PlanData];
    PlanData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
      }
    });
    setPlanData(temp);
  };
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < PlanData.length; index++) {
        if (PlanData[index]?.validate) {
          let body = PlanData[index]?.id;
          await deletePlan(body);
        }
      }
      await typeTelecomListMutates();
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );
  const _handleDelete = async () => {
    await desk();
  };

  useEffect(() => {
    let checkd = PlanData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [PlanData]);
  const _handleAllSelct = () => {
    //전체샐렉트동기화

    let data = [...PlanData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setPlanData(resdata);
    setChecked(true);
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel style={{ marginRight: 15 }}>요금제 관리</TopLabel>
          <div style={{ width: 200 }}>
            <Select
              onChange={(e) => {
                setTelecom({ label: e.label, value: e.value });
              }}
              value={telecom}
              options={teleList}
            />
          </div>
          <TopButton>
            <Link to="/admin/usage/detail/add/new">
              <Add>요금제 추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable style={{ minHeight: 500 }}>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>요금제 명</th>
              <th>납부 금액</th>
              <th>데이터 용량</th>
              <th>음성통화 용량</th>
              <th>문자 용량</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {/* 여기엔 el?.pricemonth 값 기준으로 값을 뿌려야함 */}
            {PlanData?.length !== 0 &&
              PlanData?.map((el, idx) => {
                return (
                  <tr>
                    <LinedTag>
                      <input
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                        checked={el?.validate}
                      />
                    </LinedTag>
                    <LinedTag>{el?.name}</LinedTag>
                    <LinedTag>{el?.pricemonth?.toLocaleString()}</LinedTag>
                    <LinedTag>{el?.datarem}</LinedTag>
                    <LinedTag>{el?.callrem}</LinedTag>
                    <LinedTag>{el?.smsrem}</LinedTag>
                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/usage/detail/${el?.id}/${el?.telecomname}`,
                          state: el,
                        }}
                      >
                        더보기
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={typeTelecomListDatas}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default Usage;
