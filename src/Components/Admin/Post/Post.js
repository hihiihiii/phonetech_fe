import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { GetBoardList } from "Datas/swr";
import { Link } from "react-router-dom";
import { deletePost } from "Datas/api";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Category = () => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  const [checked, setChecked] = useState(false);
  //SWR
  const { BoardListData, BoardListMutate, isLoading } = GetBoardList(offset);

  const [BoardData, setBoardData] = useState([]);
  console.log(BoardData, "보드데이터");
  useEffect(() => {
    if (!isLoading) {
      setBoardData(BoardListData?.rows);
    }
  }, [BoardListData]);

  const _handleSelect = (id) => {
    let temp = [...BoardData];
    BoardData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
        console.log(data);
      }
    });
    setBoardData(temp);
  };
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < BoardData.length; index++) {
        if (BoardData[index]?.validate) {
          let id = BoardData[index]?.id;

          await deletePost(id);
        }
      }
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );

  const _handleDelete = async () => {
    await desk();
  };

  useEffect(() => {
    let checkd = BoardData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [BoardData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화
    console.log("올셀렉트");

    let data = [...BoardData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setBoardData(resdata);
    setChecked(true);
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>공지사항 관리</TopLabel>
          <TopButton>
            <Link to="/admin/post/detail/add">
              <Add>추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>제목</th>
              <th>내용</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {BoardData?.length !== 0 &&
              BoardData?.map((el, idx) => {
                return (
                  <tr>
                    <LinedTag>
                      <input
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                        checked={el?.validate}
                      />
                    </LinedTag>
                    <LinedTag>{el?.title}</LinedTag>
                    <LinedTag>{el?.contents}</LinedTag>
                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/post/detail/${el?.id}`,
                          state: el,
                        }}
                      >
                        수정
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={BoardListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default Category;
