import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import { GetCategoryList } from "Datas/swr";

import { Link } from "react-router-dom";
import { deleteCategory } from "Datas/api";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Category = () => {
  const [page, setPage] = useState([]);
  const [offset, setOffset] = useState(0);
  const [checked, setChecked] = useState(false);
  //SWR
  const { CategoryListData, CategoryListMutate, isLoading } = GetCategoryList();

  const [CategoryData, setCategoryData] = useState([]);

  useEffect(() => {
    if (!isLoading && CategoryListData?.status !== 400) {
      setCategoryData(CategoryListData);
    }
  }, [CategoryListData]);

  console.log(CategoryListData);

  const _handleSelect = (id) => {
    let temp = [...CategoryData];
    CategoryData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
        console.log(data);
      }
    });
    setCategoryData(temp);
  };
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < CategoryData?.length; index++) {
        if (CategoryData[index]?.validate) {
          let body = {
            id: CategoryData[index]?.id,
          };
          await deleteCategory(body);
        }
      }
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );
  const _handleDelete = async () => {
   await desk();
  };
  useEffect(() => {
    let checkd = CategoryData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [CategoryData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화

    let data = [...CategoryData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setCategoryData(resdata);
    setChecked(true);
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>카테고리 관리</TopLabel>
          <TopButton>
            <Link to="/admin/category/detail/add">
              <Add>추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>메뉴명</th>
              <th>순서</th>
              <th>수정</th>
            </tr>
          </thead>
          <tbody>
            {CategoryData !== undefined &&
              CategoryData?.length !== 0 &&
              CategoryData?.map((el, idx) => {
                return (
                  <tr>
                    <LinedTag>
                      <input
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                        checked={el.validate}
                      />
                    </LinedTag>
                    <LinedTag>{el?.name}</LinedTag>
                    <LinedTag>{el?.order}</LinedTag>
                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/category/detail/${el?.id}`,
                          state: el,
                        }}
                      >
                        수정
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
      </Wrapper>
    </Fade>
  );
};

export default Category;
