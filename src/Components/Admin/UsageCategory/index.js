import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import { GetTelecomCategoryList, GetTelecomList } from "Datas/swr";
import { deletePlan, deleteTelecomCategory } from "Datas/api";
import Select from "react-select";
import { Link } from "react-router-dom";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const UsageCategory = () => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  const [ListData, setListData] = useState([]);
  const [telecom, setTelecom] = useState("");
  const [teleList, setTeleList] = useState([]);
  //SWR
  const [checked, setChecked] = useState(false);
  const { TelecomListData } = GetTelecomList();
  const { TelecomCategoryData, TelecomCategoryMutate, isLoading } =
    GetTelecomCategoryList(telecom?.value, offset);

  useEffect(() => {
    if (!isLoading) {
      setListData(TelecomCategoryData?.rows);
    }
  }, [TelecomCategoryData]);

  useEffect(() => {
    if (TelecomListData !== undefined) {
      setTelecom({
        value: TelecomListData[0].id,
        label: TelecomListData[0].name,
      });
      let newArray = [];
      if (TelecomListData.length !== 0) {
        for (let i = 0; i < TelecomListData.length; i++) {
          newArray.push({
            value: TelecomListData[i].id,
            label: TelecomListData[i].name,
          });
        }
        setTeleList(newArray);
      }
    }
  }, [TelecomListData]);

  console.log(TelecomCategoryData);

  const _handleSelect = (id) => {
    let temp = [...ListData];
    ListData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
      }
    });
    setListData(temp);
  };

  useEffect(() => {
    let checkd = ListData?.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [ListData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화

    let data = [...ListData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setListData(resdata);
    setChecked(true);
  };
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < ListData.length; index++) {
        if (ListData[index]?.validate) {
          let body = ListData[index]?.id;
          await deleteTelecomCategory(body);
        }
      }
      await TelecomCategoryMutate();
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );
  const _handleDelete = async () => {
    await desk();
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel style={{ marginRight: 15 }}>요금제카테고리 관리</TopLabel>
          <div style={{ width: 200 }}>
            <Select
              onChange={(e) => {
                setTelecom({
                  value: e.value,
                  label: e.label,
                });
              }}
              options={teleList}
              value={telecom}
            />
          </div>
          <TopButton>
            <Link to="/admin/usageCategory/detail/add/new">
              <Add>카테고리 추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable style={{ minHeight: 500 }}>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>타이틀</th>
              <th>기가</th>
              <th>혜택</th>
            </tr>
          </thead>
          <tbody>
            {ListData?.length !== 0 &&
              ListData?.map((el, idx) => {
                return (
                  <tr>
                    <LinedTag>
                      <input
                        checked={el?.validate}
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                      />
                    </LinedTag>
                    <LinedTag>{el?.title}</LinedTag>
                    <LinedTag>{el?.giga}</LinedTag>
                    <LinedTag>{el?.option}</LinedTag>
                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/usageCategory/detail/${el?.id}/${el?.telecomid}`,
                          state: el,
                        }}
                      >
                        더보기
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={TelecomCategoryData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default UsageCategory;
