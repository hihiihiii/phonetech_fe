import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import { withRouter } from "react-router";
import { createStore, updateBanner, updateStore } from "Datas/api";
import { GetStoreInfo } from "Datas/swr";
import { Fade } from "react-reveal";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border-bottom: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
  align-items: center;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-left: 30px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const InputLabel = styled.div`
  margin-bottom: 5px;
  margin-top: 30px;
`;

const InputMain = styled.input`
  height: 45px;
  border: none;
  width: 100%;
`;

const Content = styled.div``;

const StoreDetail = ({ match, history, location }) => {
  const { StoreInfoData, StoreInfoMutate, isLoading } = GetStoreInfo(
    match?.params?.id
  );
  const [datas, setDatas] = useState({
    name: "",
    code: "",
    tel: "",
    email: "",
    price: "",
    dc: "",
    slae: "",
    admintype: false,
    changeType: false,
  });
  console.log("디테일");
  useEffect(() => {
    if (!isLoading && match?.params?.id !== "add") {
      setDatas({
        name: StoreInfoData?.name,
        code: StoreInfoData?.code,
        tel: StoreInfoData?.tel,
        email: StoreInfoData?.email,
        price: StoreInfoData?.price,
        dc: StoreInfoData?.dc,
        sale: StoreInfoData?.sale,
        admintype: StoreInfoData?.admintype,
        changeType: true,
      });
    }
  }, [StoreInfoData]);

  const _handleCreate = async () => {
    await createStore(datas);
    history.goBack();
  };

  const _handleUpdate = async () => {
    await updateStore({ id: match?.params?.id, body: datas });
    history.goBack();
  };

  return (
    <Fade Button>
      <Wrapper style={{ width: "100%" }}>
        <Top style={{ width: "80%", margin: "0px auto" }}>
          <TopLabel>가맹점 정보</TopLabel>
          <TopButton>
            {match?.params?.id !== "add" && (
              <span>누적 수수료 {StoreInfoData?.price}원</span>
            )}
            <Add
              onClick={() =>
                match?.params?.id === "add" ? _handleCreate() : _handleUpdate()
              }
            >
              완료
            </Add>
          </TopButton>
        </Top>
        <Content style={{ width: "100%" }}>
          <div style={{ width: "80%", margin: "0px auto" }}>
            <table
              id="customers"
              style={{
                marginTop: 24,
                width: "100%",
              }}
            >
              <tr>
                <td>가맹점명</td>
                <td>
                  <InputMain
                    placeholder="가맹점명을 입력해주세요"
                    value={datas.name}
                    onChange={(e) =>
                      setDatas({ ...datas, name: e.target.value })
                    }
                    style={{ paddingLeft: 15 }}
                  />
                </td>
              </tr>
              <tr>
                <td>이메일</td>
                <td>
                  <InputMain
                    placeholder="이메일을 입력해주세요"
                    value={datas.email}
                    onChange={(e) =>
                      setDatas({ ...datas, email: e.target.value })
                    }
                    style={{ paddingLeft: 15 }}
                  />
                </td>
              </tr>
              <tr>
                <td>인증코드</td>
                <td>
                  <InputMain
                    placeholder="이메일을 입력해주세요"
                    value={datas.code}
                    onChange={(e) =>
                      setDatas({ ...datas, code: e.target.value })
                    }
                    style={{ paddingLeft: 15 }}
                  />
                </td>
              </tr>
              <tr>
                <td>가맹점 누적 수수료</td>
                <td>
                  <InputMain
                    placeholder="가맹점 누적 수수료를 입력해주세요"
                    value={datas.price}
                    onChange={(e) =>
                      setDatas({ ...datas, price: e.target.value })
                    }
                    style={{ paddingLeft: 15 }}
                  />
                </td>
              </tr>
              <tr>
                <td>가맹점 누적 포인트</td>
                <td>
                  <InputMain
                    placeholder="가맹점 누적 포인트를 입력해주세요"
                    value={datas.dc}
                    onChange={(e) => setDatas({ ...datas, dc: e.target.value })}
                    style={{ paddingLeft: 15 }}
                  />
                </td>
              </tr>
              <tr>
                <td>가맹점 수수료</td>
                <td>
                  <InputMain
                    placeholder="가맹점 수수료를 입력해주세요"
                    value={datas.sale}
                    onChange={(e) =>
                      setDatas({ ...datas, sale: e.target.value })
                    }
                    style={{ paddingLeft: 15 }}
                  />
                </td>
              </tr>
              <tr>
                <td>대표번호</td>
                <td>
                  <InputMain
                    placeholder="대표번호를 입력해주세요"
                    value={datas.tel}
                    onChange={(e) =>
                      setDatas({ ...datas, tel: e.target.value })
                    }
                    style={{ paddingLeft: 15 }}
                  />
                </td>
              </tr>
              <tr>
                <td>관리자 모드</td>
                <td>
                  {datas.admintype ? (
                    <div
                      onClick={() => {
                        setDatas({ ...datas, admintype: !datas.admintype });
                      }}
                      style={{
                        backgroundColor: "#33a0df",
                        width: 120,
                        height: 50,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        color: "white",
                        borderRadius: 16,
                        cursor: "pointer",
                      }}
                    >
                      최고관리자
                    </div>
                  ) : (
                    <div
                      onClick={() => {
                        setDatas({ ...datas, admintype: !datas.admintype });
                      }}
                      style={{
                        backgroundColor: "#33a0df",
                        width: 120,
                        height: 50,
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        color: "white",
                        borderRadius: 16,
                        cursor: "pointer",
                      }}
                    >
                      기본운영자
                    </div>
                  )}
                </td>
              </tr>
            </table>
          </div>
        </Content>
      </Wrapper>
    </Fade>
  );
};

export default withRouter(StoreDetail);
