import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { GetStoreList } from "Datas/swr";
import { deleteStore, updateStore } from "Datas/api";
import { Link } from "react-router-dom";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: ${(props) => {
    if (props.color) {
      return "#212121";
    }
    return "#6091ed";
  }};
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.5);
  padding: 10px 20px;
  color: #fff;
  margin-right: 10px;
  border-radius: 10px;

  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.5);
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const InputLeft = styled.input`
  width: 282px;
  height: 40px;
  border-radius: 5px;
  padding: 16px;
  background-color: #f8f8f8;
  border: none;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.5);
  margin-left: 12px;
  margin-right: 12px;
`;
const InputRight = styled.input`
  width: 180px;
  margin-left: 16px;
  margin-right: 16px;
  height: 40px;
  padding: 16px;
  border-radius: 5px;
  border: none;
  box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.5);
  background-color: #f8f8f8;
`;
const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Store = ({ profile }) => {
  const [page, setPage] = useState([]);
  const [offset, setOffset] = useState(0);
  const [checked, setChecked] = useState(false);
  const [sale, setSale] = useState(null);
  const [dc, setDc] = useState(null);
  const [search, setSearch] = useState(null);
  const [select, setSelect] = useState([]);
  //SWR
  const { StoreListData, StoreListMutate, isLoading } = GetStoreList(
    offset,
    search
  );

  const [StoreData, setStoreData] = useState([]);

  console.log(StoreListData);

  useEffect(() => {
    if (!isLoading) {
      setStoreData(StoreListData?.rows);
    }
  }, [StoreListData]);

  const _handleSelect = (id) => {
    //셀렉트 메서드를 정렬시켜줌
    let newArray = [...select];
    let temp = [...StoreData];
    temp?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          isvalidate: !el?.isvalidate,
        };
        temp[idx] = data;

        if (!el?.isvalidate) {
          setDc(el.dc);

          if (newArray.findIndex((item) => item === el.id) === -1) {
            newArray.push(el.id);
          }
        } else {
          newArray = newArray.filter((item) => item !== el.id);

          setSale("");
          setDc("");
          console.log("언체크");
        }
      }
    });

    setStoreData(temp);
    setSelect(newArray);

    //셀렉트처리
  };

  useEffect(() => {
    let checkd = StoreData?.every((item) => {
      return item.isvalidate;
    });
    setChecked(checkd);
  }, [StoreData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화

    let data = [...StoreData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, isvalidate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, isvalidate: true };
      });
    }

    setStoreData(resdata);
    setChecked(true);
  };

  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      await deleteStore({ id: select });
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );

  const _handleDelete = async () => {
    await desk();
  };

  //차감 추가 수정

  const _handlePoint = async (type) => {
    if (select.length > 1) {
      alert("포인트 추가또는 차감은 하나만 선택해주세요");
      return;
    }
    await updateStore({
      id: select,
      body: {
        dc: dc,
        type: type,
        option: "Point",
        formid: select,
      },
    });
    setSelect([]);
    await StoreListMutate();
  };
  useEffect(() => {
    if (search === "") {
      setSearch(null);
    }
  }, [search]);

  const _handleAddPrice = async (type) => {
    if (sale === null) {
      alert("수수료를 입력해주세요");
      return;
    }

    if (select.length > 1) {
      alert("수수료 추가또는 차감은 하나만 선택해주세요");
      return;
    }

    //가맹점 수수료 패치 멀티.
    await updateStore({
      id: select,
      body: {
        type: type,
        sale: sale,
        option: "price",
        formid: select,
      },
    });
    setSelect([]);
    await StoreListMutate();
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>가맹점 현황</TopLabel>
          <TopLabel style={{ marginLeft: 32 }}>가맹점 수수료</TopLabel>
          <InputLeft
            style={{ width: 130 }}
            value={sale}
            onChange={(e) => {
              setSale(e.target.value);
            }}
          />

          <Add
            onClick={() => {
              _handleAddPrice("Add");
            }}
          >
            추가
          </Add>

          <Add
            style={{ backgroundColor: "red" }}
            onClick={() => {
              _handleAddPrice("Miner");
            }}
          >
            환급
          </Add>
          <TopLabel style={{ marginLeft: 24 }}>포인트 입력</TopLabel>
          <InputRight
            value={dc}
            onChange={(e) => {
              setDc(e.target.value);
            }}
          ></InputRight>
          <Add
            onClick={() => {
              _handlePoint("Add");
            }}
          >
            추가
          </Add>
          <Delete
            onClick={() => {
              _handlePoint("Miner");
            }}
          >
            차감
          </Delete>

          <InputLeft
            style={{ width: 130, marginLeft: 100 }}
            value={search}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
          />

          <Add color>검색</Add>

          <TopButton>
            <Link to="/admin/store/detail/add">
              <Add>가맹점 생성</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>가맹점 명</th>
              <th>누적 수수료</th>
              <th>인증코드</th>
              <th>가맹 수수료</th>
              <th>대표번호</th>
              <th>누적 포인트</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {StoreData?.length !== 0 &&
              StoreData?.map((el, idx) => {
                return (
                  <tr>
                    <LinedTag>
                      <input
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                        checked={el?.isvalidate}
                      />
                    </LinedTag>
                    <LinedTag>{el?.name}</LinedTag>
                    <LinedTag>{Number(el?.salea).toLocaleString()}</LinedTag>
                    <LinedTag>{el?.code}</LinedTag>
                    <LinedTag>{Number(el?.sale).toLocaleString()}원</LinedTag>
                    <LinedTag>{el?.tel}</LinedTag>
                    <LinedTag>{el?.dc}p</LinedTag>

                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/store/detail/${el?.id}`,
                        }}
                      >
                        더보기
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={StoreListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default Store;
