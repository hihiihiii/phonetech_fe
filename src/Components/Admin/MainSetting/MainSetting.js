import { GetInforMation } from "Datas/swr";
import React, { useState } from "react";
import { useEffect } from "react";
import { Fade } from "react-reveal";
import styled from "styled-components";
import { sendPushNoti, updateInfo, uploadImage } from "../../../Datas/api";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
`;

const InputLabel = styled.div`
  margin-bottom: 5px;
  margin-top: 30px;
`;

const InputMain = styled.input`
  height: 45px;
  border: none;
  width: 100%;
  background: #f4f4f4;
  border-radius: 7px;
`;

const InputTextArea = styled.textarea`
  height: 293px;
  padding: 16px;
  width: 100%;
  border-radius: 7px;
  background-color: #f4f4f4;
  border: none;
`;

const InputMaintext = styled.textarea`
  height: 345px;
  border: none;
  width: 100%;
  background: #f4f4f4;
  border-radius: 7px;
  margin-bottom: 110px;
`;

const Content = styled.div`
  margin: 15px 0 0 20px;
  max-width: 690px;
`;
const NameCardAdd = styled.label`
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  font-size: 17px;
  background: #6091ed;
  border-radius: 30px;
  padding: 9px 15px;
  color: #fff;
  margin-left: auto;
  @media screen and (max-width: 450px) {
    font-size: 15px;
  }
`;
const Images = styled.div`
  width: 175px;
  height: 30px;
  background-image: ${(props) => {
    if (props.src) {
      return `url(${props.src})`;
    }
  }};
  background-size: contain;
  background-repeat: no-repeat;
`;
const MainSetting = () => {
  const [datas, setDatas] = useState({
    id: "",
    kakaoTalk: "",
    call: "",
    pagename: "",
    image: [],
    ceo: "",
    ceoName: "",
    ceoCode: "",
    TeleCode: "",
    zip: "",
    mainCall: "",
    email: "",
    live: "",
    salesPartner: "",
  });
  const [image, setImage] = useState([]);
  const [imgBase64, setImgBase64] = useState([]);
  const { InfoData, InfoDataMutate, isLoading } = GetInforMation();
  //불러오기로직실행

  useEffect(() => {
    if (!isLoading) {
      setDatas(InfoData[0]);
      setImgBase64(InfoData[0].image);
      console.log(InfoData);
    }
  }, [InfoData]);

  const _handleUpdate = async () => {
    if (
      datas.kakaoTalk === "" ||
      datas.call === "" ||
      datas.pagename === "" ||
      datas.id === "" ||
      datas?.ceo === "" ||
      datas?.ceoName === "" ||
      datas?.ceoCode === "" ||
      datas?.TeleCode === "" ||
      datas?.zip === "" ||
      datas?.mainCall === "" ||
      datas?.email === "" ||
      datas?.live === "" ||
      datas?.salesPartner === ""
    ) {
      alert("모든정보를 입력해주세요");
      return;
    }
    if (image.length > 0) {
      console.log("이미지잇음");
      const ids = await Promise.all(
        image.map((uri) => {
          if (!!uri.id) return uri.id;
          let formData = new FormData();
          formData.append("files", uri);
          return uploadImage(formData);
        })
      );
      datas.image = ids;
    }
    console.log(datas);
    let res = await updateInfo(
      {
        call: datas.call,
        kakaoTalk: datas.kakaoTalk,
        pagename: datas.pagename,
        image: datas.image,
        ceo: datas?.ceo,
        ceoName: datas?.ceoName,
        ceoCode: datas?.ceoCode,
        TeleCode: datas?.TeleCode,
        zip: datas?.zip,
        mainCall: datas?.mainCall,
        email: datas?.email,
        live: datas?.live,
        salesPartner: datas?.salesPartner,
      },
      datas.id
    );
    if (res === undefined) {
      alert("잘못된 데이터입니다다");
      return;
    } else {
      alert("수정완료");
      await InfoDataMutate();
    }
    //업데이트로직실행
    //조건부검증
  };

  const handleImage = (e) => {
    let reader = new FileReader();
    reader.onloadend = () => {
      const base64 = reader.result;
      if (base64) {
        setImgBase64([base64.toString()]);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
      setImage([e.target.files[0]]); //경로탈착
    }
  };
  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>사이트 관리</TopLabel>
          <TopButton>
            <Add onClick={() => _handleUpdate()}>수정</Add>
          </TopButton>
        </Top>
        <Content>
          <InputLabel>사이트명</InputLabel>
          <InputMain
            value={datas?.pagename}
            onChange={(e) => setDatas({ ...datas, pagename: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="페이지 명을 입력해주세요"
          />

          <InputLabel>대포자명</InputLabel>
          <InputMain
            value={datas?.ceoName}
            onChange={(e) =>
              setDatas({
                ...datas,
                ceoName: e.target.value,
                ceo: e.target.value,
              })
            }
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="대포자명을 입력해주세요"
          />
          <InputLabel>사업자 번호</InputLabel>
          <InputMain
            value={datas?.ceoCode}
            onChange={(e) => setDatas({ ...datas, ceoCode: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="사업자 번호를 입력해주세요"
          />
          <InputLabel>통신판매 번호</InputLabel>
          <InputMain
            value={datas?.TeleCode}
            onChange={(e) => setDatas({ ...datas, TeleCode: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="통신판매 번호를 입력해주세요"
          />
          <InputLabel>주소</InputLabel>
          <InputMain
            value={datas?.zip}
            onChange={(e) => setDatas({ ...datas, zip: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="주소를 입력해주세요"
          />
          <InputLabel>대표번호</InputLabel>
          <InputMain
            value={datas?.mainCall}
            onChange={(e) => setDatas({ ...datas, mainCall: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="대표번호를 입력해주세요"
          />
          <InputLabel>이메일</InputLabel>
          <InputMain
            value={datas?.email}
            onChange={(e) => setDatas({ ...datas, email: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="이메일을 입력해주세요"
          />
          <InputLabel>운영시간</InputLabel>
          <InputMain
            value={datas?.live}
            onChange={(e) => setDatas({ ...datas, live: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="운영시간을 입력해주세요"
          />
          <InputLabel>판메 제휴업체 사업자 등록번호</InputLabel>
          <InputMain
            value={datas?.salesPartner}
            onChange={(e) =>
              setDatas({ ...datas, salesPartner: e.target.value })
            }
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="판메 제휴업체 사업자 등록번호를 입력해주세요"
          />

          <InputLabel>카카오톡 연결링크</InputLabel>
          <InputMain
            value={datas?.kakaoTalk}
            onChange={(e) => setDatas({ ...datas, kakaoTalk: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="카카오톡 링크를 입력해주세요"
          />
          <InputLabel>연결 전화번호</InputLabel>
          <InputMain
            value={datas?.call}
            onChange={(e) => setDatas({ ...datas, call: e.target.value })}
            style={{
              paddingLeft: 15,
              marginTop: 12,
            }}
            placeholder="010-0000-0000"
          />
          <InputLabel>사이트 로고</InputLabel>
          {imgBase64?.length !== 0 ? (
            imgBase64.map((item, idx) => {
              return (
                <label for="upload-file">
                  <Images src={item} />
                </label>
              );
            })
          ) : (
            <div style={{ padding: 8 }}>
              <NameCardAdd for="upload-file">이미지 추가</NameCardAdd>
            </div>
          )}
          <input
            type="file"
            name="upload-file"
            style={{ display: "none" }}
            onChange={handleImage}
            id="upload-file"
          ></input>
          <p
            style={{
              color: "gray",
              fontWeight: "bold",
              fontSize: 12,
              marginTop: 12,
            }}
          >
            799 × 138 사이즈의 이미지를 첨부해주세요
          </p>
        </Content>
      </Wrapper>
    </Fade>
  );
};

export default MainSetting;
