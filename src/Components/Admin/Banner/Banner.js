import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { Link } from "react-router-dom";
import { GetBannerList } from "Datas/swr";
import { deleteBanner } from "Datas/api";
import { Fade } from "react-reveal";
import { Pagination } from "react-bootstrap";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopLabel = styled.div``;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const Imgskeleton = styled.img`
  max-width: 200px;
  height: 100px;
  border: none;
  resize: both;
  margin: 0 auto;
  border-radius: 5px;
  object-fit: contain;
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Banner = () => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  //SWR
  const { BannerListData, BannerListMutate, isLoading } = GetBannerList(offset);
  const [checked, setChecked] = useState(false);
  const [BannerData, setBannerData] = useState([]);

  useEffect(() => {
    if (BannerListData !== undefined) {
      let Banners = [...BannerListData?.rows];
      setBannerData(Banners);
    }
  }, [BannerListData]);

  useEffect(() => {
    let checkd = BannerData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [BannerData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화
    console.log("세팅");
    let data = [...BannerData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setBannerData(resdata);
    setChecked(true);
  };

  const _handleSelect = (id) => {
    let temp = [...BannerData];
    BannerData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
        console.log(data);
      }
    });
    setBannerData(temp);
  };
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < BannerData.length; index++) {
        if (BannerData[index]?.validate) {
          let body = {
            id: BannerData[index]?.id,
          };
          await deleteBanner(body);
        }
      }
      await BannerListMutate();
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );
  const _handleDelete = async () => {
    desk();
  };

  console.log(BannerData);
  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>배너관리</TopLabel>
          <TopButton>
            <Link to="/admin/banner/detail/add">
              <Add>배너 추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>이미지</th>
              <th>배너순서</th>
              <th>활성화</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {BannerData?.length !== 0 &&
              BannerData?.map((el, idx) => {
                console.log(el);
                return (
                  <tr key={idx}>
                    <LinedTag>
                      <input
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                        checked={el.validate}
                      />
                    </LinedTag>
                    <LinedTag>
                      <Imgskeleton src={el?.image[0]} />
                    </LinedTag>
                    <LinedTag>{el?.order}</LinedTag>
                    <LinedTag>{el?.ison ? "활성화" : "비활성화"}</LinedTag>
                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/banner/detail/${el?.id}`,
                          state: el,
                        }}
                      >
                        더보기
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={BannerListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default Banner;
