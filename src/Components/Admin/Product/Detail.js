import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import {
  GetAllTypeTelecom,
  GetByIdTypeTelecom,
  GetCategoryList,
  GetProductInfo,
  GetPromotionList,
  GetTelecomList,
} from "Datas/swr";
import Select from "react-select";
import Editor from "Components/Editor";
import { withRouter } from "react-router";
import { Fade } from "react-reveal";
import { createProduct, updateProduct } from "Datas/api";
import { GetUri } from "Settings/imageHandler";
import _ from "lodash";
const Wrapper = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopLabel = styled.div``;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
`;

const ProductImageWrapper = styled.div`
  display: flex;
  overflow-y: scroll;
`;

const ProductImageLabel = styled.div`
  margin-bottom: 5px;
`;

const ProductImageskeleton = styled.img`
  max-width: 335px;
  height: 175px;
  border: none;
  resize: both;
  margin: 0 auto;
  border-radius: 5px;
  object-fit: contain;
  display: flex;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  @media screen and (max-width: 450px) {
    width: 216px;
    height: 113px;
  }
`;

const ProductNameLabel = styled.div`
  margin: 35px 0 5px 0;
`;

const ProductNameInput = styled.input`
  width: 100%;
  border-radius: 7px;
  border: none;
  padding: 0 20px;
  height: 45px;
`;

const ProductFromWrapper = styled.div`
  display: flex;
  /* max-width: 690px; */
  width: 100%;
  & > div:first-child {
    margin-right: 25px;
  }
`;

const ProductFromBtn = styled.div`
  display: flex;
  width: 100%;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  align-items: center;
  justify-content: center;
  padding: 10px;
  border-radius: 10px;
  color: ${(props) => (props.selected === true ? "#fff" : "#333")};
  background: ${(props) => (props.selected === true ? "#6C74F4" : "#F4F4F4")};
`;

const ProductPromotionWrapper = styled.div``;

const ProductPromotion = styled.div`
  width: 690px;
  height: 45px;
  background: #f4f4f4;
  border-radius: 7px;
  border: none;
  padding: 0 20px;
`;

const ProductMemoryWrapper = styled.div`
  width: 690px;
  border-radius: 7px;
  border: none;
`;

const ProductMemoryInput = styled.div`
  display: flex;
  & > input {
    width: 100%;
    height: 45px;
    border-radius: 7px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border: none;
    background: #f4f4f4;
    padding: 0 20px;
  }
  & > span {
    color: #de4949;
    background: #f4f4f4;
    display: flex;
    white-space: nowrap;
    align-items: center;
    border-top-right-radius: 7px;
    border-bottom-right-radius: 7px;
    justify-content: center;
  }
  margin: 15px 0;
`;

const ProductMemoryInputAddBtn = styled.div`
  background: #6c74f4;
  padding: 6px 11px;
  display: flex;
  white-space: nowrap;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  color: #fff;
  //margin-left: 10px;
`;

const Content = styled.div`
  margin: 15px 0 0 20px;
`;

const ProductSupportWrapper = styled.div``;

const ProductSupportDiscuntLabel = styled.div`
  margin-top: 35px;
  margin-bottom: 5px;
`;
const MarkingBox = styled.div`
  cursor: pointer;

  :hover {
    opacity: 0.5;
  }
  background-color: #feb43c;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  border-radius: 24px;
  color: white;
  font-weight: bold;
  height: 35px;
`;

const ProductSupportDiscountInput = styled.div`
  background: #f4f4f4;
  height: 50px;
  display: flex;
  align-items: center;
  max-width: 690px;
  border-radius: 7px;
  & > input {
    height: 100%;
    border: none;
    background: transparent;
    padding: 0 15px;
  }
  & > span:nth-child(3) {
    color: red;
    margin-left: auto;
  }
  padding: 0 20px;
`;

const ProductSupportCOM = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 15px;
  width: 25%;
  max-width: 690px;
  & > input {
    height: 45px;
    border: none;
    background: #f4f4f4;
    border-radius: 7px;
  }
`;

const ProductSupportCOMLabel = styled.div``;

const ProductSupportDC = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 690px;
  width: 65%;
  & > input {
    height: 45px;
    border: none;
    background: #f4f4f4;
    border-radius: 7px;
  }
`;

const Flex = styled.div`
  display: flex;
  margin-top: 25px;
  align-items: center;
  max-width: 690px;
`;

const ProductSupportDCLabel = styled.div``;

const ProductSupportADD = styled.div`
  width: 10%;
  display: flex;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  margin-left: 15px;
  background: #6c74f4;
  align-items: center;
  justify-content: center;
  padding: 6px 11px;
  color: #fff;
  border-radius: 7px;
`;

const DetailedInfoLabel = styled.div`
  margin-top: 25px;
`;

const MarkDown = styled.div`
  width: 970px;
  height: 364px;
  margin-bottom: 110px;
`;

const NameCardAdd = styled.label`
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  font-size: 15px;
  background: #fab43c;
  border-radius: 30px;
  padding: 7px 14px;
  color: #fff;
  margin-left: 15;
  @media screen and (max-width: 450px) {
    font-size: 12px;
  }
`;

const storageData = [
  { label: "64GB", value: 1 },
  { label: "128GB", value: 2 },
  { label: "256GB", value: 3 },
  { label: "512GB", value: 4 },
  { label: "1TB", value: 5 },
  { label: "직접입력", value: 6 },
];

const opttypeData = [
  { label: "신청 및 개통문의", value: 0 },
  { label: "배송문의", value: 1 },
  { label: "취소 교환 반품문의", value: 2 },
];

const InputTextArea = styled.textarea`
  height: 293px;

  width: 100%;
  border-radius: 7px;

  border: none;
`;

const Product = ({ location, history }) => {
  //이미지 업로더등록
  const [data, setData] = useState({
    name: "",
    images: null,
    marketprice: 0,
    transtype: [],
    giga: "",
    order: 0, //순서
  });

  const [HardData, setHardData] = useState([]);
  const [ClolorData, setColorData] = useState([]);
  const [ArrayDatas, setArrayDatas] = useState();
  const [ArrayDatasQa, setArrayDatasQa] = useState({
    contents: "",
    title: "",
    opttype: 0,
  });
  const [typeData, setTypeData] = useState();
  const [cateList, setCateList] = useState([]);
  //요금제데이터
  const [productQa, setProductQa] = useState([]);
  const { TypeTelecomListData, TypeTelecomListMutate } = GetAllTypeTelecom();
  const [prList, setPrList] = useState([]);
  const [initialType, setInitialType] = useState({});
  const { CategoryListData } = GetCategoryList(0);
  const { PromotionListData } = GetPromotionList(0);

  const [image, setImage] = useState([]);
  const [filesdata, setfilesCheck] = useState([]);
  const [imgBase64, setImgBase64] = useState([]);
  const [productBoardData, setProductBoardData] = useState(null);
  const [type, setType] = useState();
  const [telecomData, setTelecomData] = useState([]);
  const [typeListData, setTypeListData] = useState([]);

  const { ProductInfoData, ProductInfoMutate, isLoading } = GetProductInfo(
    location?.state?.id
  );

  const [opengiga, setOpenGiga] = useState(false);
  const { ByIdTypeTeleComData, ByIdTypeTeleComMutate } = GetByIdTypeTelecom(
    location?.state?.id
  );

  const [files, setFiles] = useState([]);

  const onEditorChange = (value) => {
    setProductBoardData(value);
  };

  const onFilesChange = (files) => {
    setFiles(files);
  };
  //주제별 카테고리 출력

  const _handleClolorData = () => {
    if (!ArrayDatas.clname) {
      alert("올바른 값을 입력해주세요");
      return;
    }
    let data = [...ClolorData];
    setColorData([...data, { price: 0, name: ArrayDatas.clname }]);
    setArrayDatas({ ...ArrayDatas, clprice: "", clname: "" });
  };

  const _handleQaData = () => {
    if (ArrayDatasQa === undefined) {
      alert("올바른 값을 입력해주세요");
      return;
    }
    if (ArrayDatasQa.title === "" || ArrayDatasQa.contents === "") {
      alert("올바른 값을 입력해주세요");
      return;
    }

    let data = [...productQa];
    setProductQa([
      ...data,
      {
        contents: ArrayDatasQa.contents,
        title: ArrayDatasQa.title,
        opttype: ArrayDatasQa.opttype.value,
      },
    ]);
    setArrayDatasQa({ ...ArrayDatasQa, contents: "", title: "" });
  };

  const handleImage = (e) => {
    let reader = new FileReader();
    reader.onloadend = () => {
      const base64 = reader.result;
      if (base64) {
        setImgBase64([...imgBase64, base64.toString()]);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
      setImage([...image, e.target.files[0]]); //경로탈착
    }
  };

  //type='Edit'일시 productInfo 데이터 할당
  useEffect(() => {
    if (!isLoading && location?.state?.type === "Edit") {
      setData({
        name: ProductInfoData?.name,
        images: ProductInfoData?.images,
        marketprice: ProductInfoData?.marketprice,
        memoadmin: ProductInfoData?.memoadmin,
        giga: ProductInfoData?.giga,
        price: ProductInfoData?.price,
        order: ProductInfoData?.order,
      });
      setImgBase64(ProductInfoData?.images);

      setProductBoardData(ProductInfoData?.ProductBoard?.contents);

      setProductQa(ProductInfoData?.ProductQas);

      const totalHard = [];
      ProductInfoData?.HardPrices?.map((el, idx) => {
        const tempData = {
          name: el?.name,
          price: el?.price,
        };
        totalHard.push(tempData);
      });
      setHardData(totalHard);

      const totalColor = [];
      ProductInfoData?.ColorPrices?.map((el, idx) => {
        const tempData = {
          name: el?.name,
          price: el?.price,
        };
        totalColor.push(tempData);
      });
      setColorData(totalColor);

      const totalData = [];
      ProductInfoData?.telecom_product_key_product?.map((el, idx) => {
        const tempData = {
          TelecomId: el?.telecom_product?.TelecomId,
          dc_move_price: el?.telecom_product?.dc_move_price,
          dc_change_price: el?.telecom_product?.dc_change_price,
          dc_sales_price: el?.telecom_product?.dc_sales_price,
          dc_chooses_price: el?.telecom_product?.dc_chooses_price,
        };
        totalData.push(tempData);
      });
      setTelecomData(totalData);

      if (ProductInfoData?.Promotion?.length !== 0) {
        setType(false);
        setTypeData({ type: "category", id: ProductInfoData?.Category?.id });
        setInitialType({
          label: ProductInfoData?.Category?.name,
          value: ProductInfoData?.Category?.id,
        });
      } else {
        setType(true);
        setTypeData({ type: "promotion", id: ProductInfoData?.Promotion?.id });
        setInitialType({
          label: ProductInfoData?.Promotion?.name,
          value: ProductInfoData?.Promotion?.id,
        });
      }
    }
  }, [location?.state, isLoading]);

  //초기로딩당시에 리스트 데이터를 만든다
  useEffect(() => {
    if (CategoryListData !== undefined) {
      let newArray = [];
      if (CategoryListData.length !== 0) {
        for (let i = 0; i < CategoryListData.length; i++) {
          newArray.push({
            value: CategoryListData[i].id,
            label: CategoryListData[i].name,
          });
        }
        setCateList(newArray);
      }
    }
  }, [CategoryListData]);
  console.log(ByIdTypeTeleComData, "적용중데이터");
  console.log(TypeTelecomListData, "타입텔레콤데이터");
  useEffect(() => {
    if (
      TypeTelecomListData !== undefined &&
      ByIdTypeTeleComData !== undefined
    ) {
      let newArray = [];
      if (TypeTelecomListData.length !== 0) {
        if (location?.state?.type !== "Add") {
          for (let i = 0; i < TypeTelecomListData.length; i++) {
            //필터링구간활용
            if (ByIdTypeTeleComData.length < i) {
              newArray.push({
                TypeTelecomId: TypeTelecomListData[i].id, //요금제아이디
                ProductId: null, //프로덕트아이디  선택해야 동기화
                name: TypeTelecomListData[i].name, //텔레콤네임
                telecomid: TypeTelecomListData[i].telecomid, //텔레콤아이디 분류
                dc_sales_price: 0, // 공시지원 할인금
                dc_sales_mnp_price: 0, //공시지원 mnp 프라이스
                dc_sales_change_price: 0, //공시지원 기변 프라이스
                dc_chooses_price: 0, //선택약정 할인금
                dc_choose_mnp_price: 0, //선택약정 mnp 가격
                dc_choose_change_price: 0, //선택약정 기변금
                validate: false, //선택사항 디폴트 출력
              });
              continue;
            }
            if (
              ByIdTypeTeleComData[i]?.TypeTelecomId ===
              TypeTelecomListData[i]?.id
            ) {
              console.log("같은값확인");
              console.log(ByIdTypeTeleComData[i]);
              //같은아이디일경우
              newArray.push({
                TypeTelecomId: TypeTelecomListData[i].id, //요금제아이디
                ProductId: null, //가져올부분
                name: TypeTelecomListData[i].name, //텔레콤네임
                telecomid: ByIdTypeTeleComData[i].telecomid, //텔레콤아이디 분류
                dc_sales_price: ByIdTypeTeleComData[i].dc_sales_price, // 공시지원 할인금 이부분은 가져오기
                dc_sales_mnp_price: ByIdTypeTeleComData[i].dc_sales_mnp_price, //공시지원 mnp 프라이스 이부분 가져오기
                dc_sales_change_price:
                  ByIdTypeTeleComData[i].dc_sales_change_price, //공시지원 기변 프라이스
                dc_chooses_price: ByIdTypeTeleComData[i].dc_chooses_price, //선택약정 할인금
                dc_choose_mnp_price: ByIdTypeTeleComData[i].dc_choose_mnp_price, //선택약정 mnp 가격
                dc_choose_change_price:
                  ByIdTypeTeleComData[i].dc_choose_change_price, //선택약정 기변금
                validate: ByIdTypeTeleComData[i].validate, //선택사항 디폴트 출력
              });
            } else {
              newArray.push({
                TypeTelecomId: TypeTelecomListData[i].id, //요금제아이디
                ProductId: null, //프로덕트아이디  선택해야 동기화
                name: TypeTelecomListData[i].name, //텔레콤네임
                telecomid: TypeTelecomListData[i].telecomid, //텔레콤아이디 분류
                dc_sales_price: 0, // 공시지원 할인금
                dc_sales_mnp_price: 0, //공시지원 mnp 프라이스
                dc_sales_change_price: 0, //공시지원 기변 프라이스
                dc_chooses_price: 0, //선택약정 할인금
                dc_choose_mnp_price: 0, //선택약정 mnp 가격
                dc_choose_change_price: 0, //선택약정 기변금
                validate: false, //선택사항 디폴트 출력
              });
            }
          }
        } else {
          for (let i = 0; i < TypeTelecomListData.length; i++) {
            newArray.push({
              TypeTelecomId: TypeTelecomListData[i].id, //요금제아이디
              ProductId: null, //프로덕트아이디  선택해야 동기화
              name: TypeTelecomListData[i].name, //텔레콤네임
              telecomid: TypeTelecomListData[i].telecomid, //텔레콤아이디 분류
              dc_sales_price: 0, // 공시지원 할인금
              dc_sales_mnp_price: 0, //공시지원 mnp 프라이스
              dc_sales_change_price: 0, //공시지원 기변 프라이스
              dc_chooses_price: 0, //선택약정 할인금
              dc_choose_mnp_price: 0, //선택약정 mnp 가격
              dc_choose_change_price: 0, //선택약정 기변금
              validate: false, //선택사항 디폴트 출력
            });
          }
        }
        setTypeListData(newArray);
        //데이터세팅
      }
    }
  }, [TypeTelecomListData, ByIdTypeTeleComData]);
  console.log(ByIdTypeTeleComData);
  console.log(TypeTelecomListData);
  useEffect(() => {
    if (PromotionListData !== undefined) {
      console.log("PromotionListData?.rows");
      console.log(PromotionListData?.rows);
      let newArray = [];
      if (PromotionListData?.rows?.length !== 0) {
        for (let i = 0; i < PromotionListData?.rows?.length; i++) {
          newArray.push({
            value: PromotionListData?.rows[i].id,
            label: PromotionListData?.rows[i].title,
          });
        }
        setPrList(newArray);
      }
    }
  }, [PromotionListData]);

  const _handleChnnel = (type, id) => {
    setTypeData({ type: type, id: id });
  };

  const _handleDelete = (idx1) => {
    let temp = [...HardData];
    let data = temp.filter((item, idx) => idx !== idx1);
    setHardData(data);
  };

  const _handleDeleteColor = (idx1) => {
    let temp = [...ClolorData];
    let data = temp.filter((item, idx) => idx !== idx1);
    setColorData(data);
  };

  const _handleDeleteproductQa = (idx1) => {
    let temp = [...productQa];
    let data = temp.filter((item, idx) => idx !== idx1);
    setProductQa(data);
  };

  const _handleAddHard = () => {
    if (!ArrayDatas.hdname || !ArrayDatas.hdname) {
      alert("올바른 값을 입력해주세요");
      return;
    }
    let check = HardData.findIndex((item) => item.name === ArrayDatas.hdname);
    if (check !== -1) {
      alert("이미 존재하는 GB 설정입니다");
      return;
    }
    let data = [...HardData];
    //정렬함수
    let sortData = [
      ...data,
      { price: ArrayDatas.hdprice, name: ArrayDatas.hdname },
    ];
    sortData = sortData.sort((a, b) => {
      return a.name.replace("GB", "") - b.name.replace("GB", "");
    });
    setHardData(sortData);
    setArrayDatas({ ...ArrayDatas, hdprice: "", hdname: "" });
  };

  const handleRemoveImage = (idx) => {
    setImage((images) => images.filter((el, index) => index !== idx));
    setImgBase64((imgBase64) => imgBase64.filter((el, index) => index !== idx));
  };

  console.log(location?.state?.type);

  const _handleSubmit = async () => {
    if (typeData === undefined) {
      alert("카테고리 또는 프로모션을 선택해주세요");
      return;
    }
    let productdata = data;
    if (typeData.type === "category") {
      productdata.categoryid = typeData.id;
    } else {
      productdata.promotionid = typeData.id;
    }
    if (location?.state?.type === "Add") {
      if (image.length === 0) {
        alert("이미지를 등록해주세요");
        return;
      }
    }
    if (image?.length !== 0) {
      const { url } = await GetUri({ image: image });
      productdata.images = url;
    } else {
      productdata.images = imgBase64;
    }
    //타입리스트 검증
    //유효성검증

    let telele1Type = false;
    let telele2Type = false;
    let telele3Type = false;
    telele1Type = typeListData.findIndex((item, idx) => {
      if (item.telecomid === 1) {
        if (item.validate === true) {
          return true;
        }
      }
    });
    telele2Type = typeListData.findIndex((item, idx) => {
      if (item.telecomid === 2) {
        if (item.validate === true) {
          return true;
        }
      }
    });
    telele3Type = typeListData.findIndex((item, idx) => {
      if (item.telecomid === 3) {
        if (item.validate === true) {
          return true;
        }
      }
    });
    if (telele1Type === -1) {
      alert("LG 기본요금제를 선택해주세요");
      return;
    }
    if (telele2Type === -1) {
      alert("SKT 기본요금제를 선택해주세요");
      return;
    }
    if (telele3Type === -1) {
      alert("KT 기본 요금제를 선택해주세요");
      return;
    }
    if (data.name === "") {
      alert("상품명을 입력해주세요");
      return;
    }
    if (data.giga === "") {
      alert("5G/LTE 를 입력해주세요");
      return;
    }
    if (data.marketprice === 0) {
      alert("출고가를 설정해주세요");
      return;
    }
    if (HardData.length === 0) {
      alert("하드용량을 설정해주세요");
      return;
    }
    if (ClolorData.length === 0) {
      alert("색상 데이터를 설정해주세요");
      return;
    }
    let body = {
      productdata: productdata, //하드
      hardData: HardData, //기기
      colorData: ClolorData, //색상
      productBoardData: productBoardData, //콘텐츠
      telecoms: typeListData,
      productQaData: productQa, //QA 필드
    };
    if (location?.state?.type === "Add") {
      console.log(body);
      await createProduct(body);
    } else {
      await updateProduct(ProductInfoData?.id, body);
    }
    history.goBack();
    await ProductInfoMutate();
  };

  useEffect(() => {
    console.log("타입이 변경되었습니다");
    //타입값에 기반하여서 가져온다
  }, [type]);

  console.log(productBoardData);
  const _handleChange = (e, idx) => {
    //해당 네임값 기준으로 벨류 분산
    let types = [...typeListData];
    let datasA = { ...typeListData[idx], [e.target.name]: e.target.value };
    types[idx] = datasA;
    setTypeListData(types);
  };

  // const inputPriceFormat = (str) => {
  //   const comma = (str) => {
  //     str = String(str);
  //     return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, "$1,");
  //   };
  //   const uncomma = (str) => {
  //     str = String(str);
  //     return str.replace(/[^\d]+/g, "");
  //   };
  //   return comma(uncomma(str));
  // };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>상품관리</TopLabel>
          <TopButton onClick={() => _handleSubmit()}>
            <Add>
              {location?.state?.type === "Add" ? "상품 추가" : "상품 수정"}
            </Add>
          </TopButton>
        </Top>

        <Content
          style={{
            width: "100%",
          }}
        >
          <div style={{ width: "80%", margin: "0px auto" }}>
            <table
              id="customers"
              style={{
                marginTop: 24,
                width: "100%",
              }}
            >
              <tr>
                <td>상품 이미지</td>
                <td style={{ padding: 10 }}>
                  <input
                    type="file"
                    name="upload-file"
                    style={{ display: "none" }}
                    onChange={handleImage}
                    id="upload-file"
                  ></input>
                  {imgBase64 !== undefined &&
                  imgBase64 !== null &&
                  imgBase64?.length !== 0 ? (
                    <ProductImageWrapper>
                      {imgBase64 &&
                        imgBase64.map((item, idx) => {
                          return (
                            <ProductImageskeleton
                              onClick={() => handleRemoveImage(idx)}
                              src={item}
                            ></ProductImageskeleton>
                          );
                        })}
                    </ProductImageWrapper>
                  ) : (
                    <NameCardAdd for="upload-file">추가</NameCardAdd>
                  )}
                </td>
              </tr>
              <tr>
                <td>상품 명</td>
                <td>
                  <ProductNameInput
                    value={data.name}
                    onChange={(e) => setData({ ...data, name: e.target.value })}
                  />
                </td>
              </tr>
              <tr>
                <td>5G/LTE</td>
                <td>
                  <ProductNameInput
                    value={data.giga}
                    onChange={(e) => setData({ ...data, giga: e.target.value })}
                  />
                </td>
              </tr>
              <tr>
                <td>상품 소속</td>
                <td style={{ padding: 10 }}>
                  <ProductFromWrapper>
                    <ProductFromBtn
                      onClick={() => setType(true)}
                      selected={type === true ? true : false}
                    >
                      프로모션
                    </ProductFromBtn>
                    <ProductFromBtn
                      onClick={() => setType(false)}
                      selected={type === true ? false : true}
                    >
                      카테고리
                    </ProductFromBtn>
                  </ProductFromWrapper>
                </td>
              </tr>
              <tr>
                <td>{type === true ? "프로모션" : "카테고리"}</td>
                <td style={{ padding: 10 }}>
                  {type === true ? (
                    <ProductPromotionWrapper style={{ width: "100%" }}>
                      <Select
                        placeholder={
                          location?.state?.type === "Edit"
                            ? initialType.label
                            : "선택해주세요"
                        }
                        onChange={(e) => _handleChnnel("promotion", e.value)}
                        options={prList}
                      />
                    </ProductPromotionWrapper>
                  ) : (
                    <ProductPromotionWrapper style={{ width: "100%" }}>
                      <Select
                        placeholder={
                          location?.state?.type === "Edit"
                            ? initialType.label
                            : "선택해주세요"
                        }
                        onChange={(e) => _handleChnnel("category", e.value)}
                        options={cateList}
                      />
                    </ProductPromotionWrapper>
                  )}
                </td>
              </tr>
              <tr>
                <td>출고가격</td>
                <td>
                  <ProductNameInput
                    value={data.marketprice.toLocaleString()}
                    type="number"
                    onChange={(e) =>
                      setData({ ...data, marketprice: e.target.value })
                    }
                  />
                </td>
              </tr>
              <tr>
                <td>출력순서</td>
                <td>
                  <ProductNameInput
                    value={data.order}
                    type="number"
                    onChange={(e) =>
                      setData({ ...data, order: e.target.value })
                    }
                  />
                </td>
              </tr>
            </table>

            <ProductNameLabel>기기 용량</ProductNameLabel>
            <div
              style={{
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  width: "95%",
                }}
              >
                <ProductMemoryInput
                  style={{
                    marginRight: 12,
                    width: 200,
                  }}
                >
                  <Select
                    placeholder="용량"
                    onChange={(e) => {
                      if (e.label === "직접입력") {
                        //옵션온
                        setOpenGiga(true);
                      } else {
                        setOpenGiga(false);
                        setArrayDatas({
                          ...ArrayDatas,
                          hdname: e.label,
                        });
                      }
                    }}
                    options={storageData}
                  />
                </ProductMemoryInput>
                {opengiga && (
                  <>
                    <ProductMemoryInput style={{ marginRight: 12 }}>
                      <input
                        placeholder="용량GB"
                        onChange={(e) =>
                          setArrayDatas({
                            ...ArrayDatas,
                            hdname: e.target.value,
                          })
                        }
                        style={{
                          border: "none",
                          backgroundColor: "none",
                        }}
                      />
                    </ProductMemoryInput>
                  </>
                )}
                <input
                  style={{
                    width: "45%",
                    height: 40,
                    border: "none",
                    backgroundColor: "#f0f0f0",
                    paddingLeft: 15,
                    borderRadius: 5,
                  }}
                  placeholder="출고가"
                  value={ArrayDatas?.hdprice}
                  onChange={(e) =>
                    setArrayDatas({
                      ...ArrayDatas,
                      hdprice: e.target.value,
                    })
                  }
                />
              </div>
              <ProductMemoryInputAddBtn
                style={{ fontSize: 15, width: 100, height: 40 }}
                onClick={() => _handleAddHard()}
              >
                추가
              </ProductMemoryInputAddBtn>
            </div>
            <table
              id="customers"
              style={{
                marginTop: 24,
                width: "100%",
              }}
            >
              <thead>
                <tr>
                  <td>용량</td>
                  <td>출고가</td>
                  <td>삭제</td>
                </tr>
              </thead>
              <tbody>
                {HardData.map((item, idx) => {
                  return (
                    <tr>
                      <td>
                        <input
                          value={item?.name}
                          placeholder="용량"
                          disabled={true}
                          style={{
                            border: "none",
                            backgroundColor: "none",
                          }}
                        />
                      </td>
                      <td>
                        <input
                          style={{
                            border: "none",
                            backgroundColor: "#fff",
                          }}
                          placeholder="가격"
                          value={item?.price.toLocaleString()}
                          disabled={true}
                        />
                      </td>
                      <td>
                        <ProductMemoryInputAddBtn
                          style={{ backgroundColor: "red" }}
                          onClick={() => _handleDelete(idx)}
                        >
                          삭제
                        </ProductMemoryInputAddBtn>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            <ProductNameLabel>기기 색상</ProductNameLabel>
            <div
              style={{
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  width: "95%",
                }}
              >
                <input
                  style={{
                    width: "45%",
                    height: 40,
                    marginRight: 15,
                    border: "none",
                    backgroundColor: "#f0f0f0",
                    paddingLeft: 15,
                    borderRadius: 5,
                  }}
                  value={ArrayDatas?.clname}
                  placeholder="색상"
                  onChange={(e) =>
                    setArrayDatas({ ...ArrayDatas, clname: e.target.value })
                  }
                />
              </div>
              <ProductMemoryInputAddBtn
                style={{ fontSize: 15, width: 100, height: 40 }}
                onClick={() => _handleClolorData()}
              >
                추가
              </ProductMemoryInputAddBtn>
            </div>
            <table
              id="customers"
              style={{
                marginTop: 24,
                width: "100%",
              }}
            >
              <thead>
                <tr>
                  <td>색상</td>

                  <td>삭제</td>
                </tr>
              </thead>
              <tbody>
                {ClolorData.map((item, idx) => {
                  return (
                    <tr>
                      <td>
                        <input
                          style={{
                            border: "none",
                          }}
                          value={item?.name}
                          placeholder="색상"
                          disabled={true}
                        />
                      </td>
                      <td></td>
                      <td>
                        <ProductMemoryInputAddBtn
                          style={{ backgroundColor: "red" }}
                          onClick={() => _handleDeleteColor(idx)}
                        >
                          삭제
                        </ProductMemoryInputAddBtn>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            <ProductNameLabel>요금제 별 설정</ProductNameLabel>

            <table
              id="customers"
              style={{
                marginTop: 24,
                width: "100%",
              }}
            >
              <thead>
                <tr>
                  <td colspan="8" style={{ textAlign: "center" }}>
                    SKT
                  </td>
                </tr>
              </thead>
              <thead>
                <tr>
                  <td style={{ textAlign: "center" }}>요금제</td>
                  <td style={{ textAlign: "center" }}>공시지원</td>
                  <td style={{ textAlign: "center" }}>DC MNP</td>
                  <td style={{ textAlign: "center" }}>DC 기변</td>
                  <td style={{ textAlign: "center" }}>선택약정</td>
                  <td style={{ textAlign: "center" }}>DC MNP</td>
                  <td style={{ textAlign: "center" }}>DC 기변</td>
                  <td style={{ textAlign: "center" }}>선택</td>
                </tr>
              </thead>
              <tbody>
                {typeListData.map((item, idx) => {
                  if (item.telecomid !== 1) {
                    return;
                  }
                  return (
                    <tr>
                      <td style={{ textAlign: "center" }}>{item?.name}</td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_price.toLocaleString()}
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          name="dc_sales_price"
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_mnp_price.toLocaleString()}
                          name="dc_sales_mnp_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_change_price.toLocaleString()}
                          name="dc_sales_change_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_chooses_price.toLocaleString()}
                          name="dc_chooses_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_choose_mnp_price.toLocaleString()}
                          name="dc_choose_mnp_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_choose_change_price.toLocaleString()}
                          name="dc_choose_change_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td
                        onClick={() => {
                          let Cover = [...typeListData];
                          //기존의 체크여부확인후 체크값제거
                          for (let i = 0; i < Cover.length; i++) {
                            if (Cover[i].telecomid !== 1) {
                              continue;
                            }
                            if (i === idx) {
                              Cover[i].validate = true;
                            } else {
                              Cover[i].validate = false;
                            }
                          }

                          setTypeListData(Cover);
                        }}
                        style={{ textAlign: "center", width: "120px" }}
                      >
                        <MarkingBox style={{ marginRight: -24 }}>
                          {item?.validate === true ? "선택됨" : "선택"}
                        </MarkingBox>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <table
              id="customers"
              style={{
                marginTop: 24,
                width: "100%",
              }}
            >
              <thead>
                <tr>
                  <td colspan="8" style={{ textAlign: "center" }}>
                    KT
                  </td>
                </tr>
              </thead>
              <thead>
                <tr>
                  <td style={{ textAlign: "center" }}>요금제</td>
                  <td style={{ textAlign: "center" }}>공시지원</td>
                  <td style={{ textAlign: "center" }}>DC MNP</td>
                  <td style={{ textAlign: "center" }}>DC 기변</td>
                  <td style={{ textAlign: "center" }}>선택약정</td>
                  <td style={{ textAlign: "center" }}>DC MNP</td>
                  <td style={{ textAlign: "center" }}>DC 기변</td>
                  <td style={{ textAlign: "center" }}>선택</td>
                </tr>
              </thead>
              <tbody>
                {typeListData.map((item, idx) => {
                  if (item.telecomid !== 2) {
                    return;
                  }
                  return (
                    <tr>
                      <td style={{ textAlign: "center" }}>{item?.name}</td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_price.toLocaleString()}
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          name="dc_sales_price"
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_mnp_price.toLocaleString()}
                          name="dc_sales_mnp_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_change_price.toLocaleString()}
                          name="dc_sales_change_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_chooses_price.toLocaleString()}
                          name="dc_chooses_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_choose_mnp_price.toLocaleString()}
                          name="dc_choose_mnp_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_choose_change_price.toLocaleString()}
                          name="dc_choose_change_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td
                        onClick={() => {
                          let Cover = [...typeListData];
                          //기존의 체크여부확인후 체크값제거
                          for (let i = 0; i < Cover.length; i++) {
                            if (Cover[i].telecomid !== 2) {
                              continue;
                            }
                            if (i === idx) {
                              Cover[i].validate = true;
                            } else {
                              Cover[i].validate = false;
                            }
                          }

                          setTypeListData(Cover);
                        }}
                        style={{ textAlign: "center", width: "120px" }}
                      >
                        <MarkingBox style={{ marginRight: -24 }}>
                          {item?.validate === true ? "선택됨" : "선택"}
                        </MarkingBox>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <table
              id="customers"
              style={{
                marginTop: 24,
                width: "100%",
              }}
            >
              <thead>
                <tr>
                  <td colspan="8" style={{ textAlign: "center" }}>
                    LGU+
                  </td>
                </tr>
              </thead>
              <thead>
                <tr>
                  <td style={{ textAlign: "center" }}>요금제</td>
                  <td style={{ textAlign: "center" }}>공시지원</td>
                  <td style={{ textAlign: "center" }}>DC MNP</td>
                  <td style={{ textAlign: "center" }}>DC 기변</td>
                  <td style={{ textAlign: "center" }}>선택약정</td>
                  <td style={{ textAlign: "center" }}>DC MNP</td>
                  <td style={{ textAlign: "center" }}>DC 기변</td>
                  <td style={{ textAlign: "center" }}>선택</td>
                </tr>
              </thead>
              <tbody>
                {typeListData.map((item, idx) => {
                  if (item.telecomid !== 3) {
                    return;
                  }
                  return (
                    <tr>
                      <td style={{ textAlign: "center" }}>{item?.name}</td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_price.toLocaleString()}
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          name="dc_sales_price"
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_mnp_price.toLocaleString()}
                          name="dc_sales_mnp_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_sales_change_price.toLocaleString()}
                          name="dc_sales_change_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_chooses_price.toLocaleString()}
                          name="dc_chooses_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_choose_mnp_price.toLocaleString()}
                          name="dc_choose_mnp_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td>
                        <input
                          type="text"
                          value={item?.dc_choose_change_price.toLocaleString()}
                          name="dc_choose_change_price"
                          onChange={(e) => {
                            _handleChange(e, idx);
                          }}
                          style={{
                            border: "none",
                            width: "100%",
                          }}
                        ></input>
                      </td>
                      <td
                        onClick={() => {
                          let Cover = [...typeListData];
                          //기존의 체크여부확인후 체크값제거
                          for (let i = 0; i < Cover.length; i++) {
                            if (Cover[i].telecomid !== 3) {
                              continue;
                            }
                            if (i === idx) {
                              Cover[i].validate = true;
                            } else {
                              Cover[i].validate = false;
                            }
                          }

                          setTypeListData(Cover);
                        }}
                        style={{ textAlign: "center", width: "120px" }}
                      >
                        <MarkingBox style={{ marginRight: -24 }}>
                          {item?.validate === true ? "선택됨" : "선택"}
                        </MarkingBox>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <table
              style={{ marginTop: 24, border: "1px soild #eee", width: "100%" }}
            >
              <tbody>
                <tr>
                  <td style={{ width: 55, textAlign: "center" }}>메모</td>
                  <td>
                    <ProductNameInput
                      value={data.memoadmin}
                      onChange={(e) =>
                        setData({ ...data, memoadmin: e.target.value })
                      }
                    />
                  </td>
                </tr>
              </tbody>
            </table>
            <ProductNameLabel>상품 QA</ProductNameLabel>
            <Select
              placeholder="분류"
              onChange={(e) =>
                setArrayDatasQa({
                  ...ArrayDatasQa,
                  opttype: e,
                })
              }
              value={ArrayDatasQa.opttype}
              options={opttypeData}
            />

            <table
              id="customers"
              style={{
                marginTop: 24,
                marginBottom: 24,
                width: "100%",
              }}
            >
              <tbody>
                <tr
                  style={{
                    display: "flex",
                    width: 500,
                    flexDirection: "column",
                  }}
                >
                  <td
                    style={{
                      height: 45,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <input
                      type="text"
                      placeholder={`Q&A 제목을 입력해주세요`}
                      style={{
                        border: "none",
                        backgroundColor: "#f3f3f3",
                        width: "100%",
                      }}
                      onChange={(e) => {
                        setArrayDatasQa({
                          ...ArrayDatasQa,
                          title: e.target.value,
                        });
                      }}
                      value={ArrayDatasQa?.title}
                    ></input>
                  </td>
                  <td>
                    <InputTextArea
                      onChange={(e) => {
                        setArrayDatasQa({
                          ...ArrayDatasQa,
                          contents: e.target.value,
                        });
                      }}
                      value={ArrayDatasQa?.contents}
                      placeholder="내용을 입력해주세요"
                    />
                  </td>
                </tr>
              </tbody>
            </table>

            <NameCardAdd
              onClick={() => {
                _handleQaData();
              }}
            >
              추가
            </NameCardAdd>
            <table
              id="customers"
              style={{
                marginTop: 24,
                marginBottom: 24,
                width: "100%",
              }}
            >
              <tbody style={{ display: "flex", flexWrap: "wrap" }}>
                {productQa?.map((item, idx) => {
                  if (item.opttype !== ArrayDatasQa.opttype.value) {
                    return;
                  }
                  //해당부분에서 조건분기를한다

                  return (
                    <tr
                      style={{
                        marginTop: 12,
                        marginLeft: 24,
                        display: "flex",
                        width: 500,
                        flexDirection: "column",
                      }}
                    >
                      <td
                        style={{
                          height: 45,
                          display: "flex",
                          alignItems: "center",
                        }}
                      >
                        <input
                          type="text"
                          placeholder={`Q&A 제목을 입력해주세요`}
                          disabled={true}
                          style={{
                            border: "none",
                            backgroundColor: "#f3f3f3",
                            width: "100%",
                          }}
                          value={item?.title}
                        ></input>
                      </td>
                      <td>
                        <InputTextArea
                          disabled={true}
                          value={item?.contents}
                          placeholder="내용을 입력해주세요"
                        />
                      </td>
                      <td>
                        <ProductMemoryInputAddBtn
                          style={{ backgroundColor: "red", cursor: "pointer" }}
                          onClick={() => _handleDeleteproductQa(idx)}
                        >
                          삭제
                        </ProductMemoryInputAddBtn>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            {/*스크린 완료 작없시작 */}
            <div
              style={{
                marginTop: 36,
                width: "100%",
              }}
            >
              <DetailedInfoLabel>상세 정보</DetailedInfoLabel> {/* quill */}
              <MarkDown style={{ marginTop: 24, width: "100%" }}>
                <Editor
                  value={
                    productBoardData
                      ? productBoardData
                      : "상품 정보를 입력해주세요."
                  }
                  placeholder={"상품 정보를 입력해주세요."}
                  onEditorChange={onEditorChange}
                  setfilesCheck={setfilesCheck}
                  filesdata={filesdata}
                  onFilesChange={onFilesChange}
                />
              </MarkDown>
            </div>
          </div>
        </Content>
      </Wrapper>
    </Fade>
  );
};

export default withRouter(Product);
