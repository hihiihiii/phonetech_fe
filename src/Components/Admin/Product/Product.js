import React, { useEffect } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Fade } from "react-reveal";
import { GetproductList } from "Datas/swr";
import { useState } from "react";
import { CopyProduct, deleteProduct } from "Datas/api";
import { exportDataToXlsx } from "Settings/excel";
import { Pagination } from "react-bootstrap";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopLabel = styled.div``;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
`;

const Delete = styled.div`
  display: flex;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const Imgskeleton = styled.img`
  max-width: 200px;
  height: 100px;
  border: none;
  resize: both;
  margin: 0 auto;
  border-radius: 5px;
  object-fit: contain;
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Product = () => {
  const [page, setPage] = useState([]);
  const [offset, setOffset] = useState(0);
  const [filtervalue, setFilterValue] = useState(1);
  const [select, setSelect] = useState([]);
  const [checked, setChecked] = useState(false);
  const { ProductListData, ProductListMutate } = GetproductList(
    "admin",
    0,
    0,
    offset,
    filtervalue
  );
  console.log(ProductListData);

  const [ProductData, setProductData] = useState([]);

  const _joinStringData = (data) => {
    let result = "";
    for (let idx = 0; idx < data?.length; idx++) {
      if (data?.length === idx + 1) {
        result = result + data[idx]?.name;
      } else {
        result = result + data[idx]?.name + ", ";
      }
    }
    return result;
  };
  useEffect(() => {
    let checkd = ProductData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [ProductData]);

  useEffect(() => {
    if (ProductListData?.rows !== undefined) {
      let Products = [...ProductListData?.rows];
      setProductData(Products);
    }
  }, [ProductListData]);

  const _handleSelect = (id) => {
    //셀렉트 메서드를 정렬시켜줌
    let newArray = [...select];
    let temp = [...ProductData];
    temp?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;

        if (!el?.validate) {
          if (newArray.findIndex((item) => item === el.id) === -1) {
            newArray.push(el.id);
          }
        } else {
          newArray = newArray.filter((item) => item !== el.id);
        }
      }
    });

    setProductData(temp);
    setSelect(newArray);

    //셀렉트처리
  };

  const _handleAllSelct = () => {
    //전체샐렉트동기화
    let data = [...ProductData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setProductData(resdata);
    setChecked(true);
  };
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      await deleteProduct({ formid: select });
      setSelect([]);
      await ProductListMutate();
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );
  console.log(select);
  console.log(ProductData, "프로덕트데이터");
  const _handleCopy = async () => {
    if (select.length === 0) {
      alert("복제할 상품을 선택해주세요");
      return;
    }
    let body = {
      formid: select,
    };
    await CopyProduct(body);

    await ProductListMutate();
    setSelect([]);
  };
  const _handleDelete = async () => {
    if (select.length === 0) {
      alert("삭제할 상품을 선택해주세요");
      return;
    }

    desk();
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>상품관리</TopLabel>
          <TopButton>
            <Add
              onClick={() => {
                _handleCopy();
              }}
              style={{ marginLeft: 15 }}
            >
              상품 복제
            </Add>
            <Add>
              <Link
                to={{
                  pathname: "/admin/product/Detail",
                  state: {
                    type: "Add",
                  },
                }}
              >
                상품 추가
              </Link>
            </Add>
            <Delete onClick={_handleDelete}>삭제</Delete>
            <Add
              style={{ marginLeft: 15 }}
              onClick={() =>
                exportDataToXlsx({
                  data: ProductData.filter((item, idx) => {
                    return item.validate === true;
                  }).map((item1, idx1) => {
                    return {
                      번호: item1.id,
                      기기명: item1.name,
                      용량: _joinStringData(item1.HardPrices),
                      색상: _joinStringData(item1.ColorPrices),
                      통신사: _joinStringData(
                        item1.telecom_product_key_product
                      ),
                      출고가: item1.marketprice,
                    };
                  }),
                  filename: "상품_추출",
                })
              }
            >
              엑셀 다운
            </Add>
          </TopButton>
        </Top>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>이미지</th>
              <th>상품명</th>
              <th>용량</th>
              <th>메모</th>
              <th>노출요금제</th>
              <th>출고가격</th>
              <th>순서</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {ProductData?.map((item, idx) => {
              console.log(item);
              return (
                <tr>
                  <LinedTag>
                    <input
                      type="checkbox"
                      onClick={() => _handleSelect(item?.id)}
                      checked={item?.validate}
                    />
                  </LinedTag>
                  <LinedTag>
                    <Imgskeleton
                      src={
                        item?.images !== undefined &&
                        item?.images !== null &&
                        item?.images[0]
                      }
                    />
                  </LinedTag>
                  <LinedTag>{item?.name}</LinedTag>
                  <LinedTag>
                    {item?.HardPrices?.map((hard, idx1) => {
                      return `${hard.name} `;
                    })}
                  </LinedTag>
                  <LinedTag>
                    {item?.memoadmin?.length > 16
                      ? item?.memoadmin?.substring(0, 16) + "..."
                      : item?.memoadmin}
                  </LinedTag>
                  <LinedTag>
                    {item?.telecom_product_key_product?.map((tel, idx3) => {
                      return `${tel.name} `;
                    })}
                  </LinedTag>
                  <LinedTag>
                    {String(item?.marketprice)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  </LinedTag>

                  <LinedTag>{item?.order}</LinedTag>
                  <HoverTag>
                    <Link
                      to={{
                        pathname: "/admin/product/Detail",
                        state: {
                          type: "Edit",
                          id: item?.id,
                        },
                      }}
                    >
                      더보기
                    </Link>
                  </HoverTag>
                </tr>
              );
            })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={ProductListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default Product;
