import React, { Component, useState } from "react";
import styled from "styled-components";
import { Link, withRouter } from "react-router-dom";
import logo from "../../logo.png";

import { GetMe, useAuth } from "Datas/storage";
import { GetCategoryList, GetInforMation } from "Datas/swr";

const Wrapper = styled.div`
  height: 100px;
  width: 100%;
  position: fixed;
  top: 0;
  background: #fff;
  z-index: 2;
  border-bottom: 1px solid #eee;
  @media screen and (max-width: 768px) {
    height: 80px;
  }
  @media screen and (max-width: 375px) {
    height: 70px;
  }
`;

const Inner = styled.div`
  display: flex;
  max-width: 1570px;
  margin: 0 auto;
  height: 100%;
  align-items: center;
  padding: 0 20px;
  justify-content: space-between;
`;

const Nav = styled.div`
  display: flex;
  margin-left: 20px;

  @media screen and (max-width: 1300px) {
    display: none;
  }

  & > div {
    margin: 0 15px;
    font-size: 22px;
    cursor: pointer;
  }
`;

const LoginAndSignup = styled.div`
  display: flex;

  @media screen and (max-width: 1300px) {
    display: none;
  }

  & > div {
    margin: 0 5px;
    font-size: 22px;
    cursor: pointer;
  }
`;

const OpenMenu = styled.div`
  width: 30px;
  display: none;
  @media screen and (max-width: 1300px) {
    display: block;
  }
  @media screen and (max-width: 600px) {
    display: block;
    width: 23px;
  }
`;

const Logo = styled.img`
  max-width: 278px;
  max-height: 57px;
  @media screen and (max-width: 1300px) {
    max-width: 238px;
    max-height: 43px;
  }
  @media screen and (max-width: 600px) {
    max-width: 175px;
    max-height: 35px;
  }
`;

const ExpandedMenu = styled.div`
  position: fixed;
  align-items: center;
  flex-direction: column;
  padding-top: 80px;
  display: ${(props) => (props.expanded ? "flex" : "none")};
  background: #fff;
  z-index: 3;
  top: 100px;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  & > div {
    margin: 10px 0;
    font-size: 24px;
    font-weight: 500;
  }
  @media screen and (max-width: 768px) {
    top: 80px;
  }

  @media screen and (max-width: 375px) {
    top: 70px;
  }
`;

const MenuButton = styled.div`
  color: ${(props) => {
    let color;
    if (props.isOn) {
      color = "orange";
    }
    return color;
  }};
  :hover {
    opacity: 0.5;
  }
`;

const Header = ({ profile, location }) => {
  const { CategoryListData, isLoading } = GetCategoryList();
  const { InfoData, InfoDataMutate } = GetInforMation();
  // state
  const [type, setType] = useState(false);
  const typeData = ["one", "two", "three"];
  console.log(InfoData);
  const openMenu = () => {
    setType(!type);
    window.scrollTo(0, 0);
  };

  const _handlerTopScroll = () => {
    window.scrollTo(0, 0);
  };

  if (location.pathname.indexOf("admin") === -1) {
    return (
      <Wrapper>
        <Inner>
          <Link to="/">
            <Logo alt="" src={InfoData !== undefined && InfoData[0].image} />
            {/* 로고 수정하실때 style값 수정 X */}
          </Link>
          <Nav>
            {/* <MenuButton isOn={location.pathname.indexOf("y/one") !== -1}>
              <Link to="/category/one">번호이동</Link>
            </MenuButton>
            <MenuButton isOn={location.pathname.indexOf("y/two") !== -1}>
              <Link to="/category/two">기기변경</Link>
            </MenuButton>
            <MenuButton isOn={location.pathname.indexOf("y/three") !== -1}>
              <Link to="/category/three">사은품</Link>
            </MenuButton> */}
            {CategoryListData?.length !== 0 &&
              CategoryListData?.map((el, idx) => {
                return (
                  <MenuButton
                    isOn={location.pathname.indexOf(`y/${el?.name}`) !== -1}
                  >
                    <Link
                      to={`/category/${el?.name}/${el.id}`}
                      onClick={() => {
                        _handlerTopScroll();
                      }}
                      onClick={() => {
                        _handlerTopScroll();
                      }}
                    >
                      {el?.name}
                    </Link>
                  </MenuButton>
                );
              })}
            <MenuButton isOn={location.pathname.indexOf("announcement") !== -1}>
              <Link
                to="/announcement"
                onClick={() => {
                  _handlerTopScroll();
                }}
              >
                공지사항
              </Link>
            </MenuButton>
            <MenuButton isOn={location.pathname.indexOf("preorder") !== -1}>
              <Link
                to="/preorder"
                onClick={() => {
                  _handlerTopScroll();
                }}
              >
                사전예약
              </Link>
            </MenuButton>
          </Nav>
          <LoginAndSignup>
            <MenuButton isOn={location.pathname.indexOf("profile") !== -1}>
              <Link
                to="/profile"
                onClick={() => {
                  _handlerTopScroll();
                }}
              >
                프로필
              </Link>
            </MenuButton>
            <MenuButton
              isOn={location.pathname.indexOf("/contractlist") !== -1}
            >
              <Link
                to="/contractlist"
                onClick={() => {
                  _handlerTopScroll();
                }}
              >
                주문 내역
              </Link>
            </MenuButton>
            {profile?.gm > 0 && (
              <MenuButton isOn={location.pathname.indexOf("admin") !== -1}>
                <Link
                  to="/admin"
                  onClick={() => {
                    _handlerTopScroll();
                  }}
                >
                  관리자
                </Link>
              </MenuButton>
            )}
          </LoginAndSignup>
          <OpenMenu onClick={openMenu}>
            <svg aria-hidden="true" viewBox="0 0 448 512">
              <path
                fill="currentColor"
                d="M436 124H12c-6.627 0-12-5.373-12-12V80c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12z"
              ></path>
            </svg>
          </OpenMenu>
        </Inner>
        <ExpandedMenu expanded={type}>
          {/* <MenuButton isOn={location.pathname.indexOf("y/one") !== -1}>
            <Link onClick={openMenu} to="/category/one">
              번호이동
            </Link>
          </MenuButton>
          <MenuButton isOn={location.pathname.indexOf("y/two") !== -1}>
            <Link onClick={openMenu} to="/category/two">
              기기변경
            </Link>
          </MenuButton>
          <MenuButton isOn={location.pathname.indexOf("y/three") !== -1}>
            <Link onClick={openMenu} to="/category/three">
              사은품
            </Link>
          </MenuButton>
          <MenuButton isOn={location.pathname.indexOf("announcement") !== -1}>
            <Link onClick={openMenu} to="/announcement">
              공지사항
            </Link>
          </MenuButton>
          <MenuButton>사전예약</MenuButton>

          <MenuButton isOn={location.pathname.indexOf("profile") !== -1}>
            <Link onClick={openMenu} to="/profile">
              프로필
            </Link>
          </MenuButton> */}
          {CategoryListData?.length !== 0 &&
            CategoryListData?.map((el, idx) => {
              return (
                <MenuButton
                  isOn={location.pathname.indexOf(`y/${el?.name}`) !== -1}
                  onClick={openMenu}
                >
                  <Link to={`/category/${el?.name}/${el.id}`}>{el?.name}</Link>
                </MenuButton>
              );
            })}
          <MenuButton isOn={location.pathname.indexOf("announcement") !== -1}>
            <Link onClick={openMenu} to="/announcement">
              공지사항
            </Link>
          </MenuButton>
          <MenuButton isOn={location.pathname.indexOf("preorder") !== -1}>
            <Link onClick={openMenu} to="/preorder">
              사전예약
            </Link>
          </MenuButton>

          <MenuButton isOn={location.pathname.indexOf("profile") !== -1}>
            <Link onClick={openMenu} to="/profile">
              프로필
            </Link>
          </MenuButton>
          <MenuButton isOn={location.pathname.indexOf("contractlist") !== -1}>
            <Link onClick={openMenu} to="/contractlist">
              주문 내역
            </Link>
          </MenuButton>
          {/* {profile?.gm > 0 && (
            <MenuButton isOn={location.pathname.indexOf("admin") !== -1}>
              <Link onClick={openMenu} to="/admin">
                관리자
              </Link>
            </MenuButton>
          )} */}
        </ExpandedMenu>
      </Wrapper>
    );
  } else {
    return "";
  }
};

export default withRouter(Header);
