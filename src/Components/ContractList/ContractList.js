import React, { Component } from "react";
import styled from "styled-components";

import PhoneImgPng from "phone.png";
import { Link, withRouter } from "react-router-dom";
import { Fade } from "react-reveal";
import { GetUserOrderList } from "Datas/swr";

const Wrapper = styled.div``;

const Inner = styled.div`
  max-width: 1610px;
  width: 100%;
  margin: 0 auto;
  padding: 0 20px;
  margin-top: 150px;
`;

const ContractText = styled.div`
  font-size: 40px;
  font-weight: 700;
  padding-bottom: 15px;
  border-bottom: 1px solid #d1d1d1;

  @media screen and (max-width: 950px) {
    font-size: 32px;
  }

  @media screen and (max-width: 450px) {
    font-size: 22px;
  }
`;

const Row = styled.div`
  margin: 55px 0;
  display: flex;
`;

const PhoneImg = styled.div`
  & > img {
    width: 140px;
  }
  max-width: 334px;
  @media screen and (max-width: 718px) {
    max-width: 280px;
  }

  @media screen and (max-width: 359px) {
    max-width: 140px;
  }
`;

const OnlyDataRow = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  width: 100%;
  margin: 0 20px;
`;

const RowItem = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% / 5 - 16px - 0.01px);
  margin-left: calc(16px / 5);
  margin-right: calc(16px / 5);

  @media screen and (max-width: 1250px) {
    width: calc(100% / 2 - 16px - 0.01px);
    margin-left: calc(16px / 2);
    margin-right: calc(16px / 2);
  }

  @media screen and (max-width: 500px) {
    width: calc(100% / 1 - 16px - 0.01px);
    margin-left: calc(16px / 1);
    margin-right: calc(16px / 1);
  }

  margin-bottom: 20px;
`;

const RowItemTitle = styled.div`
  color: #989898;
  font-size: 22px;
  @media screen and (max-width: 950px) {
    font-size: 16px;
  }
`;

const RowItemValue = styled.div`
  font-size: 30px;
  font-weight: 700;
  @media screen and (max-width: 950px) {
    font-size: 20px;
  }
  white-space: nowrap;
`;

const RowWriteReviewBtn = styled.div`
  font-size: 20px;
  white-space: nowrap;
  display: flex;
  padding: 8px 13px;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
  background: #ef522a;
  color: #fff;
  max-height: 55px;
  cursor: pointer;
  @media screen and (max-width: 950px) {
    font-size: 16px;
  }
  @media screen and (max-width: 450px) {
    font-size: 12px;
  }
  & > svg {
    width: 22px;
    margin-right: 15px;
  }
`;

const Contract = ({ profile }) => {
  const { GetUserOrderListData, GetUserOrderListMutate } = GetUserOrderList(
    profile?.userId
  );
  console.log(GetUserOrderListData);
  return (
    <Fade Button>
      <Wrapper>
        <Inner>
          <ContractText>주문조회</ContractText>
          {GetUserOrderListData?.map((item, idx) => {
            console.log(item);
            return (
              <Row>
                <PhoneImg>
                  <img
                    alt=""
                    src={
                      item?.OrderProduct?.image !== null &&
                      item?.OrderProduct?.image?.length !== 0 &&
                      item?.OrderProduct?.image[0]
                    }
                  />
                </PhoneImg>
                <OnlyDataRow>
                  <RowItem>
                    <RowItemTitle>제품명</RowItemTitle>
                    <RowItemValue>
                      {item?.OrderProduct?.productname}
                    </RowItemValue>
                  </RowItem>
                  <RowItem>
                    <RowItemTitle>공식신청서</RowItemTitle>
                    <RowItemValue>공식신청서</RowItemValue>
                  </RowItem>
                  <RowItem>
                    <RowItemTitle>진행상태</RowItemTitle>
                    <RowItemValue>
                      {item.isvalidate === 0 ? "개통 진행중" : "개통 완료"}
                    </RowItemValue>
                  </RowItem>
                  <RowItem>
                    <RowItemTitle>월 납입료</RowItemTitle>
                    <RowItemValue>
                      {item.OrderProduct?.monthprice
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      원 / 월
                    </RowItemValue>
                  </RowItem>

                  <RowWriteReviewBtn>
                    <svg viewBox="0 0 512 512">
                      <path
                        fill="currentColor"
                        d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z"
                      ></path>
                    </svg>
                    <Link to={`/writereview/${item.productid}/${item.id}`}>
                      <span>리뷰 작성하기</span>
                    </Link>
                  </RowWriteReviewBtn>
                </OnlyDataRow>
              </Row>
            );
          })}
        </Inner>
      </Wrapper>
    </Fade>
  );
};

export default withRouter(Contract);
