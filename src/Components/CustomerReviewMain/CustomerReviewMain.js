import React, { useState } from "react";
import { StarIcon } from "assets";
import moment from "moment";
import Modal from "react-modal";
import styled from "styled-components";

const breakpoints = [450, 768, 992, 1200]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const ModalWrapper = styled.div``;
const CloseBox = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 999px;
  background-color: #f5f5f5;
  position: absolute;
  top: 2%;
  left: 93%;
  cursor: pointer;
  z-index: 99999;

  ${mq[1]} {
    left: 92%;
  }
  ${mq[0]} {
    left: 88%;
  }
`;

const ModalCardInner = styled.div`
  display: flex;
  overflow-x: scroll;
  height: 100%;
  @media screen and (max-width: 450px) {
    ::-webkit-scrollbar {
      display: none;
    }
  }
`;
const PhoneOptionName = styled.div`
  font-size: 23px;
  color: #838383;
  margin-bottom: 10px;
  font-weight: 600;
  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
`;

const PhoneOptionSelect = styled.div`
  background: #feb43c;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px 0;
  border-radius: 99px;
  color: #fff;
  font-weight: 500;
  width: 50%;
  margin-top: 10px;
  cursor: pointer;
`;

const ReviewBox = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
  background: #fff;
  width: calc(100% / 4 - 45px);
  height: 250px;
  margin-bottom: 40px;
  border-radius: 7px;
  margin-right: 15px;
  @media screen and (max-width: 768px) {
    width: calc(100% / 2 - 45px);
  }

  @media screen and (max-width: 375px) {
    width: 100%;
  }
`;

const CustomerReviewMain = () => {
  const [isReviewModalOpen, setIsReviewModalOpen] = useState(false);
  const [selectedReview, setSelectedReview] = useState();
  return (
    <>
      <Modal
        closeTimeoutMS={300}
        style={{
          overlay: {
            position: "fixed",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            background: "rgba(10, 30, 66, 0.4)",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
            padding: "0 20px",
            zIndex: "999",
          },
          content: {
            position: null,
            maxWidth: "700px",
            width: "100%",
            padding: "0",
            borderRadius: "16px",
            background: "#fff",
            height: "550px",
            inset: null,
            border: null,
            display: "flex",
            flexDirection: "column",
            overflowY: "scroll",
          },
        }}
        isOpen={isReviewModalOpen}
        onRequestClose={() => {
          setIsReviewModalOpen(false);
        }}
      >
        <div
          style={{
            maxWidth: "700px",
            width: "100%",
            padding: "0",
            borderRadius: "16px",
            background: "#fff",
            height: "550px",
            position: "relative",
          }}
        >
          <CloseBox
            onClick={() => {
              setIsReviewModalOpen(false);
            }}
          />
          <ModalWrapper>
            <ModalCardInner
              style={{
                display: "flex",
                flexDirection: "column",
                position: "relative",
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  position: "absolute",
                  top: 15,
                  left: 15,
                }}
              >
                {selectedReview?.star !== 0 &&
                  selectedReview?.star.map((el, idx) => {
                    return (
                      <img
                        src={StarIcon}
                        style={{ width: 25, height: 25, marginRight: 5 }}
                      />
                    );
                  })}
              </div>
              {selectedReview?.images?.length !== 0 && (
                <img
                  src={selectedReview?.images[0]}
                  style={{
                    width: "100%",
                    maxHeight: 550,
                    resize: "both",
                    objectFit: "cover",
                    borderRadius: 10,
                    borderBottomLeftRadius: 0,
                    borderBottomRightRadius: 0,
                  }}
                />
              )}
              <div
                style={{
                  width: "100%",
                  padding: 15,
                  marginTop: selectedReview?.images?.length === 0 && 50,
                }}
              >
                <div>
                  <div style={{ fontSize: 20, fontWeight: "600" }}>
                    {selectedReview?.title}
                  </div>
                  <div
                    style={{
                      marginTop: 7,
                      fontSize: 14,
                      fontWeight: "600",
                      color: "#a7a7a7",
                    }}
                  >
                    {moment(selectedReview?.createdAt).format(
                      "YYYY년 MM월 DD일"
                    )}
                  </div>
                  <div
                    style={{ marginBottom: 25, marginTop: 15 }}
                    dangerouslySetInnerHTML={{
                      __html: selectedReview?.contents,
                    }}
                  ></div>
                </div>
              </div>
            </ModalCardInner>
          </ModalWrapper>
        </div>
      </Modal>
      {/* <PhoneOptionName>고객 후기</PhoneOptionName> */}
      <div
        style={{
          display: "flex",
          alignItems: "center",
          width: "100%",
          flexWrap: "wrap",
        }}
      >
        {/* {ProductInfoData !== undefined &&
                ProductInfoData?.Liviews?.length !== 0 &&
                ProductInfoData?.Liviews?.map((el, idx) => {
                  return (
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        width: "100%",
                      }}
                    >
                      {profile?.gm === 2 && (
                        <PhoneOptionSelect
                          onClick={() => {
                            _handleDeleteReview(el.id);
                          }}
                          style={{ width: "22%", marginBottom: 12 }}
                        >
                          <p>삭제</p>
                        </PhoneOptionSelect>
                      )}
                      <ReviewBox
                        onClick={() => {
                          setIsReviewModalOpen(true);
                          setSelectedReview(el);
                        }}
                        style={{
                          position: "relative",
                          zIndex: 1,
                          cursor: "pointer",
                        }}
                      >
                        {el?.images?.length !== 0 && (
                          <img
                            src={el?.images[0]}
                            style={{
                              width: "100%",
                              height: "100%",
                              resize: "both",
                              objectFit: "cover",
                              borderRadius: 9,
                            }}
                          />
                        )}
                        <div
                          style={{
                            width: "100%",
                            height: "30%",
                            position: "absolute",
                            zIndex: 2,
                            backgroundColor: "transparent",
                            bottom: 0,
                            borderBottomLeftRadius: 7,
                            borderBottomRightRadius: 7,
                            padding: 15,
                            backgroundColor: "rgba(0,0,0,0.5)",
                          }}
                        >
                          <div
                            style={{
                              fontSize: 17,
                              fontWeight: "600",
                              color: "#fff",
                            }}
                          >
                            {el?.title}
                          </div>
                        </div>
                      </ReviewBox>
                    </div>
                  );
                })} */}
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
          }}
        >
          <PhoneOptionSelect style={{ width: "22%", marginBottom: 12 }}>
            <p>삭제</p>
          </PhoneOptionSelect>

          <ReviewBox
            onClick={() => {
              setIsReviewModalOpen(true);
            }}
            style={{
              position: "relative",
              zIndex: 1,
              cursor: "pointer",
            }}
          >
            <img
              style={{
                width: "100%",
                height: "100%",
                resize: "both",
                objectFit: "cover",
                borderRadius: 9,
              }}
            />

            <div
              style={{
                width: "100%",
                height: "30%",
                position: "absolute",
                zIndex: 2,
                backgroundColor: "transparent",
                bottom: 0,
                borderBottomLeftRadius: 7,
                borderBottomRightRadius: 7,
                padding: 15,
                backgroundColor: "rgba(0,0,0,0.5)",
              }}
            >
              <div
                style={{
                  fontSize: 17,
                  fontWeight: "600",
                  color: "#fff",
                }}
              >
                감사합니다.
              </div>
            </div>
          </ReviewBox>
        </div>
      </div>
    </>
  );
};

export default CustomerReviewMain;
