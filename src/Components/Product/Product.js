import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import Modal from "react-modal";
import Faq from "react-faq-component";
import ReactStars from "react-stars";

import { Link } from "react-router-dom";
import logo from "../../logo.png";
import { Fade } from "react-reveal";
import {
  GetByIdTypeTelecom,
  GetCardList,
  GetInforMation,
  GetProductInfo,
  GetPrTelecom,
  GetStoreInfo,
  GetUsageCate,
} from "Datas/swr";
import moment from "moment";
import { StarIcon } from "assets";
import Slider from "react-slick";
import { DeleteReview } from "Datas/api";
import { handlerTopMovement } from "Settings/util";
import { floor, flowRight } from "lodash";

const breakpoints = [450, 768, 992, 1200]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const Wrapper = styled.div``;

const Top = styled.div`
  display: flex;
  justify-content: space-between;

  @media screen and (max-width: 1600px) {
    flex-direction: column;
  }
`;

const Things = styled.div`
  margin-top: 25px;
  margin-bottom: 25px;
`;

const Inner = styled.div`
  margin-top: 180px;
  max-width: 1610px;
  padding: 0 20px;
  margin: 0 auto;
  margin-top: 150px;
  width: 100%;

  @media screen and (max-width: 768px) {
    margin-top: 100px;
  }

  @media screen and (max-width: 450px) {
    margin-top: 75px;
  }
`;

const ProductInformationLabel = styled.div`
  font-size: 35px;
  margin-top: 40px;
  margin-bottom: 25px;
`;

const ProductInformationView = styled.img`
  //height: 2129px;
  width: 100%;
  background: #f0f0f0;
`;

const PhonePreview = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const PhonePreviewImg = styled.div`
  width: 375px;
  height: 550px;
  background-color: #eee;

  & > img {
    width: 100%;
    height: 100%;
  }
`;

const PhonePreviewInfo = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 25px;
  align-items: center;
`;

const PhonePreviewName = styled.div`
  display: flex;
  font-size: 30px;
  color: #535353;
  font-weight: 500;
  @media screen and (max-width: 450px) {
    font-size: 24px;
  }
`;

const PhonePreviewOriginalPrice = styled.div`
  font-size: 25px;
  text-decoration: line-through;
  font-weight: 500;
`;

const PhonePreviewDiscountPrice = styled.div`
  color: #ef522a;
  font-size: 35px;
  font-weight: 500;
`;

const PhoneOptionWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  //overflow-x: scroll;
  margin-bottom: 25px;
`;

const PhoneOption = styled.div`
  display: flex;
  flex-direction: column;
`;

const PhoneOptionCircleMemory = styled.div`
  //display: flex;
  display: -webkit-box;
  overflow-x: scroll;
`;

const PhoneOptionName = styled.div`
  font-size: 23px;
  color: #838383;
  margin-bottom: 10px;
  font-weight: 600;
  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
`;

const PhoneOptionCircle = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${(props) => (props.selected === true ? "#feb43c" : "#f0f0f0")};
  color: ${(props) => (props.selected === true ? "#fff" : "333")};
  padding: 7px 21px;
  font-weight: 500;
  margin-right: 20px;
  font-size: 20px;
  cursor: pointer;
  border-radius: 30px;

  @media screen and (max-width: 768px) {
    padding: 7px 16px;
    font-size: 14px;
    width: 75px;
  }

  @media screen and (max-width: 450px) {
    padding: 8px 16px;
    font-size: 15px;
    margin-right: 15px;
    width: 100px;
    //margin-bottom: 15px;
  }
`;

const SelectBox = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

const PhoneSummary = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  padding: 25px 20px;
  border-radius: 10px;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
  background: #fff;
`;

const PhoneSummaryKeyValue = styled.div`
  display: flex;
  align-items: center;
  margin: 3px 0;
`;

const PhoneSummaryKey = styled.div`
  font-size: 20px;
  color: #a0a0a0;

  @media screen and (max-width: 768px) {
    font-size: 18px;
  }
`;

const PhoneSummaryValue = styled.div`
  font-size: 22px;
  margin-left: auto;
  font-weight: ${(props) => {
    if (props.summary) {
      return "bold";
    }
    return "500";
  }};
  color: ${(props) => {
    if (props.summary) {
      return "red";
    }
    return "";
  }};
  @media screen and (max-width: 768px) {
    font-size: 20px;
  }
`;

const PhoneSummaryWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  max-width: 572px;
  width: 100%;
`;

const Phone5G = styled.div`
  display: flex;
  margin-left: 10px;
  //background: #ffb43c;
  font-weight: 700;
  align-items: center;
  justify-content: center;
  //border-radius: 30px;
  //padding: 3px 16px;
  font-size: 20px;
  color: #ffb43c;

  @media screen and (max-width: 450px) {
    font-size: 24px;
  }
`;

const SummaryLogo = styled.div`
  width: 325px;
  height: 57px;
  /* background: #f0f0f0; */
  background-image: ${(props) => {
    if (props.src) {
      return `url(${props.src})`;
    }
  }};
  background-repeat: no-repeat;
  background-size: contain;
  margin-bottom: 20px;

  @media screen and (max-width: 768px) {
    font-size: 11px;
  }
  @media screen and (max-width: 375px) {
    width: 150px;
    height: 27px;
  }
`;

const PhoneOptionAll = styled.div`
  display: flex;
  flex-direction: column;
`;

const PhoneOptionRateBox = styled.div`
  display: flex;
  flex-direction: column;
  background: #f0f0f0;
  width: 100%;
  padding: 15px 20px;
  cursor: pointer;
  border-radius: 10px;
`;

const PhoneOptionRateBoxNameAndDetail = styled.div`
  display: flex;
  align-items: center;
`;

const PhoneOptionRateBoxName = styled.div`
  font-weight: 700;
  font-size: 30px;

  @media screen and (max-width: 768px) {
    font-size: 24px;
  }
  @media screen and (max-width: 375px) {
    font-size: 19px;
  }
`;

const PhoneOptionRateBoxPrice = styled.div`
  margin-left: auto;
  font-size: 20px;
  color: #525252;

  @media screen and (max-width: 1600px) {
    font-size: 18px;
  }
  @media screen and (max-width: 1100px) {
    font-size: 16px;
  }
`;

const PhoneOptionRateBoxDetail = styled.div`
  margin-top: 10px;
`;

const PhoneOptionDiscountWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  & > div:first-child {
    margin-right: 20px;
  }
`;

const PhoneOptionDiscount = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  justify-content: center;
  padding: 15px 20px;
  border-radius: 10px;
  background: #f0f0f0;
  cursor: pointer;

  & > span:first-child {
    color: #818181;
  }
  & > span:last-child {
    font-weight: 700;
    font-size: 18px;
  }
`;

const PhoneOptionSelect = styled.div`
  background: ${(props) => (props.selected === true ? "#feb43c" : "#f0f0f0")};
  color: ${(props) => (props.selected === true ? "#fff" : "333")};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px 0;
  border-radius: 99px;

  font-weight: 500;
  width: 50%;
  margin-top: 10px;
  cursor: pointer;
`;

const PhoneOptionNotSelect = styled.div`
  background: #feb43c;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 10px 0;
  border-radius: 99px;
  color: #fff;
  font-weight: 500;
  margin-top: 10px;
`;

const ProductInformationGallery = styled.div`
  margin-top: 140px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const ProductInformationGalleryBox = styled.img`
  margin-bottom: 45px;
  max-width: 350px;
  height: 350px;
  width: 100%;
  background: #f0f0f0;
  resize: both;
`;

const BelowBar = styled.div`
  height: 100px;
  background: #ef522a;
  width: 100%;
  position: fixed;
  left: 0;
  right: 0;
  display: flex;
  bottom: 0;
  z-index: 3;

  @media screen and (max-width: 1600px) {
    height: 75px;
  }
  @media screen and (max-width: 1100px) {
    height: 60px;
  }
  @media screen and (max-width: 450px) {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    height: 50px;
  }
`;

const BelowBarInner = styled.div`
  display: flex;
  max-width: 1610px;
  padding: 0 20px;
  align-items: center;
  margin: 0 auto;
  font-weight: 700;
  color: #fff;
  width: 100%;
  font-size: 30px;
  @media screen and (max-width: 1600px) {
    font-size: 22px;
  }
  @media screen and (max-width: 1100px) {
    font-size: 14px;
  }
  @media screen and (max-width: 450px) {
    font-size: 10px;
    display: flex;
    align-items: center;
    flex-direction: row;
    justify-content: space-between;
  }
`;

const BelowBarBtn = styled.div`
  background: #fff;
  color: #ef522a;
  padding: 10px 24px;
  border-radius: 50px;
  margin-left: auto;
  white-space: nowrap;
  cursor: pointer;
  ${mq[0]} {
    margin-left: 0px;
  }
`;

const PhoneSummaryTotal = styled.div`
  display: flex;
  background: #f0f0f0;
  padding: 10px 20px;
  border-radius: 10px;
  justify-content: space-between;
  margin-top: 30px;
`;

const PhoneSummaryTotalEach = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  & > span:nth-child(1) {
    font-size: 17px;
    font-weight: 700;
    @media screen and (max-width: 768px) {
      font-size: 15px;
    }
  }
  & > span:nth-child(2) {
    font-size: 14px;
    color: #606060;
    @media screen and (max-width: 768px) {
      font-size: 12px;
    }
  }
  & > span:nth-child(3) {
    font-size: 25px;
    margin-top: 5px;
    font-weight: 700;
    @media screen and (max-width: 768px) {
      font-size: 15px;
    }
  }
`;

const FaqWrapper = styled.div`
  margin-bottom: 250px;
  margin-top: 50px;
`;

const FaqLabel = styled.div`
  display: flex;
  margin-bottom: 20px;
`;

const FaqLabelSpan = styled.div`
  font-size: 25px;
  font-weight: 600;
  margin-right: 25px;
  cursor: pointer;
  color: #b1b1b1;
  &[aria-selected="true"] {
    color: #000;
    box-shadow: inset 0 -8px 0 #d9fcdb;
  }
  @media screen and (max-width: 768px) {
    font-size: 15px;
  }
  @media screen and (max-width: 450px) {
    font-size: 14px;
  }
`;

const ReviewBox = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
  background: #fff;
  width: calc(100% / 4 - 45px);
  height: 250px;
  margin-bottom: 40px;
  border-radius: 7px;
  margin-right: 15px;
  @media screen and (max-width: 768px) {
    width: calc(100% / 2 - 45px);
  }

  @media screen and (max-width: 375px) {
    width: 100%;
  }
`;

const Selected = styled.div`
  cursor: pointer;
  width: 100%;
  height: 40px;
  background-color: orange;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  border-radius: 100px;
  font-weight: 500;

  :hover {
    background-color: rgba(250, 190, 88, 1);
  }
`;

const MobileChangeBox = styled.div`
  width: 60%;
  margin-top: 5px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`;

const MobileChangeLeftBox = styled.div`
  width: 90px;
  height: 25px;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #333333;
  color: white;
  font-size: 14px;
  margin-right: 3px;
  ${mq[0]} {
    font-size: 12px;
  }
`;
const MobileChangeRightBox = styled.div`
  width: 170px;
  height: 30px;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  opacity: 0.5;
  ${mq[0]} {
    font-size: 10px;
  }
`;

const Bar = styled.div`
  width: 100%;
  height: 3px;
  background-color: black;
  opacity: 0.7;
`;

const FaqRender = ({ qadata }) => {
  const [select, setSelect] = useState(0);
  const [data, setData] = useState({
    rows: [
      {
        title: "tt",
        content: "tt",
      },
    ],
  });

  useEffect(() => {
    let newArray = [];
    if (qadata !== undefined) {
      let newdata = [...qadata];
      switch (select) {
        case 0:
          newArray = newdata
            ?.map((item, index) => {
              if (item.opttype !== 0) {
                return;
              } else {
                return {
                  title: item.title,
                  content: item.contents,
                };
              }
            })
            .filter((item) => item !== undefined);
          break;
        case 1:
          newArray = newdata
            ?.map((item, index) => {
              if (item.opttype !== 1) {
                return;
              } else {
                return {
                  title: item.title,
                  content: item.contents,
                };
              }
            })
            .filter((item) => item !== undefined);
          break;
        case 2:
          newArray = newdata
            ?.map((item, index) => {
              if (item.opttype !== 2) {
                return;
              } else {
                return {
                  title: item.title,
                  content: item.contents,
                };
              }
            })
            .filter((item) => item !== undefined);
          break;
        default:
          break;
      }
      console.log(newArray);
      newdata.rows = newArray;
      setData(newdata);
      console.log(newdata, "데이터");
      console.log();
    }
  }, [qadata, select]);

  return (
    <FaqWrapper>
      <FaqLabel>
        <FaqLabelSpan
          aria-selected={select === 0}
          onClick={() => {
            setSelect(0);
          }}
        >
          신청 및 개통문의
        </FaqLabelSpan>
        <FaqLabelSpan
          aria-selected={select === 1}
          onClick={() => {
            setSelect(1);
          }}
        >
          배송문의
        </FaqLabelSpan>
        <FaqLabelSpan
          aria-selected={select === 2}
          onClick={() => {
            setSelect(2);
          }}
        >
          취소 교환 반품문의
        </FaqLabelSpan>
      </FaqLabel>
      <Faq data={data} />
    </FaqWrapper>
  );
};

const ModalWrapper = styled.div``;

const ModalHeader = styled.div`
  background: #e55436;
  padding: 10px 20px;
  font-weight: 700;
  color: #fff;
  font-size: 32px;
  margin-bottom: 15px;
  position: relative;

  @media screen and (max-width: 450px) {
    font-size: 24px;
  }
`;

const ModalRow = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin: 5px 0;
  padding: 0 20px;
  font-weight: 500;
  cursor: pointer;

  & > span {
    display: flex;
    flex-direction: column;
    & > span {
      font-size: 13px;
      color: #000;
    }
  }
  d & > span:nth-child(1) {
    width: 20%;
  }
  & > span:nth-child(2) {
    color: #e55436;
    width: 20%;
  }
  & > span:nth-child(3) {
    color: #e55436;
    width: 30%;
  }
  & > span:nth-child(4) {
    width: 20%;
  }
  & > span:nth-child(5) {
    width: 15%;
    margin-left: auto;
  }
`;

const ReviewTable = styled.table`
  width: 100%;
  display: block;
  margin-bottom: 30px;
  white-space: nowrap;
  overflow-x: auto;

  @media screen and (max-width: 768px) {
    margin-bottom: 10px;
  }

  thead th {
    padding: 10px 20px;
    font-size: 25px;
    @media screen and (max-width: 768px) {
      font-size: 15px;
    }
    color: #6f6f6f;
    text-align: left;
    background: #eaeaea;
  }
  tbody {
    width: 100%;
  }
  tbody tr td {
    padding: 10px 20px;
    font-size: 20px;
    @media screen and (max-width: 768px) {
      font-size: 14px;
      font-weight: 500;
    }
    text-align: left;
    margin: 10px 0;
  }
  tbody tr {
    margin: 10px 0;
  }
`;

const ReviewContent = styled.div`
  display: flex;
  flex-direction: column;
  text-align: left;

  @media screen and (max-width: 450px) {
    font-size: 10px;
  }

  /* & > span:last-child {
    font-size: 12px;
    margin-top: 5px;
  } */
`;

const ModalCardItem = styled.div`
  min-width: 315px;
  //height: 370px;
  height: 450px;
  background: #fff;
  border: 1.5px solid #f0f0f0;
  border-radius: 8px;
  margin: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  //justify-content: center;

  @media screen and (max-width: 450px) {
    width: 275px;
    height: 450px;
  }
`;

const ModalCardInner = styled.div`
  display: flex;
  overflow-x: scroll;
  height: 100%;
  @media screen and (max-width: 450px) {
    ::-webkit-scrollbar {
      display: none;
    }
  }
`;

const NameText = styled.div`
  font-size: 20px;
  font-weight: 500;
`;

const PriceText = styled.div`
  font-size: 20px;
  font-weight: 600;
`;

const Imgskeleton = styled.img`
  max-width: 90%;
  height: 200px;
  border: none;
  resize: both;
  margin: 0 auto;
  border-radius: 5px;
  object-fit: contain;
`;

const CardDiscount_View = styled.div`
  width: 25%;

  font-size: 16px;
  font-weight: 500;
  box-shadow: inset 0 -8px 0 #d9fcdb;
`;

const CardSelect = styled.div`
  background: orange;
  width: 25%;
  height: 35px;
  border-radius: 5px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const CardSeletText = styled.div`
  width: 45px;
  font-size: 12px;
  font-weight: 500;
  color: #fff;
`;

const AttentionMessage = styled.p`
  font-weight: bold;
  color: red;
  margin-top: 12px;
  font-size: 16px;
  margin-bottom: 12;
  ${mq[0]} {
    font-size: 12px;
  }
`;

const CloseBox = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 999px;
  background-color: #f5f5f5;
  position: absolute;
  top: 2%;
  left: 93%;
  cursor: pointer;
  z-index: 99999;

  ${mq[1]} {
    left: 92%;
  }
  ${mq[0]} {
    left: 88%;
  }
`;

const Product = ({ location, profile }) => {
  console.log("location?.state");
  console.log(location?.state);

  const [datas, setDatas] = useState({
    storage: "",
    color: "",
    oldtelecom: "",
    newtelecom: "",
    plan: "",
    discount: "공시지원할인",
    Sales: 0,
    checkout: "",
    sim: "",
    sktDiscout: "",
    card: "",
    storagePrice: 0,
  });

  const [allPrice, setAllPrice] = useState(0);
  const [isRatePlanModalOpen, setisRatePlanModalOpen] = useState(false);
  const [isCardModalOpen, setisCardModalOpen] = useState(false);
  const [isReviewModalOpen, setIsReviewModalOpen] = useState(false);
  const [isPhoneViewOpen, setIsPhoneViewOpen] = useState(false);
  const [phoneImg, setPhoneImg] = useState(0);
  const [selectedReview, setSelectedReview] = useState();
  const [bene, setBene] = useState(false);
  const [telCount, setTelcount] = useState(0);
  const [teleComPrice, setTelecomPrice] = useState(0);
  const [cardSelected, setCardSelected] = useState(0);
  const [telId, setTelId] = useState(1);
  const { ProductInfoData, isLoading, ProductInfoMutate } = GetProductInfo(
    location?.state?.id
  );
  const [teleDatas, setTeleDatas] = useState([]); //총텔레콤데이터
  const [currentTele, setCurrentTele] = useState(undefined); //현재 텔레데이터
  const { InfoData, InfoDataMutate } = GetInforMation();

  //전체 텔레콤리스트를 필터링하여야함
  const { GetUsageCateData } = GetUsageCate(telId);
  const { CardListData } = GetCardList(datas?.newtelecom, 0);
  const { StoreInfoData } = GetStoreInfo(profile?.storeid);

  console.log("StoreInfoData");
  console.log(StoreInfoData);

  console.log("CardListData");
  console.log(CardListData?.rows);

  //console.log(GetUsageCateData);

  const MonthlyPlan = () => {
    const temp = ProductInfoData?.telecom_product_key_product.filter(
      (item, idx) => item.id === currentTele
    )[0];

    console.log(temp?.pricemonth);
    console.log(temp?.telecom_product.dc_chooses_price);
    console.log(temp?.telecom_product.dc_chooses_price / 24);

    const price =
      temp?.pricemonth - temp?.telecom_product.dc_chooses_price / 24;

    console.log(price);

    /* const price =
      ProductInfoData?.telecom_product_key_product.filter(
        (item, idx) => item.id === currentTele
      )[0]?.pricemonth -
      ProductInfoData?.telecom_product_key_product.filter(
        (item, idx) => item.id === currentTele
      )[0]?.telecom_product.dc_chooses_price /
        24; */
    return price;
  };

  const _handleDeleteReview = async (id) => {
    let res = await DeleteReview(id);
    if (res === false) {
      alert("잘못된 요청입니다");
      return;
    } else {
      alert("리뷰삭제가 완료되었습니다");
      await ProductInfoMutate();

      return;
    }
  };
  useEffect(() => {
    if (!isLoading) {
      setAllPrice(ProductInfoData?.price);

      console.log(ProductInfoData, "프로덕트정보");
      setCurrentTele(location?.state?.chooseBase); //선택된 텔레콤아이디 세팅
      setDatas({
        ...datas,
        oldtelecom: location?.state?.tel,
        newtelecom: location?.state?.newtel,
        color: ProductInfoData?.ColorPrices[0]?.name,
        storage: ProductInfoData?.HardPrices[0]?.name,
        storagePrice: ProductInfoData?.HardPrices[0]?.price,
        price: ProductInfoData?.HardPrices[0]?.price,
        sim:
          location?.state?.tel === location?.state?.newtel
            ? "기존 유심"
            : "새 유심",
        checkout: "일시불",
      });
      //tel id 규정

      setTelId(
        ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.id === location?.state?.chooseBase
        )[0].telecomid
      );
      //이구간에서 선택된 텔레콤만필터하여서 계산해주면됨
      //선택된데이터이므로 해당데이터 기반으로 뿌려줌
    }
  }, [isLoading]);

  const DcPrice = () => {
    let mnps;
    let changes;
    if (datas.oldtelecom !== datas.newtelecom) {
      //번호이동
      //선택약정 기기변경 조건절
      if (datas.discount === "선택약정할인") {
        //선택약정 번호이동비
        mnps = ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.id === currentTele
        )[0]?.telecom_product.dc_choose_mnp_price;
      } else {
        //공시지원번호이동비
        mnps = ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.id === currentTele
        )[0]?.telecom_product.dc_sales_mnp_price;
      }
      return mnps;
    } else {
      //기기변경
      if (datas.discount === "선택약정할인") {
        //선택약정 기기변경비
        changes = ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.id === currentTele
        )[0]?.telecom_product.dc_choose_change_price;
      } else {
        //공시지원 기기변경비
        changes = ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.id === currentTele
        )[0]?.telecom_product.dc_sales_change_price;
      }
      return changes;
    }
  };

  useEffect(() => {
    //일시불일시
    if (datas.checkout === "일시불") {
    }
    if (datas.checkout !== "일시불") {
      let hal = datas.checkout.replace("개월", "");
      let Prices = Math.round(allPrice / hal);

      console.log(Prices);
    }

    if (datas.oldtelecom === datas.newtelecom) {
      setTelcount(0);
    } else {
      let price = ProductInfoData?.telecom_product_key_product?.filter(
        (item) => item.name === datas.newtelecom
      )[0]?.telecom_product?.dc_move_price;
      setTelcount(price);
    }
  }, [datas]);

  console.log(ProductInfoData?.telecom_product_key_product, "뿔기");

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  //기존 수식
  /* const PriceDefault = () => {
    let PhonePrice =
      Number(ProductInfoData?.marketprice) + Number(datas.storagePrice);
    let Sales =
      datas.discount === "선택약정할인"
        ? ProductInfoData?.telecom_product_key_product.filter(
            (item, idx) => item.id === currentTele
          )[0]?.telecom_product.dc_chooses_price
        : ProductInfoData?.telecom_product_key_product.filter(
            (item, idx) => item.id === currentTele
          )[0]?.telecom_product.dc_sales_price;
    PhonePrice = PhonePrice - Number(Sales);
    PhonePrice = PhonePrice - Number(DcPrice());
    PhonePrice =
      PhonePrice -
      Number(cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0);
    return PhonePrice;
  }; */

  const PriceDefault = () => {
    // 1차 가격
    let PhonePrice =
      Number(ProductInfoData?.marketprice) + Number(datas?.storagePrice);

    console.log("--------------------------------------------------------");
    console.log(`1: ${PhonePrice}`);

    if (datas.discount !== "선택약정할인") {
      PhonePrice =
        PhonePrice -
        Number(
          ProductInfoData?.telecom_product_key_product.filter(
            (item, idx) => item.id === currentTele
          )[0]?.telecom_product.dc_sales_price
        );
    }
    console.log(`2: ${PhonePrice}`);

    // 제휴카드 할인
    if (cardSelected !== 0) {
      PhonePrice = PhonePrice + cardSelected?.monthlyDiscount;
    }
    console.log(`3: ${PhonePrice}`);

    // 케이스메이커 D.C
    PhonePrice = PhonePrice - Number(StoreInfoData?.dc);
    console.log(`4: ${PhonePrice}`);

    //PhonePrice = PhonePrice /
    const CheckoutMonth = datas?.checkout.replace("개월", "");

    PhonePrice = Math.round(PhonePrice / CheckoutMonth);
    console.log(`5: ${PhonePrice}`);
    console.log("--------------------------------------------------------");
    return PhonePrice;
  };

  /* const PriceDefault = () => {
    let PhonePrice = Number(datas.storagePrice);
    let Sales =
      datas.discount === "선택약정할인"
        ? ProductInfoData?.telecom_product_key_product.filter(
            (item, idx) => item.id === currentTele
          )[0]?.telecom_product.dc_chooses_price
        : ProductInfoData?.telecom_product_key_product.filter(
            (item, idx) => item.id === currentTele
          )[0]?.telecom_product.dc_sales_price;
    PhonePrice = PhonePrice - Number(Sales);
    PhonePrice = PhonePrice - Number(DcPrice());
    PhonePrice =
      PhonePrice -
      Number(cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0);
    return PhonePrice;
  }; */

  const PricePay = () => {
    // let payOrder = ProductInfoData?.telecom_product_key_product.filter(
    //   (item, idx) => item.id === currentTele
    // )[0]?.pricemonth;

    let payOrder = MonthlyPlan();
    let PhonePrice;

    if (datas.checkout === "일시불") {
      PhonePrice = Number(payOrder);
      /*   PhonePrice = PriceDefault() + Number(payOrder);
      PhonePrice = PhonePrice + Number(PriceDefault()) * 0.059; */
    } else {
      PhonePrice =
        Number(Number(PriceDefault())) / datas.checkout.replace("개월", "") +
        Number(payOrder);
      PhonePrice =
        PhonePrice +
        (Number(Number(PriceDefault())) / datas.checkout.replace("개월", "")) *
          0.059;
    }

    //PhonePrice = PhonePrice + PhonePrice * 0.059;

    return Math.round(PhonePrice);
  };

  const PriceAdd = () => {
    let priceAdd;

    if (datas.checkout !== "일시불") {
      priceAdd =
        (Number(Number(PriceDefault())) / datas.checkout.replace("개월", "")) *
        0.059;
    } else {
      priceAdd = Number(PriceDefault()) * 0.059;
    }
    return Math.round(priceAdd).toLocaleString();
  };

  PriceAdd();

  console.log(ProductInfoData);

  const CheckoutMonth = datas?.checkout.replace("개월", "");

  /* const DiscountPrice =
    Number(
      ProductInfoData?.telecom_product_key_product.filter(
        (item, idx) => item.id === currentTele
      )[0]?.telecom_product.dc_sales_price
    ) +
    StoreInfoData?.dc +
    (cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0); */

  // 출시가격, 공시지원 & 선택약정 기본, 기기변경 & 통신사 이동 (번호 이동), 카드 할인, 폰테크 할인

  // 출시가격
  const BeforeDiscount =
    Number(ProductInfoData?.marketprice) + Number(datas?.storagePrice);

  console.log("Current Telecom");
  console.log(datas?.newtelecom);
  console.log(currentTele);

  // 공시지원 / 선택약정 + 기기변경 / 통신사 이동 가격
  // 공시지원 기본
  const Gongsi_base = ProductInfoData?.telecom_product_key_product.filter(
    (item, idx) => item?.id === currentTele
  )[0]?.telecom_product.dc_sales_price;

  const Gongsi_TypeChange =
    datas?.oldtelecom === datas?.newtelecom
      ? ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item?.id === currentTele
        )[0]?.telecom_product.dc_choose_change_price
      : ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item?.telecomname === datas?.newtelecom
        )[0]?.telecom_product.dc_choose_mnp_price;

  // 선택약정

  // 선택약정 기본 가격
  const Select_base = ProductInfoData?.telecom_product_key_product.filter(
    (item, idx) => item?.id === currentTele
  )[0]?.telecom_product.dc_chooses_price;

  // 선택약정 기본 + 기기변경 / 통신사 이동 가격
  const Select_TypeChagne =
    datas?.oldtelecom === datas?.newtelecom
      ? ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item?.id === currentTele
        )[0]?.telecom_product.dc_sales_change_price
      : ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item?.telecomname === datas?.newtelecom
        )[0]?.telecom_product.dc_sales_mnp_price;

  // 제휴카드 할인금
  const CardDiscount = cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0;

  // 출시가격 - 제휴카드 할인
  const CardDiscountPrice =
    BeforeDiscount -
    (StoreInfoData?.dc +
      (cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0));

  // 공시지원 / 선택약정 + 기기변경 / 통신사 이동 가격 + 제휴카드 할인 + 폰테크 슈퍼 D.C
  const DiscountPrice =
    Number(
      ProductInfoData?.telecom_product_key_product.filter(
        (item, idx) => item.id === currentTele
      )[0]?.telecom_product.dc_sales_price
    ) +
    StoreInfoData?.dc +
    (cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0);

  // 출시가격 - (공시지원 & 선택약정 기본 + 기기변경 & 통신사 이동 + 제휴카드 할인 + 폰테크 슈퍼 D.C)

  const AfterDiscount = BeforeDiscount - DiscountPrice;

  console.log(typeof BeforeDiscount, BeforeDiscount);
  console.log(typeof DiscountPrice, DiscountPrice);
  console.log(typeof CardDiscountPrice, CardDiscountPrice);
  console.log(typeof AfterDiscount, AfterDiscount);

  // ===========================================================================================

  // 출고가
  const PRODUCT_PRICE =
    Number(ProductInfoData?.marketprice) + Number(datas?.storagePrice);

  // 공시지원 / 선택약정
  const DC_PRICE =
    datas?.discount === "공시지원할인"
      ? ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.id === currentTele
        )[0]?.telecom_product.dc_sales_price
      : ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.id === currentTele
        )[0]?.telecom_product.dc_chooses_price;

  const DC_PRICE_TYPE =
    datas?.discount === "공시지원할인"
      ? datas?.oldtelecom === datas?.newtelecom
        ? ProductInfoData?.telecom_product_key_product.filter(
            (item, idx) => item.id === currentTele
          )[0]?.telecom_product.dc_sales_change_price
        : ProductInfoData?.telecom_product_key_product.filter(
            (item, idx) => item.telecomname === datas?.newtelecom
          )[0]?.telecom_product.dc_sales_mnp_price
      : datas?.oldtelecom === datas?.newtelecom
      ? ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.id === currentTele
        )[0]?.telecom_product.dc_choose_change_price
      : ProductInfoData?.telecom_product_key_product.filter(
          (item, idx) => item.telecomname === datas?.newtelecom
        )[0]?.telecom_product.dc_choose_mnp_price;

  // 케이스메이커 슈퍼 D.C
  const SUPER_DC = StoreInfoData?.dc;

  // 제휴카드 할인
  const CARD_DC = cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0;

  // 총 할인금액
  const TOTAL_DISCOUNT =
    Number(DC_PRICE) +
    Number(DC_PRICE_TYPE) +
    Number(SUPER_DC) +
    Number(CARD_DC);

  console.log("TOTAL_DISCOUNT");
  console.log(DC_PRICE);
  console.log(DC_PRICE_TYPE);
  console.log(SUPER_DC);
  console.log(cardSelected?.monthlyDiscount);

  // 할부원금
  const CHECKOUT_PRICE = PRODUCT_PRICE - TOTAL_DISCOUNT;

  console.log(
    "CardListData CardListData CardListData CardListData CardListData CardListData CardListData"
  );
  console.log(CardListData);

  const PricePayData = () => {
    let data;

    if (datas.checkout !== "일시불") {
      if (datas?.discount === "공시지원할인") {
        data = Math.round(
          Math.round(
            Number(
              ProductInfoData?.telecom_product_key_product.filter(
                (item, idx) => item.id === currentTele
              )[0]?.pricemonth
            ) + Number(CHECKOUT_PRICE / CheckoutMonth)
          ) + Math.round(Number(CHECKOUT_PRICE / CheckoutMonth) * 0.059)
        );
      } else {
        data = Math.round(
          Math.round(
            Math.round(
              ProductInfoData?.telecom_product_key_product.filter(
                (item, idx) => item.id === currentTele
              )[0]?.pricemonth -
                (DC_PRICE + DC_PRICE_TYPE) / 24
            ) + Math.round(Number(CardDiscountPrice / CheckoutMonth))
          ) + Math.round(Number(CardDiscountPrice / CheckoutMonth) * 0.059)
        );
      }
    } else {
      if (datas?.discount === "공시지원할인") {
        data =
          ProductInfoData?.telecom_product_key_product
            .filter((item, idx) => item.id === currentTele)[0]
            ?.pricemonth.toLocaleString() + "원";
      } else {
        data = Math.round(
          ProductInfoData?.telecom_product_key_product.filter(
            (item, idx) => item.id === currentTele
          )[0]?.pricemonth -
            (DC_PRICE + DC_PRICE_TYPE) / 24
        ).toLocaleString();
      }
    }
    return data;
  };

  console.log("PricePayData()");
  console.log(PricePayData());

  return (
    <Fade Button>
      <Wrapper>
        <Inner>
          <Modal
            closeTimeoutMS={300}
            style={{
              overlay: {
                position: "fixed",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                background: "rgba(10, 30, 66, 0.4)",
                top: "0px",
                left: "0px",
                width: "100%",
                height: "100%",
                padding: "0 20px",
                zIndex: "999",
              },
              content: {
                position: null,
                maxWidth: "700px",
                width: "100%",
                padding: "0",
                borderRadius: "16px",
                background: "#fff",
                minHeight: "460px",
                inset: null,
                border: null,
                display: "flex",
                flexDirection: "column",
              },
            }}
            isOpen={isCardModalOpen}
            onRequestClose={() => {
              setisCardModalOpen(false);
            }}
          >
            <div
              style={{
                maxWidth: "700px",
                width: "100%",
                padding: "0",
                borderRadius: "16px",
                background: "#fff",
                minHeight: "460px",
                position: "relative",
              }}
            >
              <CloseBox
                onClick={() => {
                  setisCardModalOpen(false);
                }}
              />
              <ModalWrapper>
                <ModalCardInner>
                  {CardListData?.rows?.length !== 0 &&
                    CardListData?.rows?.map((el, idx) => {
                      return (
                        <ModalCardItem>
                          <div
                            style={{
                              marginTop: 15,
                              marginBottom: 15,
                              fontSize: 16,
                              fontWeight: "600",
                            }}
                          >
                            {el?.name}
                          </div>
                          <a
                            style={{ width: "100%", height: "100%" }}
                            href={el?.link}
                          >
                            <Imgskeleton src={el?.images[0]} />
                          </a>
                          <div
                            style={{
                              fontSize: 15,
                              fontWeight: "600",
                              color: "#a7a7a7",
                              marginBottom: 15,
                            }}
                          >
                            고객센터 {el?.tel}
                          </div>
                          {el?.price?.map((item, idx) => {
                            return (
                              <div
                                style={{
                                  width: "86%",
                                  display: "flex",
                                  flexDirection: "column",
                                  alignItems: "center",
                                  marginBottom: 15,
                                }}
                              >
                                <div
                                  style={{
                                    width: "100%",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                  }}
                                >
                                  <div
                                    style={{
                                      fontSize: 15,
                                      fontWeight: "500",
                                      color: "#535353",
                                      width: "70%",
                                      height: 40,
                                    }}
                                  >
                                    전월실적{" "}
                                    {item?.last
                                      ?.toString()
                                      .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        ","
                                      )}{" "}
                                    이상
                                  </div>
                                  <CardSelect
                                    onClick={() => {
                                      setisCardModalOpen(false);
                                      const tempData = {
                                        images: el?.images,
                                        name: el?.name,
                                        discount: item?.discount,
                                        last: item?.last,
                                        monthlyDiscount:
                                          Number(item?.discount) * 24,
                                      };
                                      console.log(typeof cardSelected); // state --> Number
                                      console.log("tempData");
                                      console.log(typeof tempData); // 할당 값 --> Object
                                      setCardSelected(0);
                                      //console.log(tempData);
                                      setCardSelected({
                                        images: el?.images,
                                        name: el?.name,
                                        discount: Number(item?.discount),
                                        last: Number(item?.last),
                                        monthlyDiscount: Number(
                                          Number(item?.discount) * 24
                                        ),
                                      });
                                    }}
                                  >
                                    <CardSeletText>선택하기</CardSeletText>
                                  </CardSelect>
                                </div>

                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    alignSelf: "flex-start",
                                    justifyContent: "space-between",
                                    marginTop: 5,
                                    width: "100%",
                                  }}
                                >
                                  <div
                                    style={{
                                      fontSize: 15,
                                      fontWeight: "500",
                                      width: "75%",
                                      height: 50,
                                      display: "flex",
                                      alignItems: "center",
                                    }}
                                  >
                                    매월{" "}
                                    {item?.discount
                                      ?.toString()
                                      .replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        ","
                                      )}{" "}
                                    x 24개월 = 총
                                  </div>
                                  <CardDiscount_View style={{ marginLeft: 3 }}>
                                    {(Number(item?.discount) * 24)
                                      ?.toString()
                                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                  </CardDiscount_View>
                                </div>
                              </div>
                            );
                          })}
                        </ModalCardItem>
                      );
                    })}
                </ModalCardInner>
              </ModalWrapper>
            </div>
          </Modal>
          <Modal
            closeTimeoutMS={300}
            style={{
              overlay: {
                position: "fixed",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                background: "rgba(10, 30, 66, 0.4)",
                top: "0px",
                left: "0px",
                width: "100%",
                height: "100%",
                padding: "0 20px",
                zIndex: "999",
              },
              content: {
                position: null,
                maxWidth: "700px",
                width: "100%",
                padding: "0",
                borderRadius: "16px",
                background: "#fff",
                maxHeight: "460px",
                overflowY: "scroll",
                inset: null,
                border: null,
                display: "flex",
                flexDirection: "column",
              },
            }}
            isOpen={isRatePlanModalOpen}
            onRequestClose={() => {
              setisRatePlanModalOpen(false);
            }}
          >
            <ModalWrapper>
              {GetUsageCateData !== undefined &&
              GetUsageCateData?.length !== 0 ? (
                GetUsageCateData?.map((el, idx) => {
                  console.log("GetUsageCateData");
                  console.log(el);
                  return (
                    <div style={{ marginBottom: 15 }}>
                      <ModalHeader
                        style={{
                          width: "100%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}
                      >
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <div>{el?.title}</div>
                          <div
                            style={{
                              fontSize: 17,
                              fontWeight: "500",
                              marginLeft: 10,
                              padding: "5px 15px",
                              border: "2px solid #FFA08C",
                              borderRadius: 7,
                            }}
                          >
                            {el?.giga}
                          </div>
                        </div>
                        <div style={{ fontSize: 17 }}>{el?.option}</div>
                      </ModalHeader>
                      {el?.TypeTeleComs?.length !== 0
                        ? el?.TypeTeleComs.map((item, idx) => {
                            return (
                              <div style={{ padding: 10 }}>
                                {/* <ModalRow
                            onClick={() => {
                              setTelecomPrice(item);
                              setisRatePlanModalOpen(false);
                            }}
                          > */}
                                {/* <span>{item.name}</span>
                            <span>
                              {item.pricemonth
                                ?.toString()
                                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </span>
                            <span>{item.datarem}</span>
                            <span>
                              {" "}
                              {item.callrem.indexOf("(") === -1
                                ? item.callrem
                                : +item.callrem.split("(").map((item) => {
                                    return (
                                      <div style={{ textAlign: "center" }}>
                                        {item}
                                      </div>
                                    );
                                  })}
                            </span>
                            <span>
                              {item.smsrem.indexOf("(") === -1
                                ? item.smsrem
                                : item.smsrem.split("(").map((item) => {
                                    return item;
                                  })}
                            </span> */}
                                <div
                                  style={{
                                    display: "flex",
                                    width: "100%",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                    marginBottom: 15,
                                  }}
                                >
                                  <NameText>{item.name}</NameText>
                                  <PriceText style={{ color: "orangered" }}>
                                    {item.pricemonth
                                      ?.toString()
                                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                                    원
                                  </PriceText>
                                </div>
                                <ReviewTable>
                                  <colgroup>
                                    <col style={{ width: "33%" }} />
                                    <col style={{ width: "33%" }} />
                                    <col style={{ width: "33%" }} />
                                  </colgroup>
                                  <thead>
                                    <tr>
                                      <th>데이터</th>
                                      <th>음성</th>
                                      <th>문자</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>{item.datarem}</td>
                                      <td>{item.callrem}</td>
                                      <td>{item.smsrem}</td>
                                    </tr>
                                  </tbody>
                                </ReviewTable>
                                {/* 선택 */}
                                <SelectBox>
                                  <Selected
                                    onClick={() => {
                                      setCurrentTele(item.id);
                                      setisRatePlanModalOpen(false);
                                    }}
                                    style={{ width: "48.5%" }}
                                  >
                                    선택하기
                                  </Selected>
                                  <Selected
                                    onClick={() => {
                                      setisRatePlanModalOpen(false);
                                    }}
                                    style={{ width: "48.5%" }}
                                  >
                                    닫기
                                  </Selected>
                                </SelectBox>

                                {/* </ModalRow> */}
                              </div>
                            );
                          })
                        : "요금제 정보가없습니다"}
                    </div>
                  );
                })
              ) : (
                <div
                  style={{
                    width: "100%",
                    height: 500,
                    display: "flex",
                    justifyContent: "center",
                    marginTop: 200,
                    padding: 0,
                    fontSize: 17,
                    fontWeight: "600",
                  }}
                >
                  요금제가 없습니다
                </div>
              )}
            </ModalWrapper>
          </Modal>

          <Modal
            closeTimeoutMS={300}
            style={{
              overlay: {
                position: "fixed",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                background: "rgba(10, 30, 66, 0.4)",
                top: "0px",
                left: "0px",
                width: "100%",
                height: "100%",
                padding: "0 20px",
                zIndex: "999",
              },
              content: {
                position: null,
                maxWidth: "700px",
                width: "100%",
                padding: "0",
                borderRadius: "16px",
                background: "#fff",
                height: "550px",
                inset: null,
                border: null,
                display: "flex",
                flexDirection: "column",
                overflowY: "scroll",
              },
            }}
            isOpen={isReviewModalOpen}
            onRequestClose={() => {
              setIsReviewModalOpen(false);
            }}
          >
            <div
              style={{
                maxWidth: "700px",
                width: "100%",
                padding: "0",
                borderRadius: "16px",
                background: "#fff",
                height: "550px",
                position: "relative",
              }}
            >
              <CloseBox
                onClick={() => {
                  setIsReviewModalOpen(false);
                }}
              />
              <ModalWrapper>
                <ModalCardInner
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    position: "relative",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      position: "absolute",
                      top: 15,
                      left: 15,
                    }}
                  >
                    {selectedReview?.star !== 0 &&
                      selectedReview?.star.map((el, idx) => {
                        return (
                          <img
                            src={StarIcon}
                            style={{ width: 25, height: 25, marginRight: 5 }}
                          />
                        );
                      })}
                  </div>
                  {selectedReview?.images?.length !== 0 && (
                    <img
                      src={selectedReview?.images[0]}
                      style={{
                        width: "100%",
                        maxHeight: 550,
                        resize: "both",
                        objectFit: "cover",
                        borderRadius: 10,
                        borderBottomLeftRadius: 0,
                        borderBottomRightRadius: 0,
                      }}
                    />
                  )}
                  <div
                    style={{
                      width: "100%",
                      padding: 15,
                      marginTop: selectedReview?.images?.length === 0 && 50,
                    }}
                  >
                    <div style={{}}>
                      <div style={{ fontSize: 20, fontWeight: "600" }}>
                        {selectedReview?.title}
                      </div>
                      <div
                        style={{
                          marginTop: 7,
                          fontSize: 14,
                          fontWeight: "600",
                          color: "#a7a7a7",
                        }}
                      >
                        {moment(selectedReview?.createdAt).format(
                          "YYYY년 MM월 DD일"
                        )}
                      </div>
                      <div
                        style={{ marginBottom: 25, marginTop: 15 }}
                        dangerouslySetInnerHTML={{
                          __html: selectedReview?.contents,
                        }}
                      ></div>
                    </div>
                  </div>
                </ModalCardInner>
              </ModalWrapper>
            </div>
          </Modal>

          <Modal
            closeTimeoutMS={300}
            style={{
              overlay: {
                position: "fixed",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                background: "rgba(10, 30, 66, 0.4)",
                top: "0px",
                left: "0px",
                width: "100%",
                height: "100%",
                padding: "0 20px",
                zIndex: "999",
              },
              content: {
                position: null,
                maxWidth: "700px",
                width: "100%",
                padding: "0",
                borderRadius: "16px",
                background: "#fff",
                minHeight: "550px",
                inset: null,
                border: null,
                display: "flex",
                flexDirection: "column",
                overflowY: "scroll",
              },
            }}
            isOpen={isPhoneViewOpen}
            onRequestClose={() => {
              setIsPhoneViewOpen(false);
            }}
          >
            <div
              style={{ maxWidth: "700px", width: "100%", position: "relative" }}
              onClick={() => {
                setIsPhoneViewOpen(false);
              }}
            >
              <CloseBox />
              <ModalWrapper>
                <ModalCardInner
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    position: "relative",
                  }}
                >
                  <img
                    src={ProductInfoData?.images[phoneImg]}
                    style={{
                      width: "100%",
                      height: 650,
                      resize: "both",
                      objectFit: "contain",
                    }}
                  />
                </ModalCardInner>
              </ModalWrapper>
            </div>
          </Modal>
          <BelowBar>
            <BelowBarInner>
              {/* <span>
                {datas.checkout !== "일시불" &&
                  Math.round(
                    Number(Number(PriceDefault())) /
                      datas.checkout.replace("개월", "")
                  )
                    ?.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "원 + 요금제 "}
                {
                  ProductInfoData?.telecom_product_key_product.filter(
                    (item, idx) => item.id === currentTele
                  )[0]?.name
                }
                (
                {ProductInfoData?.telecom_product_key_product
                  .filter((item, idx) => item.id === currentTele)[0]
                  ?.pricemonth.toLocaleString()}
                )원 = 월 납부액{" "}
                {(datas.checkout !== "일시불"
                  ? 0
                  : ProductInfoData?.telecom_product_key_product.filter(
                      (item, idx) => item.id === currentTele
                    )[0]?.pricemonth
                ).toLocaleString()}
                원
              </span> */}

              <span>
                {datas.checkout !== "일시불" &&
                  Math.round(
                    datas?.discount === "공시지원할인"
                      ? /* Number(Number(PriceDefault())) /
                      datas.checkout.replace("개월", "") */
                        CHECKOUT_PRICE / CheckoutMonth
                      : CardDiscountPrice / CheckoutMonth
                  )
                    ?.toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
                    `원 (${CheckoutMonth}개월) `}
                {
                  ProductInfoData?.telecom_product_key_product.filter(
                    (item, idx) => item.id === currentTele
                  )[0]?.name
                }
                (
                {ProductInfoData?.telecom_product_key_product
                  .filter((item, idx) => item.id === currentTele)[0]
                  ?.pricemonth.toLocaleString()}
                )원 = 월 납부액{" "}
                {/* {(datas.checkout !== "일시불"
                  ? 0
                  : ProductInfoData?.telecom_product_key_product.filter(
                      (item, idx) => item.id === currentTele
                    )[0]?.pricemonth
                ).toLocaleString()}
                원 */}
              </span>

              <span style={{ marginLeft: 5 }}>
                {datas.checkout !== "일시불" ? (
                  <>
                    {datas?.discount === "공시지원할인" ? (
                      <>
                        {Math.round(
                          Math.round(
                            Number(
                              ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.pricemonth
                            ) + Number(CHECKOUT_PRICE / CheckoutMonth)
                          ) +
                            Math.round(
                              Number(CHECKOUT_PRICE / CheckoutMonth) * 0.059
                            )
                        )
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      </>
                    ) : (
                      <>
                        {Math.round(
                          Math.round(
                            /* Math.round(Number(MonthlyPlan())) */ Math.round(
                              ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.pricemonth -
                                (DC_PRICE + DC_PRICE_TYPE) / 24
                            ) +
                              Math.round(
                                Number(CardDiscountPrice / CheckoutMonth)
                              )
                          ) +
                            Math.round(
                              Number(CardDiscountPrice / CheckoutMonth) * 0.059
                            )
                        )
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      </>
                    )}
                  </>
                ) : (
                  <>
                    {datas?.discount === "공시지원할인" ? (
                      <>
                        <span>
                          {ProductInfoData?.telecom_product_key_product
                            .filter((item, idx) => item.id === currentTele)[0]
                            ?.pricemonth.toLocaleString()}
                        </span>
                      </>
                    ) : (
                      <>
                        <span style={{ fontWeight: "bold" }}>
                          {
                            /* Math.round(
                                  Number(MonthlyPlan())
                                ) */ Math.round(
                              ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.pricemonth -
                                (DC_PRICE + DC_PRICE_TYPE) / 24
                            ).toLocaleString()
                          }
                        </span>
                      </>
                    )}
                  </>
                )}
                원
              </span>

              <BelowBarBtn>
                <Link
                  to={{
                    pathname: "/contract",
                    state: {
                      //data: location?.state?.data,
                      mainDatas: datas,
                      cardSelected: cardSelected,
                      productData: ProductInfoData,
                      PayPrice: PricePayData(), // 월 청구액
                      allPrice: CHECKOUT_PRICE, // 할부원금
                      // 선택약정 할인금 / 공시지원 할인금
                      ChooseSales:
                        datas.discount === "선택약정할인"
                          ? ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.id === currentTele
                            )[0]?.telecom_product.dc_chooses_price
                          : ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.id === currentTele
                            )[0]?.telecom_product.dc_sales_price,
                      telCount: telCount, //
                      // 요금제
                      teleComPrice:
                        ProductInfoData?.telecom_product_key_product.filter(
                          (item, idx) => item.id === currentTele
                        )[0],
                      DCprice: DcPrice(), // 슈퍼 D.C
                      StoreInfoData: StoreInfoData, //
                    },
                  }}
                  onClick={() => {
                    handlerTopMovement();
                  }}
                >
                  지금 개통하러 가기
                </Link>
              </BelowBarBtn>
            </BelowBarInner>
          </BelowBar>
          <Top>
            {/* 수정사항  */}
            <PhonePreview style={{ marginBottom: 35 }}>
              <PhonePreviewImg onClick={() => setIsPhoneViewOpen(true)}>
                {ProductInfoData?.images !== null &&
                  ProductInfoData?.images?.map((el, idx) => {
                    console.log(el);
                    return <img src={el}></img>;
                  })}
              </PhonePreviewImg>
              <PhonePreviewInfo>
                <PhonePreviewName>
                  {ProductInfoData?.name}
                  <Phone5G>{ProductInfoData?.giga}</Phone5G>
                </PhonePreviewName>
                <PhonePreviewDiscountPrice>
                  {/* 출고가 */}
                  {datas?.discount === "공시지원할인" ? (
                    <>
                      {/* {(
                        datas?.storagePrice -
                        ProductInfoData?.telecom_product_key_product.filter(
                          (item, idx) => item.id === currentTele
                        )[0]?.telecom_product.dc_sales_price -
                        DcPrice() -
                        (cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0)
                      )
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} */}
                      <div
                        style={{
                          width: "100%",
                          fontSize: 24,
                          marginTop: 24,
                          color: "#a7a7a7",
                          textDecoration: "line-through",
                        }}
                      >
                        {PRODUCT_PRICE.toString().replace(
                          /\B(?=(\d{3})+(?!\d))/g,
                          ","
                        )}
                        원
                      </div>

                      {(PRODUCT_PRICE - (DC_PRICE_TYPE + DC_PRICE))
                        /* Number(
                          ProductInfoData?.telecom_product_key_product.filter(
                            (item, idx) => item.id === currentTele
                          )[0]?.telecom_product.dc_sales_price
                        ) */
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    </>
                  ) : (
                    <>
                      {PRODUCT_PRICE
                        /* Number(ProductInfoData?.marketprice) +
                        Number(datas?.storagePrice) */
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    </>
                  )}
                  {/* {datas.checkout !== "일시불"
                    ? Math.round(
                        Number(Number(PriceDefault())) /
                          datas.checkout.replace("개월", "")
                      )
                        ?.toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    : Number(PriceDefault())
                        ?.toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} */}
                  원
                </PhonePreviewDiscountPrice>
              </PhonePreviewInfo>
            </PhonePreview>

            <PhoneOptionAll>
              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>용량</PhoneOptionName>
                  <PhoneOptionCircleMemory>
                    {ProductInfoData?.HardPrices.map((item, idx) => {
                      return (
                        <PhoneOptionCircle
                          selected={false}
                          onClick={() =>
                            setDatas({
                              ...datas,
                              storage: item.name,
                              storagePrice: item.price,
                            })
                          }
                          style={
                            datas.storage === item.name
                              ? { backgroundColor: "orange", color: "#fff" }
                              : null
                          }
                        >
                          {item.name}
                        </PhoneOptionCircle>
                      );
                    })}
                  </PhoneOptionCircleMemory>
                </PhoneOption>
              </PhoneOptionWrapper>
              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>색상</PhoneOptionName>
                  <PhoneOptionCircleMemory>
                    {ProductInfoData?.ColorPrices?.map((item, idx) => {
                      return (
                        <PhoneOptionCircle
                          selected={false}
                          onClick={() =>
                            setDatas({ ...datas, color: item.name })
                          }
                          style={
                            datas.color === item.name
                              ? { backgroundColor: "orange", color: "#fff" }
                              : null
                          }
                        >
                          {item.name}
                        </PhoneOptionCircle>
                      );
                    })}
                  </PhoneOptionCircleMemory>
                </PhoneOption>
              </PhoneOptionWrapper>
              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>기존 통신사</PhoneOptionName>
                  <PhoneOptionCircleMemory>
                    <PhoneOptionCircle
                      selected={datas.oldtelecom === "SKT" ? true : false}
                      onClick={() => {
                        if (datas.newtelecom !== "SKT") {
                          setDatas({
                            ...datas,
                            sim: "새 유심",
                            oldtelecom: "SKT",
                          });
                        } else {
                          setDatas({ ...datas, oldtelecom: "SKT" });
                        }
                      }}
                    >
                      SKT
                    </PhoneOptionCircle>
                    <PhoneOptionCircle
                      selected={datas.oldtelecom === "KT" ? true : false}
                      onClick={() => {
                        if (datas.newtelecom !== "KT") {
                          setDatas({
                            ...datas,
                            sim: "새 유심",
                            oldtelecom: "KT",
                          });
                        } else {
                          setDatas({ ...datas, oldtelecom: "KT" });
                        }
                      }}
                    >
                      KT
                    </PhoneOptionCircle>
                    <PhoneOptionCircle
                      selected={datas.oldtelecom === "LGU+" ? true : false}
                      onClick={() => {
                        if (datas.newtelecom !== "LGU+") {
                          setDatas({
                            ...datas,
                            sim: "새 유심",
                            oldtelecom: "LGU+",
                          });
                        } else {
                          setDatas({ ...datas, oldtelecom: "LGU+" });
                        }
                      }}
                    >
                      LGU+
                    </PhoneOptionCircle>
                  </PhoneOptionCircleMemory>
                </PhoneOption>
              </PhoneOptionWrapper>
              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>변경 통신사</PhoneOptionName>
                  <PhoneOptionCircleMemory>
                    <PhoneOptionCircle
                      selected={datas.newtelecom === "SKT" ? true : false}
                      onClick={() => {
                        if (datas.oldtelecom !== "SKT") {
                          setDatas({
                            ...datas,
                            sim: "새 유심",
                            newtelecom: "SKT",
                          });
                          setTelId(1);
                          let ChooseIdx =
                            ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.telecomname === "SKT"
                            )[0]?.id;

                          setCurrentTele(ChooseIdx);
                        } else {
                          setDatas({ ...datas, newtelecom: "SKT" });
                          setTelId(1);
                          let ChooseIdx =
                            ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.telecomname === "SKT"
                            )[0]?.id;

                          setCurrentTele(ChooseIdx);
                        }
                      }}
                    >
                      SKT
                    </PhoneOptionCircle>
                    <PhoneOptionCircle
                      selected={datas.newtelecom === "KT" ? true : false}
                      onClick={() => {
                        if (datas.oldtelecom !== "KT") {
                          setDatas({
                            ...datas,
                            sim: "새 유심",
                            newtelecom: "KT",
                          });
                          setTelId(2);
                          let ChooseIdx =
                            ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.telecomname === "KT"
                            )[0]?.id;

                          setCurrentTele(ChooseIdx);
                        } else {
                          setDatas({ ...datas, newtelecom: "KT" });
                          setTelId(2);
                          let ChooseIdx =
                            ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.telecomname === "KT"
                            )[0]?.id;

                          setCurrentTele(ChooseIdx);
                          //KT 첫요금제 걸르기 없으면 null
                        }
                      }}
                    >
                      KT
                    </PhoneOptionCircle>
                    <PhoneOptionCircle
                      selected={datas.newtelecom === "LGU+" ? true : false}
                      onClick={() => {
                        if (datas.oldtelecom !== "LGU+") {
                          setDatas({
                            ...datas,
                            sim: "새 유심",
                            newtelecom: "LGU+",
                          });
                          setTelId(3);
                          let ChooseIdx =
                            ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.telecomname === "LGU+"
                            )[0]?.id;

                          setCurrentTele(ChooseIdx);
                        } else {
                          setDatas({ ...datas, newtelecom: "LGU+" });
                          setTelId(3);
                          let ChooseIdx =
                            ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.telecomname === "LGU+"
                            )[0]?.id;

                          setCurrentTele(ChooseIdx);
                          //LGU 첫번쨰 요금 거르기
                        }
                      }}
                    >
                      LGU+
                    </PhoneOptionCircle>
                  </PhoneOptionCircleMemory>
                </PhoneOption>
                {/* 변경 통신사 */}
                <MobileChangeBox>
                  <MobileChangeLeftBox>
                    {datas.newtelecom === datas.oldtelecom
                      ? `기기변경`
                      : `통신사변경`}{" "}
                  </MobileChangeLeftBox>
                  <MobileChangeRightBox>
                    {datas.newtelecom === datas.oldtelecom
                      ? `통신사 그대로 기기만변경!`
                      : `번호는 그대로 통신사만 변경!`}
                  </MobileChangeRightBox>
                </MobileChangeBox>
              </PhoneOptionWrapper>
              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>요금제</PhoneOptionName>
                  <PhoneOptionRateBox
                    onClick={() => {
                      setisRatePlanModalOpen(true);
                    }}
                  >
                    <PhoneOptionRateBoxNameAndDetail>
                      <PhoneOptionRateBoxName>
                        {
                          ProductInfoData?.telecom_product_key_product.filter(
                            (item, idx) => item.id === currentTele
                          )[0]?.name
                        }
                      </PhoneOptionRateBoxName>
                      <PhoneOptionRateBoxPrice>
                        월
                        {ProductInfoData?.telecom_product_key_product
                          .filter((item, idx) => item.id === currentTele)[0]
                          ?.pricemonth.toLocaleString()}
                        원
                      </PhoneOptionRateBoxPrice>
                    </PhoneOptionRateBoxNameAndDetail>
                    <PhoneOptionRateBoxDetail>
                      데이터:
                      {
                        ProductInfoData?.telecom_product_key_product.filter(
                          (item, idx) => item.id === currentTele
                        )[0]?.datarem
                      }
                      <br />
                      음성:
                      {
                        ProductInfoData?.telecom_product_key_product.filter(
                          (item, idx) => item.id === currentTele
                        )[0]?.callrem
                      }
                      <br />
                      문자:
                      {
                        ProductInfoData?.telecom_product_key_product.filter(
                          (item, idx) => item.id === currentTele
                        )[0]?.smsrem
                      }
                    </PhoneOptionRateBoxDetail>
                  </PhoneOptionRateBox>
                </PhoneOption>
              </PhoneOptionWrapper>
              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>할인 방법</PhoneOptionName>
                  <PhoneOptionDiscountWrapper>
                    <PhoneOptionDiscount
                      onClick={() =>
                        setDatas({
                          ...datas,
                          discount: "공시지원할인",
                          Sales:
                            ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.id === currentTele
                            )[0]?.telecom_product.dc_sales_price,
                        })
                      }
                      style={
                        datas.discount === "공시지원할인"
                          ? { backgroundColor: "orange", color: "#fff" }
                          : null
                      }
                    >
                      <span
                        style={
                          datas.discount === "공시지원할인"
                            ? { color: "#fff" }
                            : null
                        }
                      >
                        공시지원할인
                      </span>
                      <span>
                        총
                        {datas.newtelecom === datas.oldtelecom
                          ? Number(
                              ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_sales_price +
                                ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.telecom_product.dc_sales_change_price
                            ).toLocaleString()
                          : Number(
                              ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_sales_price +
                                +ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.telecom_product.dc_sales_mnp_price
                            ).toLocaleString()}
                        원
                      </span>
                    </PhoneOptionDiscount>
                    <PhoneOptionDiscount
                      onClick={() =>
                        setDatas({
                          ...datas,
                          discount: "선택약정할인",
                          Sales:
                            ProductInfoData?.telecom_product_key_product.filter(
                              (item, idx) => item.id === currentTele
                            )[0]?.telecom_product.dc_chooses_price,
                        })
                      }
                      style={
                        datas.discount === "선택약정할인"
                          ? { backgroundColor: "orange", color: "#fff" }
                          : null
                      }
                    >
                      <span
                        style={
                          datas.discount === "선택약정할인"
                            ? { color: "#fff" }
                            : null
                        }
                      >
                        선택약정할인
                      </span>
                      <span>
                        총
                        {datas.newtelecom === datas.oldtelecom
                          ? Number(
                              ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_chooses_price +
                                ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.telecom_product.dc_choose_change_price
                            ).toLocaleString()
                          : Number(
                              ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_chooses_price +
                                ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.telecom_product.dc_choose_mnp_price
                            ).toLocaleString()}
                        원
                      </span>
                    </PhoneOptionDiscount>
                  </PhoneOptionDiscountWrapper>
                </PhoneOption>
              </PhoneOptionWrapper>
              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>구매방법</PhoneOptionName>
                  <PhoneOptionCircleMemory>
                    <PhoneOptionCircle
                      selected={datas.checkout === "일시불" ? true : false}
                      onClick={() => setDatas({ ...datas, checkout: "일시불" })}
                    >
                      일시불
                    </PhoneOptionCircle>
                    <PhoneOptionCircle
                      selected={datas.checkout === "24개월" ? true : false}
                      onClick={() => setDatas({ ...datas, checkout: "24개월" })}
                    >
                      24개월
                    </PhoneOptionCircle>
                    <PhoneOptionCircle
                      selected={datas.checkout === "30개월" ? true : false}
                      onClick={() => setDatas({ ...datas, checkout: "30개월" })}
                    >
                      30개월
                    </PhoneOptionCircle>
                    <PhoneOptionCircle
                      selected={datas.checkout === "36개월" ? true : false}
                      onClick={() => setDatas({ ...datas, checkout: "36개월" })}
                    >
                      36개월
                    </PhoneOptionCircle>
                  </PhoneOptionCircleMemory>
                </PhoneOption>
              </PhoneOptionWrapper>
              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>유심 선택</PhoneOptionName>
                  <PhoneOptionCircleMemory>
                    <PhoneOptionCircle
                      selected={datas.sim === "새 유심" ? true : false}
                      onClick={() => {
                        setDatas({ ...datas, sim: "새 유심" });
                      }}
                    >
                      새 유심
                    </PhoneOptionCircle>

                    <PhoneOptionCircle
                      selected={datas.sim === "기존 유심" ? true : false}
                      onClick={() => {
                        if (datas.newtelecom !== datas.oldtelecom) {
                          return;
                        }
                        setDatas({ ...datas, sim: "기존 유심" });
                      }}
                    >
                      기존 유심
                    </PhoneOptionCircle>
                  </PhoneOptionCircleMemory>
                </PhoneOption>
                {datas.sim === "새 유심" ? (
                  <AttentionMessage>
                    “새 유심 구매 시 첫 달 7,700원이 청구 됩니다.”
                  </AttentionMessage>
                ) : (
                  <></>
                )}
              </PhoneOptionWrapper>

              <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>복지 할인</PhoneOptionName>
                </PhoneOption>
                <AttentionMessage>
                  “통신사 고객 센터 문의”
                  {`< 고객센터 문의 < 휴대폰으로 114접속`}
                </AttentionMessage>
                {bene && (
                  <table id="customers" style={{ width: "100%" }}>
                    <thead>
                      <tr>
                        <td
                          style={{
                            backgroundColor: "#dbdbdb",
                            textAlign: "center",
                            color: "#838383",
                            fontWeight: "bold",
                          }}
                        >
                          복지할인
                        </td>
                        <td
                          style={{
                            backgroundColor: "#dbdbdb",
                            textAlign: "center",
                            color: "#838383",
                            fontWeight: "bold",
                          }}
                        >
                          설명
                        </td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style={{ height: 40 }}>
                        <td style={{ color: "#838383" }}>미적용</td>
                        <td style={{ textAlign: "right", color: "#838383" }}>
                          복지 할인
                        </td>
                      </tr>
                      <tr style={{ height: 40 }}>
                        <td style={{ color: "#838383" }}>장애등급</td>
                        <td style={{ textAlign: "right", color: "#838383" }}>
                          기본료 35% 할인
                        </td>
                      </tr>
                      <tr style={{ height: 40 }}>
                        <td style={{ color: "#838383" }}>국가 유공자</td>
                        <td style={{ textAlign: "right", color: "#838383" }}>
                          기본료 35% 할인
                        </td>
                      </tr>
                      <tr style={{ height: 40 }}>
                        <td style={{ color: "#838383" }}>차상위계층</td>
                        <td style={{ textAlign: "right", color: "#838383" }}>
                          최대 23,650원 할인
                        </td>
                      </tr>
                      <tr style={{ height: 40 }}>
                        <td style={{ color: "#838383" }}>
                          기초생활수급자(생계/의료)
                        </td>
                        <td style={{ textAlign: "right", color: "#838383" }}>
                          최대 28,600원 할인
                        </td>
                      </tr>
                      <tr style={{ height: 40 }}>
                        <td style={{ color: "#838383" }}>
                          기초생활수급자(주거/교육)
                        </td>
                        <td style={{ textAlign: "right", color: "#838383" }}>
                          최대 23,650원 할인
                        </td>
                      </tr>
                      <tr style={{ height: 40 }}>
                        <td style={{ color: "#838383" }}>
                          기초생활수급자(기초연금 수급자)
                        </td>
                        <td style={{ textAlign: "right", color: "#838383" }}>
                          최대 12,100원 할인
                        </td>
                      </tr>
                    </tbody>
                  </table>
                )}
                <PhoneOptionNotSelect
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    setBene(!bene);
                  }}
                >
                  {bene ? "접어두기" : "복지혜택보기"}
                </PhoneOptionNotSelect>
              </PhoneOptionWrapper>
              {/* <PhoneOptionWrapper>
                <PhoneOption>
                  <PhoneOptionName>SK 온가족 할인 혜택</PhoneOptionName>
                  <PhoneOptionDiscountWrapper>
                    <PhoneOptionDiscount
                      onClick={() =>
                        setDatas({ ...datas, sktDiscout: "249,000" })
                      }
                      style={
                        datas.sktDiscout === "249,000"
                          ? { backgroundColor: "orange", color: "#fff" }
                          : null
                      }
                    >
                      <span>온가족 할인</span>
                      <span>총 249,000원</span>
                    </PhoneOptionDiscount>
                    <PhoneOptionDiscount
                      onClick={() =>
                        setDatas({ ...datas, sktDiscout: "379,000" })
                      }
                      style={
                        datas.sktDiscout === "379,000"
                          ? { backgroundColor: "orange", color: "#fff" }
                          : null
                      }
                    >
                      <span>온가족 할인</span>
                      <span>총 379,000원</span>
                    </PhoneOptionDiscount>
                  </PhoneOptionDiscountWrapper>
                  <PhoneOptionNotSelect
                    onClick={() => setDatas({ ...datas, sktDiscout: "" })}
                  >
                    선택안함
                  </PhoneOptionNotSelect>
                </PhoneOption>
              </PhoneOptionWrapper> */}
              <PhoneOptionWrapper>
                <PhoneOptionName>제휴카드</PhoneOptionName>
                {cardSelected === 0 ? (
                  <>
                    <div
                      style={{
                        display: "flex",
                        width: "100%",
                        justifyContent: "space-between",
                      }}
                    >
                      <PhoneOptionSelect
                        onClick={() => {
                          setisCardModalOpen(true);
                        }}
                        selected={isCardModalOpen === true ? true : false}
                        style={{ width: "48.5%" }}
                      >
                        선택
                      </PhoneOptionSelect>
                      <PhoneOptionSelect
                        onClick={() => {
                          setisCardModalOpen(false);
                        }}
                        selected={isCardModalOpen === true ? false : true}
                        style={{ width: "48.5%" }}
                      >
                        선택안함
                      </PhoneOptionSelect>
                    </div>
                  </>
                ) : (
                  <div
                    style={{
                      border: "2px solid #f0f0f0",
                      borderRadius: 10,
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                  >
                    <div
                      style={{
                        width: "86%",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        marginBottom: 15,
                      }}
                    >
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}
                      >
                        <div
                          style={{
                            fontSize: 17,
                            fontWeight: "600",
                            marginTop: 10,
                            marginBottom: 10,
                          }}
                        >
                          {cardSelected?.name}
                        </div>
                        <div
                          style={{
                            padding: "5px 14px",
                            borderRadius: 100,
                            fontSize: 14,
                            fontWeight: "600",
                            color: "#fff",
                            backgroundColor: "orange",
                            cursor: "pointer",
                          }}
                          onClick={() => setisCardModalOpen(true)}
                        >
                          수정
                        </div>
                      </div>
                    </div>
                    <img
                      src={cardSelected?.images[0]}
                      style={{
                        width: "100%",
                        height: 250,
                        backgroundColor: "#fff",
                        resize: "both",
                        objectFit: "contain",
                        borderRadius: 7,
                      }}
                    />
                    <div
                      style={{
                        width: "86%",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        marginBottom: 15,
                      }}
                    >
                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "space-between",
                        }}
                      >
                        <div
                          style={{
                            fontSize: 15,
                            fontWeight: "500",
                            color: "#535353",
                          }}
                        >
                          전월실적
                          {cardSelected?.last
                            ?.toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                          이상
                        </div>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          alignSelf: "flex-start",
                          marginTop: 5,
                        }}
                      >
                        <div
                          style={{
                            fontSize: 16,
                            fontWeight: "500",

                            width: "90%",
                          }}
                        >
                          매월{" "}
                          {cardSelected?.discount
                            ?.toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                          x 24개월 = 총
                        </div>
                        <CardDiscount_View
                          style={{ marginLeft: 3, width: "10%" }}
                        >
                          {cardSelected?.monthlyDiscount
                            ?.toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        </CardDiscount_View>
                      </div>
                    </div>
                  </div>
                )}
                {cardSelected !== 0 && (
                  <PhoneOptionSelect
                    selected={true}
                    onClick={() => setCardSelected(0)}
                  >
                    선택안함
                  </PhoneOptionSelect>
                )}
              </PhoneOptionWrapper>
            </PhoneOptionAll>
            <PhoneSummaryWrapper>
              <PhoneSummary>
                <SummaryLogo
                  src={InfoData !== undefined && InfoData[0].image}
                />
                <PhoneSummaryKeyValue>
                  <PhoneSummaryKey>출고가</PhoneSummaryKey>
                  <PhoneSummaryValue>
                    {/* {datas?.discount === "공시지원할인" ? (
                      <>
                        {ProductInfoData?.marketprice
                          ?.toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      </>
                    ) : (
                      <>
                        {ProductInfoData?.marketprice
                          ?.toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      </>
                    )} */}
                    {PRODUCT_PRICE?.toString().replace(
                      /\B(?=(\d{3})+(?!\d))/g,
                      ","
                    )}
                    원
                  </PhoneSummaryValue>
                </PhoneSummaryKeyValue>
                <PhoneSummaryKeyValue>
                  <PhoneSummaryKey>
                    {datas.discount === "공시지원할인"
                      ? "공시지원할인"
                      : "선택약정할인"}

                    {datas.discount === "선택약정할인" ? (
                      //선택약정 할인일때
                      datas.checkout !== "일시불" ? (
                        // 여기서 일시불이 아니면
                        Math.round(
                          /* ProductInfoData?.telecom_product_key_product.filter(
                            (item, idx) => item.id === currentTele
                          )[0]?.telecom_product.dc_chooses_price / 24 */
                          (DC_PRICE + DC_PRICE_TYPE) / 24
                        ).toLocaleString() + "X 24개월"
                      ) : (
                        //일시불
                        Math.round(
                          /* ProductInfoData?.telecom_product_key_product.filter(
                            (item, idx) => item.id === currentTele
                          )[0]?.telecom_product.dc_chooses_price / 24 */
                          (DC_PRICE + DC_PRICE_TYPE) / 24
                        ).toLocaleString() + " X 24개월"
                      )
                    ) : datas.checkout !== "일시불" ? (
                      //일시불 아니면
                      <></>
                    ) : (
                      //일시불
                      <></>
                    )}
                  </PhoneSummaryKey>
                  <PhoneSummaryValue summary>
                    -
                    {/* {datas.discount === "선택약정할인"
                      ? (
                          ProductInfoData?.telecom_product_key_product.filter(
                            (item, idx) => item.id === currentTele
                          )[0]?.telecom_product.dc_chooses_price +
                          (datas?.oldtelecom === datas?.newtelecom
                            ? ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_choose_change_price
                            : ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_choose_mnp_price)
                        ).toLocaleString()
                      : (
                          ProductInfoData?.telecom_product_key_product.filter(
                            (item, idx) => item.id === currentTele
                          )[0]?.telecom_product.dc_sales_price +
                          (datas?.oldtelecom === datas?.newtelecom
                            ? ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_sales_change_price
                            : ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_sales_mnp_price)
                        ).toLocaleString()} */}
                    {(Number(DC_PRICE) + Number(DC_PRICE_TYPE))
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    원
                  </PhoneSummaryValue>
                </PhoneSummaryKeyValue>
                <PhoneSummaryKeyValue>
                  <PhoneSummaryKey>
                    {InfoData !== undefined && InfoData[0].pagename} 슈퍼 D.C
                  </PhoneSummaryKey>
                  <PhoneSummaryValue summary>
                    {/* -{DcPrice()?.toLocaleString()}원 */}
                    {StoreInfoData?.dc?.toLocaleString()}원
                  </PhoneSummaryValue>
                </PhoneSummaryKeyValue>
                <PhoneSummaryKeyValue>
                  <PhoneSummaryKey>제휴카드</PhoneSummaryKey>

                  <PhoneSummaryValue>
                    {cardSelected !== 0 ? cardSelected?.name : "선택안함"}
                  </PhoneSummaryValue>
                </PhoneSummaryKeyValue>
                <PhoneSummaryKeyValue>
                  <PhoneSummaryKey>제휴카드 할인</PhoneSummaryKey>
                  <PhoneSummaryValue>
                    {cardSelected !== 0
                      ? `${cardSelected?.monthlyDiscount
                          ?.toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}원`
                      : "선택안함"}
                  </PhoneSummaryValue>
                </PhoneSummaryKeyValue>

                <Bar></Bar>
                <PhoneSummaryKeyValue>
                  <PhoneSummaryKey>총 할인금액</PhoneSummaryKey>
                  <PhoneSummaryValue summary>
                    -
                    {/* {(
                      (datas.discount === "선택약정할인"
                        ? 0
                        : ProductInfoData?.telecom_product_key_product.filter(
                            (item, idx) => item.id === currentTele
                          )[0]?.telecom_product.dc_sales_price +
                          (datas?.newtelecom === datas?.oldtelecom
                            ? ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_sales_change_price
                            : ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.telecom_product.dc_sales_mnp_price)) +
                      StoreInfoData?.dc +
                      (cardSelected !== 0 ? cardSelected?.monthlyDiscount : 0)
                    ).toLocaleString()} */}
                    {TOTAL_DISCOUNT?.toString().replace(
                      /\B(?=(\d{3})+(?!\d))/g,
                      ","
                    )}
                    원
                  </PhoneSummaryValue>
                </PhoneSummaryKeyValue>
                <PhoneSummaryKeyValue>
                  <PhoneSummaryKey>할부원금</PhoneSummaryKey>
                  <PhoneSummaryValue summary style={{ color: "#000" }}>
                    <>
                      {/* {datas.discount === "선택약정할인" &&
                        (
                          ProductInfoData?.marketprice -
                          ProductInfoData?.telecom_product_key_product.filter(
                            (item, idx) => item.id === currentTele
                          )[0]?.telecom_product.dc_sales_price -
                          DcPrice() -
                          (cardSelected !== 0
                            ? cardSelected?.monthlyDiscount
                            : 0)
                        )
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} */}
                      {/* {datas?.discount === "공시지원할인" ? (
                        <>
                          {Number(
                            Number(ProductInfoData?.marketprice) -
                              Number(
                                ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.telecom_product.dc_sales_price
                              ) +
                              StoreInfoData?.dc +
                              (cardSelected !== 0
                                ? cardSelected?.monthlyDiscount
                                : 0)
                          )
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        </>
                      ) : (
                        <>
                          {ProductInfoData?.marketprice
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        </>
                      )} */}

                      {datas?.discount === "공시지원할인" ? (
                        <>
                          {(
                            AfterDiscount -
                            (datas?.newtelecom === datas?.oldtelecom
                              ? ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.telecom_product.dc_sales_change_price
                              : ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) =>
                                    item.telecomname === datas?.newtelecom
                                )[0]?.telecom_product.dc_sales_mnp_price)
                          )
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        </>
                      ) : (
                        <>
                          {CardDiscountPrice
                            /* (datas?.newtelecom === datas?.oldtelecom
                              ? ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.telecom_product.dc_choose_change_price
                              : ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) =>
                                    item.telecomname === datas?.newtelecom
                                )[0]?.telecom_product.dc_choose_mnp_price)
                          ) */ .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        </>
                      )}
                      {/* {CHECKOUT_PRICE.toString().replace(
                        /\B(?=(\d{3})+(?!\d))/g,
                        ","
                      )} */}
                    </>
                    <span style={{ color: "#000", fontWeight: 500 }}> 원</span>
                  </PhoneSummaryValue>
                </PhoneSummaryKeyValue>
                <PhoneSummaryTotal>
                  {datas.checkout !== "일시불" ? (
                    <PhoneSummaryTotalEach>
                      <span>월 할부금</span>
                      <span>{datas.checkout}</span>
                      <span>
                        {/* {datas.checkout !== "일시불"
                          ? Math.round(
                              Number(Number(PriceDefault())) /
                                datas.checkout.replace("개월", "")
                            )
                              ?.toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          : Number(PriceDefault())
                              ?.toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} */}
                        {/* {datas.checkout !== "일시불"
                          ? Number(Number(PriceDefault()))
                              ?.toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                          : Number(PriceDefault())
                              ?.toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} */}
                        {datas.checkout !== "일시불" ? (
                          <>
                            {datas?.discount === "공시지원할인" ? (
                              <>
                                {Math.round(
                                  /* Number(
                                    (AfterDiscount -
                                      (datas?.newtelecom === datas?.oldtelecom
                                        ? ProductInfoData?.telecom_product_key_product.filter(
                                            (item, idx) =>
                                              item.id === currentTele
                                          )[0]?.telecom_product
                                            .dc_sales_change_price
                                        : ProductInfoData?.telecom_product_key_product.filter(
                                            (item, idx) =>
                                              item.telecomname ===
                                              datas?.newtelecom
                                          )[0]?.telecom_product
                                            .dc_sales_mnp_price)) /
                                      CheckoutMonth
                                  ) */
                                  CHECKOUT_PRICE / CheckoutMonth
                                )
                                  .toString()
                                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </>
                            ) : (
                              <>
                                {Math.round(
                                  Number(CardDiscountPrice / CheckoutMonth)
                                )
                                  .toString()
                                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </>
                            )}
                          </>
                        ) : (
                          <></>
                        )}
                        원
                      </span>
                    </PhoneSummaryTotalEach>
                  ) : (
                    <></>
                  )}

                  <PhoneSummaryTotalEach>
                    <span>요금제</span>
                    <span>
                      {
                        ProductInfoData?.telecom_product_key_product.filter(
                          (item, idx) => item.id === currentTele
                        )[0]?.name
                      }
                    </span>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                      }}
                    >
                      {datas?.discount === "공시지원할인" ? (
                        <>
                          <span
                            style={{
                              fontWeight: "bold",
                              fontSize: 24,
                              marginTop: 5,
                            }}
                          >
                            {ProductInfoData?.telecom_product_key_product
                              .filter((item, idx) => item.id === currentTele)[0]
                              ?.pricemonth.toLocaleString() + "원"}
                          </span>
                        </>
                      ) : (
                        <>
                          <span
                            style={{
                              textDecoration: "line-through",
                              fontWeight: "bold",
                            }}
                          >
                            {Math.round(
                              Number(
                                /* ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.telecom_product.dc_chooses_price / 24 */
                                ProductInfoData?.telecom_product_key_product.filter(
                                  (item, idx) => item.id === currentTele
                                )[0]?.pricemonth
                              )
                            ).toLocaleString() + "원"}
                          </span>
                          <span style={{ fontWeight: "bold" }}>
                            {/* {Math.round(Number(MonthlyPlan())).toLocaleString()} */}
                            {Math.round(
                              ProductInfoData?.telecom_product_key_product.filter(
                                (item, idx) => item.id === currentTele
                              )[0]?.pricemonth -
                                (DC_PRICE + DC_PRICE_TYPE) / 24
                            ).toLocaleString()}
                            원
                          </span>
                        </>
                      )}
                    </div>
                  </PhoneSummaryTotalEach>
                  <PhoneSummaryTotalEach>
                    <span>월 납부액</span>
                    <span>VAT, 할부이자 포함</span>
                    <span style={{ color: "#EF522A" }}>
                      {/* {PricePay().toLocaleString()}원 */}
                      {/* {Number(PriceDefault()) + } */}

                      {/* 
                        {datas?.discount === "공시지원할인" ? (
                          <>
                            {Math.round(
                              Number(AfterDiscount / CheckoutMonth) * 0.059
                            )
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            }
                          </>
                        ) : (
                          <>
                            {Math.round(
                              Number(CardDiscountPrice / CheckoutMonth) * 0.059
                            )
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                          </>
                        )}
                      */}

                      {datas.checkout !== "일시불" ? (
                        <>
                          {datas?.discount === "공시지원할인" ? (
                            <>
                              {Math.round(
                                Math.round(
                                  Number(
                                    ProductInfoData?.telecom_product_key_product.filter(
                                      (item, idx) => item.id === currentTele
                                    )[0]?.pricemonth
                                  ) + Number(CHECKOUT_PRICE / CheckoutMonth)
                                ) +
                                  Math.round(
                                    Number(CHECKOUT_PRICE / CheckoutMonth) *
                                      0.059
                                  )
                              )
                                .toString()
                                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </>
                          ) : (
                            <>
                              {Math.round(
                                Math.round(
                                  /* Math.round(Number(MonthlyPlan())) */ Math.round(
                                    ProductInfoData?.telecom_product_key_product.filter(
                                      (item, idx) => item.id === currentTele
                                    )[0]?.pricemonth -
                                      (DC_PRICE + DC_PRICE_TYPE) / 24
                                  ) +
                                    Math.round(
                                      Number(CardDiscountPrice / CheckoutMonth)
                                    )
                                ) +
                                  Math.round(
                                    Number(CardDiscountPrice / CheckoutMonth) *
                                      0.059
                                  )
                              )
                                .toString()
                                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                            </>
                          )}
                        </>
                      ) : (
                        <>
                          {datas?.discount === "공시지원할인" ? (
                            <>
                              <span
                                style={{
                                  fontWeight: "bold",
                                  fontSize: 24,
                                  marginTop: 5,
                                }}
                              >
                                {ProductInfoData?.telecom_product_key_product
                                  .filter(
                                    (item, idx) => item.id === currentTele
                                  )[0]
                                  ?.pricemonth.toLocaleString() + "원"}
                              </span>
                            </>
                          ) : (
                            <>
                              <span style={{ fontWeight: "bold" }}>
                                {
                                  /* Math.round(
                                  Number(MonthlyPlan())
                                ) */ Math.round(
                                    ProductInfoData?.telecom_product_key_product.filter(
                                      (item, idx) => item.id === currentTele
                                    )[0]?.pricemonth -
                                      (DC_PRICE + DC_PRICE_TYPE) / 24
                                  ).toLocaleString()
                                }
                                원
                              </span>
                            </>
                          )}
                        </>
                      )}
                    </span>
                  </PhoneSummaryTotalEach>
                </PhoneSummaryTotal>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    marginTop: 16,
                    justifyContent: "flex-end",
                  }}
                >
                  <p
                    style={{
                      color: "gray",
                      fontWeight: "bold",
                      fontSize: 16,
                    }}
                  >
                    {/* 
                      {datas.checkout !== "일시불" ? (
                          <>
                            {datas?.discount === "공시지원할인" ? (
                              <>
                                {Math.round(
                                  Number(AfterDiscount / CheckoutMonth)
                                )
                                  .toString()
                                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </>
                            ) : (
                              <>
                                {Math.round(
                                  Number(CardDiscountPrice / CheckoutMonth)
                                )
                                  .toString()
                                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </>
                            )}
                          </>
                        ) : (
                          <></>
                        )}
                    */}

                    {datas?.checkout !== "일시불" ? (
                      <>
                        할부 이자 할부원금 기준 5.9%{" "}
                        {datas?.discount === "공시지원할인" ? (
                          <>
                            {Math.round(
                              Number(CHECKOUT_PRICE / CheckoutMonth) * 0.059
                            )
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                          </>
                        ) : (
                          <>
                            {Math.round(
                              Number(CardDiscountPrice / CheckoutMonth) * 0.059
                            )
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                          </>
                        )}
                      </>
                    ) : (
                      <>할부 이자 할부원금 (일시불)</>
                    )}
                  </p>
                </div>
              </PhoneSummary>
            </PhoneSummaryWrapper>
          </Top>
          <Things>
            <PhoneOptionName>제품정보</PhoneOptionName>
            <div
              style={{ marginBottom: 25 }}
              dangerouslySetInnerHTML={{
                __html: ProductInfoData?.ProductBoard?.contents,
              }}
            ></div>

            <PhoneOptionName>고객 후기</PhoneOptionName>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                width: "100%",
                flexWrap: "wrap",
              }}
            >
              {ProductInfoData !== undefined &&
                ProductInfoData?.Liviews?.length !== 0 &&
                ProductInfoData?.Liviews?.map((el, idx) => {
                  return (
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        width: "100%",
                      }}
                    >
                      {profile?.gm === 2 && (
                        <PhoneOptionSelect
                          onClick={() => {
                            _handleDeleteReview(el.id);
                          }}
                          style={{ width: "22%", marginBottom: 12 }}
                        >
                          <p>삭제</p>
                        </PhoneOptionSelect>
                      )}
                      <ReviewBox
                        onClick={() => {
                          setIsReviewModalOpen(true);
                          setSelectedReview(el);
                        }}
                        style={{
                          position: "relative",
                          zIndex: 1,
                          cursor: "pointer",
                        }}
                      >
                        {el?.images?.length !== 0 && (
                          <img
                            src={el?.images[0]}
                            style={{
                              width: "100%",
                              height: "100%",
                              resize: "both",
                              objectFit: "cover",
                              borderRadius: 9,
                            }}
                          />
                        )}
                        <div
                          style={{
                            width: "100%",
                            height: "30%",
                            position: "absolute",
                            zIndex: 2,
                            backgroundColor: "transparent",
                            bottom: 0,
                            borderBottomLeftRadius: 7,
                            borderBottomRightRadius: 7,
                            padding: 15,
                            backgroundColor: "rgba(0,0,0,0.5)",
                          }}
                        >
                          <div
                            style={{
                              fontSize: 17,
                              fontWeight: "600",
                              color: "#fff",
                            }}
                          >
                            {el?.title}
                          </div>
                        </div>
                      </ReviewBox>
                    </div>
                  );
                })}
              {/* <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  width: "100%",
                }}
              >
                <PhoneOptionSelect style={{ width: "22%", marginBottom: 12 }}>
                  <p>삭제</p>
                </PhoneOptionSelect>

                <ReviewBox
                  onClick={() => {
                    setIsReviewModalOpen(true);
                  }}
                  style={{
                    position: "relative",
                    zIndex: 1,
                    cursor: "pointer",
                  }}
                >
                  <img
                    style={{
                      width: "100%",
                      height: "100%",
                      resize: "both",
                      objectFit: "cover",
                      borderRadius: 9,
                    }}
                  />

                  <div
                    style={{
                      width: "100%",
                      height: "30%",
                      position: "absolute",
                      zIndex: 2,
                      backgroundColor: "transparent",
                      bottom: 0,
                      borderBottomLeftRadius: 7,
                      borderBottomRightRadius: 7,
                      padding: 15,
                      backgroundColor: "rgba(0,0,0,0.5)",
                    }}
                  >
                    <div
                      style={{
                        fontSize: 17,
                        fontWeight: "600",
                        color: "#fff",
                      }}
                    >
                      감사합니다.
                    </div>
                  </div>
                </ReviewBox>
              </div> */}
            </div>
            {/* <ReviewTable>
              <colgroup>
                <col style={{ width: "10%" }} />
                <col style={{ width: "10%" }} />
                <col style={{ width: "10%" }} />
                <col style={{ width: "16%" }} />
                <col style={{ width: "16%" }} />
                <col style={{ width: "16%" }} />
              </colgroup>
              <thead>
                <tr>
                  <th>번호</th>
                  <th>상품명</th>
                  <th>평점</th>
                  <th>후기내용</th>
                  <th>작성자</th>
                  <th>작성일</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>357</td>
                  <td>아이폰 12</td>
                  <td>5점</td>
                  <td>
                    <ReviewContent>
                      조건이 아주 좋아서 이곳에서 구입했는데 요금 청구 내용이
                      차이가 있어서 내일 정확히 확인해봐야 겠어요
                    </ReviewContent>
                  </td>
                  <td>이**</td>
                  <td>2021-10-01</td>
                </tr>
                <tr>
                  <td>357</td>
                  <td>아이폰 12</td>
                  <td>5점</td>
                  <td>
                    <ReviewContent>
                      <span>
                        조건이 아주 좋아서 이곳에서 구입했는데 요금 청구 내용이
                        차이가 있어서 내일 정확히 확인해봐야 겠어요
                      </span>
                    </ReviewContent>
                  </td>
                  <td>이**</td>
                  <td>2021-10-01</td>
                </tr>
                <tr>
                  <td>357</td>
                  <td>아이폰 12</td>
                  <td>5점</td>
                  <td>
                    <ReviewContent>
                      <span>
                        조건이 아주 좋아서 이곳에서 구입했는데 요금 청구 내용이
                        차이가 있어서 내일 정확히 확인해봐야 겠어요
                      </span>
                    </ReviewContent>
                  </td>
                  <td>이**</td>
                  <td>2021-10-01</td>
                </tr>
                <tr>
                  <td>357</td>
                  <td>아이폰 12</td>
                  <td>5점</td>
                  <td>
                    <ReviewContent>
                      <div>
                        조건이 아주 좋아서 이곳에서 구입했는데 요금 청구 내용이
                        차이가 있어서 내일 정확히 확인해봐야 겠어요
                      </div>
                    </ReviewContent>
                  </td>
                  <td>이**</td>
                  <td>2021-10-01</td>
                </tr>
              </tbody>
            </ReviewTable> */}
            <FaqRender qadata={ProductInfoData?.ProductQas} />
          </Things>
        </Inner>
      </Wrapper>
    </Fade>
  );
};

export default Product;
