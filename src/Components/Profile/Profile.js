import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { useAuth } from "Datas/storage";
import { GetStoreList, GetuserProfile } from "Datas/swr";
import moment from "moment";
import { updateUser, uploadImage, userUpdate } from "Datas/api";
import { Fade } from "react-reveal";

const breakingpoints = [375, 768, 992, 1200]; // 0 1 번째사용
const mq = breakingpoints.map((bp) => `@media(max-width:${bp}px)}`);

const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 600px;
  width: 100%;
  padding: 0 20px;
  margin-top: 200px;

  @media screen and (max-width: 768px) {
    margin-top: 150px;
  }
  @media screen and (max-width: 375px) {
    margin-top: 100px;
  }
`;

const ProfileBox = styled.div`
  width: 120px;

  text-align: center;
  font-weight: ${(props) => {
    if (props.bold) {
      return "bold";
    }
  }};

  ${mq[1]} {
    width: 120px;
  }
  ${mq[0]} {
    width: 110px;
  }
`;

const Top = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const Text = styled.div`
  display: flex;
  & > div:first-child {
    font-size: 36px;
  }
  & > div:last-child {
    font-size: 17px;
    color: #989898;
  }
  align-items: center;

  ${mq[0]} {
    width: 120px;
    & > div:first-child {
      font-size: 36px;
    }
    & > div:last-child {
      font-size: 15px;
      color: #989898;
    }
  }
`;

const Logout = styled.div`
  margin-left: auto;
  align-items: center;
  color: #ff3333;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  display: flex;
`;

const CredentialsInputText = styled.div`
  font-size: 25px;
  color: #838383;
  margin-top: 20px;

  @media screen and (max-width: 450px) {
    font-size: 17px;
  }
`;

const CredentialsInputInput = styled.input`
  width: 100%;
  margin-top: 5px;
  height: 65px;
  border: 2px solid #f0f0f0;
  border-radius: 10px;
  color: #000;
  padding: 10px;

  @media screen and (max-width: 450px) {
    height: 45px;
  }
`;

const ProfileIag = styled.div`
  background-image: ${(props) => {
    let url;
    if (props.url) {
      url = `url(${props.url})`;
    } else {
      url = "";
    }
    return url;
  }};
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  background-color: ${(props) => {
    let color;
    if (!props.url) {
      color = "#e3e3e3";
    }
    return color;
  }};
  width: 180px;
  height: 180px;
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 180px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: ${(props) => {
    let board;
    if (props.url) {
      board = "2px solid orange";
    }
    return board;
  }};

  @media screen and (max-width: 768px) {
    width: 130px;
    height: 130px;
  }
  @media screen and (max-width: 375px) {
    width: 100px;
    height: 100px;
  }
`;

const SignupBtn = styled.div`
  width: 100%;
  height: 65px;
  color: #fff;
  background: #feb43c;
  border: 1px solid #feb43c;
  border-radius: 10px;
  margin-top: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 25px;
  @media screen and (max-width: 450px) {
    height: 45px;
    font-size: 18px;
  }
  margin-bottom: 80px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const ImageLabel = styled.p`
  font-size: 16px;
  font-weight: 500;
  color: #a7a7a7;

  @media screen and (max-width: 768px) {
    font-size: 14px;
    font-weight: 500;
  }

  @media screen and (max-width: 375px) {
    font-size: 12px;
    font-weight: 500;
  }
`;

const Profile = ({ profile }) => {
  const { setLogout } = useAuth();
  const { StoreListData, StoreListMutate } = GetStoreList(0);
  const { UserProfileData, isLoading } = GetuserProfile(profile?.userId);
  const [image, setImage] = useState([]);
  const [userStore, setUserStore] = useState("");
  const [imgBase64, setImgBase64] = useState([]);
  const [datas, setDatas] = useState({
    useremail: "",
    name: "",
    birthday: "",
    office: "",
    rank: "",
    code: "",
    image: "",
  });
  const handleImage = (e) => {
    let reader = new FileReader();
    reader.onloadend = () => {
      const base64 = reader.result;
      if (base64) {
        setImgBase64([base64.toString()]);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
      setImage([e.target.files[0]]); //경로탈착
    }
  };
  useEffect(() => {
    if (!isLoading) {
      setDatas({
        useremail: UserProfileData?.useremail,
        name: UserProfileData?.name,
        birthday: moment(UserProfileData?.birthday).format("YYYY-MM-DD"),
        office: UserProfileData?.office,
        rank: UserProfileData?.rank,
        code: UserProfileData?.Store?.code,
        image: UserProfileData?.image[0],
      });
    }
  }, [UserProfileData]);
  console.log(UserProfileData);
  const _handleUpdate = async () => {
    if (datas?.image === "") delete datas.image;
    delete datas.code;

    //base 64 upload
    if (imgBase64.length > 0) {
      const ids = await Promise.all(
        image.map((uri) => {
          if (!!uri.id) return uri.id;
          let formData = new FormData();

          formData.append("files", uri);
          return uploadImage(formData);
        })
      );

      datas.image = ids;
    }
    await updateUser({ id: profile?.userId, body: datas });
  };

  const _handleLogout = async () => {
    setLogout();
    alert("로그아웃 완료");
  };

  console.log(profile);

  return (
    <Fade Button>
      <React.Fragment>
        <Wrapper>
          <div
            style={{
              alignItems: "center",
              display: "flex",
              justifyContent: "center",
              marginTop: 24,
              marginBottom: 24,
            }}
          >
            <input
              type="file"
              name="upload-file"
              style={{ display: "none" }}
              onChange={handleImage}
              id="upload-file"
            ></input>
            {imgBase64.length > 0 ? (
              <label for="upload-file">
                <ProfileIag for="upload-file" url={imgBase64[0]} />
              </label>
            ) : datas.image ? (
              <label for="upload-file">
                <ProfileIag url={datas.image} />
              </label>
            ) : (
              <label for="upload-file">
                <ProfileIag
                  for="upload-file"
                  url={datas.image}
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <ImageLabel>프로필 사진 등록</ImageLabel>
                </ProfileIag>
              </label>
            )}
          </div>

          <Top>
            <Text>
              <ProfileBox bold>
                {profile.gm === 1
                  ? `${
                      StoreListData?.rows.filter(
                        (item, idx) => item.id === profile?.storeid
                      )[0].name
                    }가맹주님`
                  : "최고 관리자"}
              </ProfileBox>
            </Text>
            <Text>
              <ProfileBox>
                누적수수료 <br />
                {profile?.salea ? profile?.salea : "권한이없습니다"}
              </ProfileBox>
            </Text>
            <Text>
              <ProfileBox>
                포인트
                <br />
                {profile?.point ? profile?.point : "권한이 없습니다"}
              </ProfileBox>
            </Text>
          </Top>

          <CredentialsInputText>이름</CredentialsInputText>
          <CredentialsInputInput
            placeholder="이름을 입력해주세요"
            value={datas?.name}
            onChange={(e) => setDatas({ ...datas, name: e.target.value })}
          />

          <CredentialsInputText>이메일</CredentialsInputText>
          <CredentialsInputInput
            placeholder="이메일을 입력해주세요"
            value={datas?.useremail}
            onChange={(e) => setDatas({ ...datas, useremail: e.target.value })}
          />

          <CredentialsInputText>생년월일</CredentialsInputText>
          <CredentialsInputInput
            type="date"
            placeholder="생년월일을 입력해주세요"
            value={datas?.birthday}
            onChange={(e) => setDatas({ ...datas, birthday: e.target.value })}
          />

          <CredentialsInputText>소속</CredentialsInputText>
          <CredentialsInputInput
            placeholder="소속을 입력해주세요"
            value={datas?.office}
            onChange={(e) => setDatas({ ...datas, office: e.target.value })}
          />

          <CredentialsInputText>직급</CredentialsInputText>
          <CredentialsInputInput
            placeholder="직급을 입력해주세요"
            value={datas?.rank}
            onChange={(e) => setDatas({ ...datas, rank: e.target.value })}
          />

          <CredentialsInputText>인증코드</CredentialsInputText>
          <CredentialsInputInput
            placeholder="인증코드를 선택해주세요"
            disabled={true}
            value={datas?.code}
            onChange={(e) => setDatas({ ...datas, code: e.target.value })}
          />

          {/* <CredentialsInputText>명함</CredentialsInputText>
        <CredentialsInputInput
          placeholder="인증코드를 선택해주세요"
          value={datas?.code}
          onChange={(e) => setDatas({ ...datas, code: e.target.value })}
        /> */}

          <SignupBtn onClick={_handleLogout}>로그아웃</SignupBtn>
          <SignupBtn onClick={_handleUpdate}>수정</SignupBtn>
        </Wrapper>
      </React.Fragment>
    </Fade>
  );
};

export default Profile;
