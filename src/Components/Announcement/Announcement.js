import React, { Component } from "react";
import styled from "styled-components";

import { GetBoardList, GetInforMation, GetOrderInfoData } from "Datas/swr";
import { Link } from "react-router-dom";
import Sliders from "Components/Sliders";
import { Fade } from "react-reveal";
import { useState } from "react";
import { useEffect } from "react";
import CustomPagination from "Settings/pagination";

const Wrapper = styled.div``;

const Leaderboard = styled.div`
  width: 100%;
  margin-top: 100px;
  height: 550px;
  background: #f0f0f0;

  @media screen and (max-width: 768px) {
    height: 320px;
  }

  @media screen and (max-width: 375px) {
    height: 207px;
  }
`;

const AnnouncementWrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1610px;
  padding: 0 20px;
  width: 100%;
  margin: 0 auto;
  margin-top: 80px;
  @media screen and (max-width: 768px) {
    margin-top: 60px;
  }
  @media screen and (max-width: 375px) {
    margin-top: 30px;
  }
`;

const AnnouncementText = styled.div`
  font-size: 25px;
  color: #feb43c;
  @media screen and (max-width: 768px) {
    font-size: 18px;
    font-weight: 600;
    margin-left: 2%;
  }
  @media screen and (max-width: 375px) {
    font-size: 14px;
    font-weight: 600;
    margin-left: 2%;
  }
`;

const AnnouncementSubText = styled.div`
  font-size: 32px;
  font-weight: 500;
  color: #000;
  @media screen and (max-width: 768px) {
    font-size: 26px;
    font-weight: 600;
    margin-left: 2%;
  }
  @media screen and (max-width: 375px) {
    font-size: 22px;
    font-weight: 600;
    margin-left: 2%;
  }
`;

const AnnouncementBoxWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  margin-bottom: 110px;
  margin-top: 50px;
  @media screen and (max-width: 768px) {
    margin-top: 45px;
  }
  @media screen and (max-width: 375px) {
    margin-top: 30px;
  }
`;

const AnnouncementBox = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;
  max-width: 735px;
  background: #fff;
  width: 100%;
  margin-bottom: 40px;
`;

const BoxThunbmail = styled.div`
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  background-image: ${(props) => {
    let url;
    if (props.url) {
      let link = props.url[0];
      console.log(link);
      url = `url(${link})`;
    } else {
    }
    return url;
  }};
  background-repeat: no-repeat;
  background-size: cover;
  height: 225px;
  background-color: #f0f0f0;
`;

const BoxTitle = styled.div`
  margin: 10px 20px 0 20px;
  font-size: 35px;
  font-weight: 500;
  @media screen and (max-width: 768px) {
    font-size: 28px;
    font-weight: 500;
  }
  @media screen and (max-width: 375px) {
    font-size: 22px;
    font-weight: 500;
  }
`;

const BoxDesc = styled.div`
  margin: 10px 20px 25px 20px;
  color: #a7a7a7;
`;

const Announcement = () => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  const { InfoData, InfoDataMutate } = GetInforMation();
  const { BoardListData, BoardListMutate, isLoading } = GetBoardList(offset);

  const [BoardData, setBoardData] = useState([]);
  useEffect(() => {
    if (!isLoading) {
      setBoardData(BoardListData?.rows);
    }
  }, [BoardListData]);

  return (
    <Fade Button>
      <Wrapper>
        <Sliders />
        <AnnouncementWrapper>
          <AnnouncementText>
          {InfoData !== undefined && InfoData[0].pagename}공지사항
          </AnnouncementText>
          <AnnouncementSubText>공지사항</AnnouncementSubText>
          <AnnouncementBoxWrapper>
            {BoardData.map((el, idx) => {
              return (
                <>
                  <AnnouncementBox>
                    <Link
                      to={{
                        pathname: `/announcementpost/${el.id}`,
                        state: {
                          title: el?.title,
                          createdAt: el?.createdAt,
                          contents: el?.contents,
                          //프로덕트 텔레콤의 데이터 아이디를 넣어준다
                        },
                      }}
                    >
                      <BoxThunbmail url={el?.images} />
                      <BoxTitle>{el?.title}</BoxTitle>
                      <BoxDesc>{el?.contents}</BoxDesc>
                    </Link>
                  </AnnouncementBox>
                </>
              );
            })}

            <CustomPagination
              data={BoardListData}
              setOffset={setOffset}
              page={page}
              setPage={setPage}
            />
          </AnnouncementBoxWrapper>
        </AnnouncementWrapper>
      </Wrapper>
    </Fade>
  );
};

export default Announcement;
