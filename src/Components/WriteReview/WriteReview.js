import React, { Component, useState } from "react";
import { Fade } from "react-reveal";
import styled from "styled-components";
import ReactStars from "react-stars";
import EditorMobile from "Components/EditorMobile";
import { GetUri, ImageHandler } from "Settings/imageHandler";
import axios from "axios";
import { GetUserOrderList } from "Datas/swr";
const Wrapper = styled.div``;

const WriteReviewText = styled.div`
  font-size: 35px;
  @media screen and (max-width: 450px) {
    font-size: 25px;
  }
  margin-bottom: 15px;
  font-weight: 700;
`;

const ReviewWrapper = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 590px;
  padding: 0 20px;
  width: 100%;
  margin: 0 auto;
  margin-top: 150px;
  margin-bottom: 110px;
`;

const CredentialsInputText = styled.div`
  font-size: 25px;
  color: #838383;
  margin-top: 25px;

  @media screen and (max-width: 450px) {
    font-size: 17px;
  }
`;

const CredentialsInputInput = styled.input`
  width: 100%;
  margin-top: 5px;
  height: 65px;
  border: 2px solid #f0f0f0;
  border-radius: 10px;
  color: #000;
  padding: 10px;

  @media screen and (max-width: 450px) {
    height: 45px;
  }
`;

const ReviewContentWrapper = styled.div``;

const ReviewContentTextarea = styled.textarea`
  width: 100%;
  border: 2px solid #f0f0f0;
  border-radius: 10px;
  padding: 10px;
  resize: none;
  height: 255px;
`;

const FinishBtn = styled.div`
  width: 100%;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  height: 65px;
  color: #fff;
  background: #feb43c;
  border: 1px solid #feb43c;
  border-radius: 10px;
  margin-top: 160px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 25px;
  @media screen and (max-width: 450px) {
    height: 45px;
    font-size: 18px;
  }
`;

const ReviewImgTop = styled.div`
  display: flex;
  align-items: center;
  margin-top: 25px;
`;

const ReviewImgImg = styled.div`
  background: #f0f0f0;
  height: 330px;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  background-repeat: no-repeat;
  background-size: cover;
  background-image: ${(props) => {
    let url;
    if (props.url) {
      url = `url(${props.url})`;
    } else {
    }
    return url;
  }};
  margin-top: 10px;
  border-radius: 10px;
`;

const ReviewImgText = styled.div`
  font-size: 25px;
  color: #838383;

  @media screen and (max-width: 450px) {
    font-size: 17px;
  }
`;

const ReviewImgWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const ReviewImgAddBtn = styled.label`
  display: flex;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  margin-left: auto;
  background: #ffb43c;
  font-weight: 700;
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  padding: 5px 11px;
  color: #fff;

  @media screen and (max-width: 450px) {
    font-size: 11px;
  }
`;
{
  /* 바꾸고싶다면 이부분을 플랫폼 분기하여주세용 :D -Murphy Studios */
}
const WriteReview = ({ profile, match, history }) => {
  const [data, setData] = useState({
    title: "",
    contetns: "",
    start: 0,
    images: "",
  });
  console.log(profile);
  console.log(match);
  const [BoardData, setBoardData] = useState("");
  const [filesdata, setfilesCheck] = useState([]);
  const [imgBase64, setImgBase64] = useState([]);
  const [image, setImage] = useState([]);
  const [files, setFiles] = useState([]);
  const { GetUserOrderListData, GetUserOrderListMutate } = GetUserOrderList(
    profile?.userId
  );
  const { handleRemove, handleCreate } = ImageHandler({
    setImgBase64: setImgBase64,
    setImage: setImage,
  });

  const onFilesChange = (files) => {
    console.log(files);
    setFiles(files);
  };
  const onEditorChange = (value) => {
    setBoardData(value);
  };

  const _handleSubmit = async () => {
    const { url } = await GetUri({ image: image });
    data.contents = BoardData;
    data.images = url;
    data.userid = profile?.userId;
    data.productid = match.params.id;
    data.orderid = match.params.orderid;
    try {
      let res = await axios.post("product/liviewCreate", data);
      if (res.data.status === 400) {
        alert(res.data.message);
      }
      else {
        alert("리뷰 등록 성공!");
      }
      await GetUserOrderListMutate();
      history.goBack();
    } catch {
      alert("리뷰 등록실패!");
    }
  };
  return (
    <Fade Button>
      <Wrapper>
        <ReviewWrapper>
          <WriteReviewText>리뷰 작성</WriteReviewText>

          <CredentialsInputText>별점</CredentialsInputText>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <ReactStars
              count={5}
              value={data.start}
              onChange={(e) => setData({ ...data, start: e })}
              size={58}
              color2={"orange"}
            />
          </div>
          <CredentialsInputText>제목</CredentialsInputText>
          <CredentialsInputInput
            value={data?.title}
            onChange={(e) => setData({ ...data, title: e.target.value })}
          ></CredentialsInputInput>
          <ReviewContentWrapper>
            <CredentialsInputText>내용</CredentialsInputText>
            <EditorMobile
              value={BoardData}
              onEditorChange={onEditorChange}
              setfilesCheck={setfilesCheck}
              filesdata={filesdata}
              onFilesChange={onFilesChange}
            />
          </ReviewContentWrapper>

          <ReviewImgWrapper>
            <ReviewImgTop>
              <ReviewImgText>타이틀 이미지</ReviewImgText>
              <ReviewImgAddBtn for="upload-file">추가</ReviewImgAddBtn>
              <input
                type="file"
                name="upload-file"
                style={{ display: "none" }}
                onChange={handleCreate}
                id="upload-file"
              ></input>
            </ReviewImgTop>
            {imgBase64.length !== 0 && (
              <ReviewImgImg
                onClick={() => handleRemove(0)}
                url={imgBase64.length !== 0 && imgBase64[0]}
              />
            )}
          </ReviewImgWrapper>
          <FinishBtn onClick={() => _handleSubmit()}>완료</FinishBtn>
        </ReviewWrapper>
      </Wrapper>
    </Fade>
  );
};

export default WriteReview;
