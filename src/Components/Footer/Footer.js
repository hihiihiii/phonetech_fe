import React, { useState } from "react";
import { StarIcon } from "assets";
import { withRouter } from "react-router";
import {
  FooterBox,
  FooterContainer,
  FooterImg,
  FooterContentBox,
  FooterContent,
  FooterText,
  ConfirmImage,
  ImageView,
} from "Settings/Footer";
import { GetInforMation } from "Datas/swr";

const Footer = ({ location }) => {
  const { InfoData, InfoDataMutate } = GetInforMation();
  const [footer, setFooter] = useState("");
  console.log("InfoData");
  console.log(InfoData);

  if (location.pathname.indexOf("admin") !== -1) {
    return <></>;
  } else {
    return (
      <>
        <FooterContainer footer={location.pathname === "/product"}>
          <FooterBox>
            <ImageView>
              <FooterImg src={InfoData !== undefined && InfoData[0].image} />
              <a
                href="https://www.ictmarket.or.kr:8443/precon/pop_CertIcon.do?PRECON_REQ_ID=PRE0000123159"
                target="_blank"
              >
                <ConfirmImage
                  src={
                    "https://www.ictmarket.or.kr:8443/getCertIcon.do?cert_icon=KP19102545493Y001"
                  }
                />
              </a>
            </ImageView>
            {/* <FooterContentBox>
              <FooterContent>
                <FooterText>(주)드림티에스</FooterText>
                <FooterText>대표 {InfoData[0]?.ceo}</FooterText>
                <FooterText>사업자등록번호 {InfoData[0]?.ceoCode}</FooterText>
                <FooterText>주소 {InfoData[0]?.zip}</FooterText>
              </FooterContent>
              <FooterContent>
                <FooterText>대표번호 {InfoData[0]?.mainCall}</FooterText>
              </FooterContent>
              <FooterContent>
                <FooterText>Email {InfoData[0]?.email}</FooterText>
              </FooterContent>
              <FooterContent>
                <FooterText>운영시간({InfoData[0]?.live})</FooterText>
              </FooterContent>
              <FooterContent>
                <FooterText>
                  통신판매업신고번호 ({InfoData[0]?.TeleCode})
                </FooterText>
              </FooterContent>
              <FooterContent>
                <FooterText>판매제휴업체 </FooterText>
                <FooterText>
                  사업자등록번호 {InfoData[0]?.salesPartner}
                </FooterText>
              </FooterContent>
              <FooterContent>
                <FooterText>대표 {InfoData[0]?.ceoName}</FooterText>
              </FooterContent>
              <FooterContent style={{ marginTop: 20 }}>
                copyrightⓒ 2021 PhoneTech. All Rights Reserved.
              </FooterContent>
            </FooterContentBox> */}
          </FooterBox>
        </FooterContainer>
      </>
    );
  }
};

export default withRouter(Footer);
