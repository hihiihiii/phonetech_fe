import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { GetBoardList, GetBoardrecList } from "Datas/swr";
import { Link } from "react-router-dom";
import { deleteBoardRec, deletePost } from "Datas/api";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Imgskeleton = styled.img`
  max-width: 200px;
  height: 100px;
  border: none;
  resize: both;
  margin: 0 auto;
  border-radius: 5px;
  object-fit: contain;
`;

const Postrec = () => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);

  //SWR
  const { BoardListrecData, BoardListrecMutate, isLoading } =
    GetBoardrecList(offset);

  const [BoardData, setBoardData] = useState([]);
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    let checkd = BoardData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [BoardData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화

    let data = [...BoardData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setBoardData(resdata);
    setChecked(true);
  };

  useEffect(() => {
    if (!isLoading) {
      setBoardData(BoardListrecData?.rows);
    }
  }, [BoardListrecData]);

  console.log(BoardListrecData);

  const _handleSelect = (id) => {
    let temp = [...BoardData];
    BoardData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
        console.log(data);
      }
    });
    setBoardData(temp);
  };
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      for (let index = 0; index < BoardData.length; index++) {
        if (BoardData[index]?.validate) {
          let id = BoardData[index]?.id;
          await deleteBoardRec({ id: id });
        }
      }
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );
  const _handleDelete = async () => {
    desk();
  };
 
  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>사전예약 관리</TopLabel>
          <TopButton>
            <Link to="/admin/postrec/detail/add">
              <Add>추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable>
          <colgroup></colgroup>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              ;<th>제목</th>
              <th>썸네일</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {BoardData?.length !== 0 &&
              BoardData?.map((el, idx) => {
                console.log(el);

                return (
                  <tr>
                    <LinedTag>
                      <input
                        type="checkbox"
                        checked={el?.validate}
                        onClick={() => _handleSelect(el?.id)}
                      />
                    </LinedTag>
                    <LinedTag>{el?.title}</LinedTag>
                    <LinedTag>
                      {el?.titleimages?.length !== 0 ? (
                        <Imgskeleton src={el?.titleimages[0]}></Imgskeleton>
                      ) : (
                        "없음"
                      )}
                    </LinedTag>
                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/postrec/detail/${el?.id}`,
                          state: el,
                        }}
                      >
                        수정
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={BoardListrecData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default Postrec;
