import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";

import { CheckPhone, validateEmail, validatePassword } from "Settings/common";
import { sends, userCreate, uploadImage } from "Datas/api";
import { getStoarge, setStoarge, useAuth } from "Datas/storage";
import { withRouter } from "react-router";
import Loader from "react-loader-spinner";
import FingerprintJS from "@fingerprintjs/fingerprintjs";
import TermsAgree from "Components/TermsAgree/TermsAgree";

const Wrapper = styled.div``;

const Inner = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 570px;
  padding: 0 20px;
`;

const SignupText = styled.div`
  font-size: 35px;
  padding-top: 1000px;
  @media screen and (max-width: 450px) {
    font-size: 25px;
  }
  margin-bottom: 15px;
  font-weight: 700;
`;

const CredentialsInputText = styled.div`
  font-size: 25px;
  color: #838383;
  margin-top: 20px;

  @media screen and (max-width: 450px) {
    font-size: 17px;
  }
`;

const CredentialsInputInput = styled.input`
  width: 100%;
  margin-top: 5px;
  height: 65px;
  border: 2px solid #f0f0f0;
  border-radius: 10px;
  color: #000;
  padding: 10px;

  @media screen and (max-width: 450px) {
    height: 45px;
  }
`;

const SignupBtn = styled.div`
  width: 100%;

  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  height: 65px;
  color: #fff;
  background: #feb43c;
  border: 1px solid #feb43c;
  border-radius: 10px;
  margin-top: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 25px;
  @media screen and (max-width: 450px) {
    height: 45px;
    font-size: 18px;
  }
  margin-bottom: 80px;
`;

const NameCardVerification = styled.div`
  margin-top: 20px;
`;

const NameCardTop = styled.div`
  display: flex;
  align-items: center;
`;

const NameCardText = styled.div`
  color: #838383;
  font-size: 25px;
  @media screen and (max-width: 450px) {
    font-size: 17px;
  }
`;

const SubTagButton = styled.div`
  width: 100%;
  height: 60px;
  :hover {
    opacity: 0.65;
  }
  cursor: pointer;
  color: #fff;
  background: ${(props) => {
    let color;
    if (props.able === true) {
      color = "#feb43c";
    } else {
      color = "#eee";
    }
    return color;
  }};
  border: 1px solid #feb43c;
  border-radius: 10px;
  margin-right: 12px;
  margin-top: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 25px;
`;
const NameCardAdd = styled.label`
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
  font-size: 20px;
  background: #fab43c;
  border-radius: 30px;
  padding: 7px 14px;
  color: #fff;
  margin-left: auto;
  @media screen and (max-width: 450px) {
    font-size: 15px;
  }
`;

const NameCardBox = styled.div`
  display: flex;
  width: 335px;
  height: 175px;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  margin-top: 10px;
  background-size: contain;
  background-repeat: no-repeat;
  background-image: ${(props) => {
    let url;
    if (props.url) {
      url = `url(${props.url})`;
    } else {
      url = "";
    }
    return url;
  }};

  @media screen and (max-width: 450px) {
    width: 216px;
    height: 113px;
  }
`;

const TintBox = styled.p`
  color: red;
  margin-top: 12px;
`;

const Register = ({ history }) => {
  const { setLogin } = useAuth();
  const fpPromise = FingerprintJS.load();
  const [acct, setAcct] = useState([]);
  const [data, setData] = useState({
    email: "",
    password: "",
    passwordCheck: "",
    name: "",
    useremail: "",
    devicetoken: "",
    office: "", //회사단체명
    code: "", //인증코드
    rank: "", //직급
    image: "", // 명함
    child: false, //성인/비성인여부
    fingertype: true,
    birthday: "", //생일
    finger: "", // 핑거프린트 고유 id
    fcm: "",
  });
  const [isTimer, setIsTimer] = useState(false);
  const [auth, setAuth] = useState({
    authcode: "",
    userauth: "",
    auth: false,
    check: false,
  });
  const [image, setImage] = useState([]);
  const [imgBase64, setImgBase64] = useState([]);
  const [seconds, setSeconds] = useState(parseInt(0));
  const [minutes, setMinutes] = useState(parseInt(5));

  useEffect(() => {
    const countdown = setInterval(() => {
      if (parseInt(seconds) > 0) {
        setSeconds(parseInt(seconds) - 1);
      }
      if (parseInt(seconds) === 0) {
        if (parseInt(minutes) === 0) {
          clearInterval(countdown);
        } else {
          setMinutes(parseInt(minutes) - 1);
          setSeconds(59);
        }
      }
    }, 1000);
    return () => clearInterval(countdown);
  }, [minutes, seconds]);

  const _confrim = () => {
    if (auth.authcode == auth.userauth) {
      setAuth({ ...auth, check: true, auth: false });
      alert("인증 완료!");
    } else {
      alert("인증번호가 틀립니다");
    }
  };
  const handleImage = (e) => {
    let reader = new FileReader();
    reader.onloadend = () => {
      const base64 = reader.result;
      if (base64) {
        setImgBase64([base64.toString()]);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
      setImage([e.target.files[0]]); //경로탈착
    }
  };

  const _sends = async () => {
    //특점함수 호출
    setIsTimer(true);
    let result = await sends({ tel: data.email });
    if (result === undefined) {
      alert("인증번호 발급실패 네트워크를 확인하여주세요");
    } else {
      alert("인증 번호 발급성공 인증을 진행하여주세요");
    }
    setIsTimer(false);
    //특정함수발생 2
    setMinutes(5); //상태검증 분
    setSeconds(0); //상태검증 초
    setAuth({ ...auth, authcode: result.data.code, auth: true }); //코드할당 및 인증상태
    //타이머 관련 세팅
  };

  //인증코드 중복 핸들러
  const handleRemoveImage = (idx) => {
    setImage((images) => images.filter((el, index) => index !== idx));
    setImgBase64((imgBase64) => imgBase64.filter((el, index) => index !== idx));
  };
  //제거용
  //이미지 업로드 핸들러

  const handleSignUp = async () => {
    let fcm = await getStoarge("fcmToken");
    fcm = await JSON.parse(fcm);
    const fp = await fpPromise;
    const result = await fp.get();
    const visitorId = result.visitorId;
    data.finger = visitorId;
    data.fcm = fcm?.fcmToken;

    if (auth.check === false) {
      alert("휴대폰 인증 이후 가입하여주세요");
      return;
    }
    if (acct.length !== 0) {
      let res = acct.findIndex((item, idx) => {
        return item.agreed === !true;
      });

      if (res !== -1) {
        alert("약관을 동의해주세요");
        return;
      }
    }
    console.log(data);
    if (
      data.email === "" ||
      data.passwordCheck === false ||
      data.email === "" ||
      data.office === "" ||
      data.name === "" ||
      data.useremail === "" ||
      data.rank === "" ||
      data.code === "" ||
      data.birthday === ""
    ) {
      alert("빈칸을 확인하여주세요");
      return;
    }
    if (image.length === 0) {
      alert("명함을 필수로 인증해주세요");
      return;
    }
    const ids = await Promise.all(
      image.map((uri) => {
        if (!!uri.id) return uri.id;
        let formData = new FormData();

        formData.append("files", uri);
        return uploadImage(formData);
      })
    );

    data.image = ids;
    console.log(data);
    delete data.passwordCheck;
    data.auth = true;
    let res = await userCreate(data);
    if (res === undefined) {
      alert("존재하지않는 상점코드이거나 이미 가입된 계정입니다!");
    } else {
      if (res.data.status == 400) {
        alert(res.data.message);
        return;
      }

      //여기라우터주세요
      alert("회원가입이 완료되었습니다 승인후 이용하여주세요");
      history.push("/");
    }
    //빈칸검증이 끝나면 회원가입
  };

  //이미지 업로더 만들어야함
  return (
    <Wrapper>
      <Inner>
        <SignupText style={{ marginTop: 154 }}>회원가입</SignupText>
        {auth.auth ? (
          <>
            <CredentialsInputText>인증번호 입력</CredentialsInputText>
            <CredentialsInputInput
              value={auth.userauth}
              onChange={(e) => setAuth({ ...auth, userauth: e.target.value })}
              placeholder="인증번호를 입력해주세요"
            />
            <TintBox>
              {minutes} 분 : {seconds} 초
            </TintBox>
            <div style={{ display: "flex" }}>
              <SubTagButton able onClick={() => _confrim()}>
                인증
              </SubTagButton>
              <SubTagButton
                able
                onClick={() => setAuth({ ...auth, authcode: "", auth: false })}
              >
                재인증
              </SubTagButton>
            </div>
          </>
        ) : (
          <>
            <CredentialsInputText>
              아이디 (휴대폰번호 입력)
            </CredentialsInputText>
            <CredentialsInputInput
              value={data.email}
              disabled={auth.check}
              onChange={(e) => setData({ ...data, email: e.target.value })}
              placeholder="휴대폰 번호를 입력해주세요 (-) 제외"
            />
            {auth.check === false && (
              <>
                {!CheckPhone(data.email) && (
                  <TintBox>휴대폰 번호 형식이 아닙니다.</TintBox>
                )}

                <SubTagButton able onClick={() => _sends()}>
                  {isTimer ? (
                    <Loader
                      type="TailSpin"
                      color="white"
                      height={35}
                      width={35}
                    />
                  ) : (
                    "인증"
                  )}
                </SubTagButton>
              </>
            )}
          </>
        )}

        <CredentialsInputText>비밀번호</CredentialsInputText>
        <CredentialsInputInput
          value={data.password}
          onChange={(e) => setData({ ...data, password: e.target.value })}
          type="password"
          placeholder="비밀번호를 입력해주세요"
        ></CredentialsInputInput>
        <TintBox>{validatePassword(data.password)}</TintBox>
        <CredentialsInputInput
          value={data.passwordCheck}
          onChange={(e) => setData({ ...data, passwordCheck: e.target.value })}
          placeholder="비밀번호 확인"
          type="password"
        ></CredentialsInputInput>
        {!validatePassword(data.password) &&
          data.passwordCheck !== data.password && (
            <TintBox>비밀번호가 서로 틀립니다.</TintBox>
          )}
        <CredentialsInputText>이름</CredentialsInputText>
        <CredentialsInputInput
          value={data.name}
          onChange={(e) => setData({ ...data, name: e.target.value })}
          placeholder="이름을 입력해주세요."
        ></CredentialsInputInput>
        <CredentialsInputText>이메일</CredentialsInputText>
        <CredentialsInputInput
          value={data.useremail}
          onChange={(e) => setData({ ...data, useremail: e.target.value })}
          placeholder="이메일을 입력해주세요."
        ></CredentialsInputInput>
        <TintBox>{validateEmail(data.useremail)}</TintBox>
        <CredentialsInputText>회사 / 단체명</CredentialsInputText>
        <CredentialsInputInput
          value={data.office}
          onChange={(e) => setData({ ...data, office: e.target.value })}
          placeholder="회사/단체명 을 입력해주세요"
        ></CredentialsInputInput>
        <CredentialsInputText>직급</CredentialsInputText>
        <CredentialsInputInput
          value={data.rank}
          onChange={(e) => setData({ ...data, rank: e.target.value })}
          placeholder="직급 을 입력해주세요"
        ></CredentialsInputInput>
        <CredentialsInputText>인증코드</CredentialsInputText>
        <CredentialsInputInput
          value={data.code}
          onChange={(e) => setData({ ...data, code: e.target.value })}
          placeholder="인증코드를 입력해주세요"
        ></CredentialsInputInput>
        <CredentialsInputText>성인여부</CredentialsInputText>

        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <SubTagButton
            onClick={() => setData({ ...data, child: false })}
            able={data.child === false}
          >
            성인
          </SubTagButton>
          <SubTagButton
            onClick={() => setData({ ...data, child: true })}
            able={data.child === true}
          >
            비성인
          </SubTagButton>
        </div>
        <CredentialsInputText>생일</CredentialsInputText>
        <CredentialsInputInput
          value={data.birthday}
          onChange={(e) => setData({ ...data, birthday: e.target.value })}
          type="date"
          max="2500-12-31"
          min="1800-01-01"
        />
        <NameCardVerification>
          <NameCardTop>
            <NameCardText>명함 인증</NameCardText>
            <input
              type="file"
              name="upload-file"
              style={{ display: "none" }}
              onChange={handleImage}
              id="upload-file"
            ></input>
            <NameCardAdd for="upload-file">추가</NameCardAdd>
          </NameCardTop>
          {imgBase64 &&
            imgBase64.map((item, idx) => {
              return (
                <NameCardBox
                  onClick={() => handleRemoveImage(idx)}
                  url={item}
                ></NameCardBox>
              );
            })}
        </NameCardVerification>
        <TermsAgree
          SignUp
          acct={acct}
          setAcct={setAcct}
          termstype={false}
        ></TermsAgree>
        <SignupBtn onClick={() => handleSignUp()}>회원가입</SignupBtn>
      </Inner>
    </Wrapper>
  );
};

export default withRouter(Register);
