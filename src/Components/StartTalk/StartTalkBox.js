import { GetInforMation } from "Datas/swr";
import React from "react";
import styled from "styled-components";
import KakaoIcon from "kakaotalk.png";
import CallIconPng from "call.png";
import { Arrow_Up } from "assets";
import { handlerTopMovement } from "Settings/util";
import { withRouter } from "react-router";

const breakpoints = [375, 768, 992, 1200, 1425]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const StartTalk = styled.div`
  position: fixed;
  right: 50px;
  bottom: 100px;
  @media screen and (max-width: 450px) {
    position: fixed;
    overflow-y: scroll;
    overflow-x: hidden;
    right: 25px;
    bottom: 75px;
  }
`;

const KakaoTalk = styled.a`
  display: flex;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  width: 256px;
  height: 60px;
  align-items: center;
  background: #f5f5f5;
  border-radius: 30px;
  & > div {
    margin-left: 15px;
  }
  font-size: 20px;
  @media screen and (max-width: 450px) {
    width: 42px;
    height: 42px;
    font-size: 12px;
    box-shadow: none;
  }
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
`;

const KakaoTalkIcon = styled.div`
  height: 60px;
  width: 60px;
  margin-left: auto !important;
  border-radius: 100%;

  @media screen and (max-width: 450px) {
    height: 42px;
    width: 42px;
  }

  & > img {
    width: 100%;
    height: 100%;
    border-radius: 100%;
  }
`;

const CallTalk = styled.a`
  display: flex;
  cursor: pointer;
  :hover {
    opacity: 0.5;
  }
  width: 256px;
  height: 60px;
  align-items: center;
  margin-top: 15px;
  background-color: #ededed;
  border-radius: 30px;
  & > div {
    margin-left: 15px;
  }
  font-size: 20px;
  @media screen and (max-width: 450px) {
    width: 42px;
    height: 42px;
    font-size: 12px;
    box-shadow: none;
  }
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
`;

const CallIcon = styled.div`
  height: 60px;
  width: 60px;
  margin-left: auto !important;
  border-radius: 100%;

  @media screen and (max-width: 450px) {
    height: 42px;
    width: 42px;
  }

  & > img {
    width: 100%;
    height: 100%;
    border-radius: 100%;
  }
`;

const PhoneText = styled.div`
  font-size: 17px;
  font-weight: 500;
  @media screen and (max-width: 450px) {
    display: none;
  }
`;

const StartTalkBox = ({ location }) => {
  const { InfoData, InfoDataMutate } = GetInforMation();

  if (location.pathname.indexOf("admin") !== -1) {
    return <></>;
  } else {
    return (
      <>
        <StartTalk>
          <KakaoTalk href={InfoData !== undefined && InfoData[0].kakaoTalk}>
            <PhoneText>카카오톡상담</PhoneText>
            <KakaoTalkIcon>
              <img alt="" src={KakaoIcon} />
            </KakaoTalkIcon>
          </KakaoTalk>
          <CallTalk href={`tel:${InfoData !== undefined && InfoData[0].call}`}>
            <PhoneText>전화 상담</PhoneText>
            <CallIcon>
              <img alt="" src={CallIconPng} />
            </CallIcon>
          </CallTalk>

          <CallTalk
            onClick={() => {
              handlerTopMovement();
            }}
          >
            <PhoneText>위로</PhoneText>
            <CallIcon>
              <img alt="" src={Arrow_Up} />
            </CallIcon>
          </CallTalk>
        </StartTalk>
      </>
    );
  }
};

export default withRouter(StartTalkBox);
