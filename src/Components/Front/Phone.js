import React, { Component, useEffect } from "react";
import styled from "styled-components";
import PhoneImgPng from "phone.png";
import { Link } from "react-router-dom";
import { useState } from "react";
import { GetProductInfo, GetTelecomList } from "Datas/swr";
import { isObjectMethod } from "@babel/types";
import { size } from "lodash";
import { Arrow_Bottom } from "assets";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 50px;
  width: calc(100% / 4 - 16px - 0.01px);
  //width: 24%;
  margin-left: calc(16px / 4);
  margin-right: calc(16px / 4);
  align-items: center;
  justify-content: center;
  //margin: 0px auto;

  -webkit-box-shadow: 5px 5px 15px 5px #eee;
  box-shadow: 5px 5px 15px 5px #eee;

  @media screen and (max-width: 1430px) {
    width: calc(100% / 3 - 16px - 0.01px);
    margin-left: calc(16px / 3);
    margin-right: calc(16px / 3);
  }

  @media screen and (max-width: 1150px) {
    width: calc(100% / 2 - 16px - 0.01px);
    margin-left: calc(16px / 2);
    margin-right: calc(16px / 2);
  }
  :hover {
    background-color: #f5f5f5;
  }
`;

const PhoneImg = styled.div`
  & > img {
    width: 100%;
  }
  max-width: 334px;
  object-fit: cover;
  resize: both;
  @media screen and (max-width: 718px) {
    max-width: 280px;
  }
  @media screen and (max-width: 359px) {
    max-width: 140px;
  }
`;

const PhoneInfo = styled.div`
  display: flex;
  flex-direction: column;

  max-width: 335px;
`;

const PhoneNameWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const PhoneName = styled.div`
  font-size: 30px;
  font-weight: 700;
  margin-right: 10px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  @media screen and (max-width: 450px) {
    font-size: 16px;
  }
`;

const Phone5G = styled.div`
  margin-top: 5px;
  display: flex;
  //margin-left: auto;
  margin-right: auto;
  background: #fff;
  font-weight: 700;
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  //padding: 5px 11px;
  color: orangered;

  @media screen and (max-width: 450px) {
    font-size: 15px;
  }
`;

const PhoneRatePlan = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`;

const PhoneRate = styled.div`
  :hover {
    background-color: #feb43c;
    color: white;
    border-radius: 16px;
    padding-left: 6px;
    padding-right: 6px;
    width: 100%;
  }
  display: flex;
  margin: 3px 0;
  align-items: center;
`;

const RateName = styled.div`
  font-size: 16px;
  @media screen and (max-width: 450px) {
    font-size: 11px;
  }
`;

const PhonePrice = styled.div`
  margin-left: auto;
  font-size: 22px;
  font-weight: 600;
  cursor: pointer;

  display: inline;
  @media screen and (max-width: 450px) {
    font-size: 15px;
  }
`;

const RateFlexBox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 125px;
  height: 20px;
`;

const RateDiscount = styled.div`
  font-size: 20px;
  height: 20px;
  color: red;
  font-weight: bold;
  display: flex;
  align-items: center;
  @media screen and (max-width: 450px) {
    font-size: 16px;
  }
`;

const HeadingText = styled.div`
  width: fit-content;
  font-size: 22px;
  font-weight: 600;
  display: inline;
  box-shadow: inset 0 -10px 0 #ffe8d4;
  @media screen and (max-width: 450px) {
    font-size: 15px;
  }
`;

const Line = styled.div`
  width: 100%;
  height: 1px;
  background: #dddddd;
  margin: 10px 0;
`;

const ReleaseAndBuyPriceWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const ReleasePriceWrapper = styled.div`
  display: flex;
`;

const ReleasePriceText = styled.div`
  font-size: 20px;
  font-weight: 500;
  @media screen and (max-width: 450px) {
    font-size: 12px;
  }
`;

const ReleasePrice = styled.div`
  text-decoration: line-through;
  margin-left: auto;
  font-size: 20px;
  font-weight: 600;
  @media screen and (max-width: 450px) {
    font-size: 13px;
  }
`;

const BuyPriceWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const BuyPriceText = styled.div`
  font-size: 20px;
  font-weight: 500;
  @media screen and (max-width: 450px) {
    font-size: 12px;
  }
`;

const BuyPrice = styled.div`
  margin-left: auto;
  font-size: 25px;
  font-weight: 700;
  @media screen and (max-width: 450px) {
    font-size: 15px;
  }
`;

const ArrowIcon = styled.div`
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
  width: 17px;
  height: 16px;
  background-repeat: no-repeat;
  object-fit: contain;
`;

const Phone = ({ data }) => {
  const [toggle, setToggle] = useState(true);
  const { TelecomListData, TelecomListMutate } = GetTelecomList();

  const ConsumePrice = Number(data?.marketprice);

  console.log("-------------------------------------------------------");
  console.log("data");
  console.log(data);
  console.log("ConsumePrice");
  console.log(ConsumePrice);
  console.log("-------------------------------------------------------");

  const [discount, setDiscount] = useState("공시지원할인");

  const openMenu = () => {
    window.scrollTo(0, 0);
  };

  return (
    <Wrapper>
      {toggle ? (
        <PhoneInfo style={{ width: "100%", padding: "15px 15px" }}>
          <PhoneImg>
            <img alt="" src={data?.images} style={{ borderRadius: 7 }} />
          </PhoneImg>

          <HeadingText style={{ marginBottom: -12 }}>{data.name}</HeadingText>

          {/* <div
            style={{
              width: "100%",
              height: 34,
              display: "flex",
              marginTop: 24,
            }}
          >
            <div
              onClick={() => {

                setDiscount("공시지원할인");
              }}
              style={{
                width: "50%",
                height: "100%",
                backgroundColor: "#a7a7a7",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                fontSize: 15,
                fontWeight: "600",
                color: "#fff",
                cursor: "pointer",
                backgroundColor:
                  discount === "공시지원할인" ? "orange" : "#ffe8d4",
                color: discount === "공시지원할인" ? "#fff" : "orange",
              }}
            >
              공시지원할인
            </div>
            <div
              onClick={() => setDiscount("선택약정할인")}
              style={{
                width: "50%",
                height: "100%",
                backgroundColor: "#f0f0f0",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                fontSize: 15,
                fontWeight: "600",
                color: "#fff",
                cursor: "pointer",
                backgroundColor:
                  discount !== "공시지원할인" ? "orange" : "#ffe8d4",
                color: discount !== "공시지원할인" ? "#fff" : "orange",
              }}
            >
              선택약정할인
            </div>
          </div> */}

          <PhoneRatePlan>
            {TelecomListData?.map((item, idx) => {
              let FilterOption = data?.telecom_product_key_product?.filter(
                (item1) => item1.telecomid === item.id
              );
              //텔레콤 리스트가 맞는 필터 배열을 생성
              //해당배열을 반복돌리며조회
              let ValidateFilter = FilterOption.filter(
                (item, idx) => item.telecom_product.validate === true
              )[0];

              return (
                <>
                  <Line style={{ marginBottom: 16 }} />
                  <RateFlexBox>
                    <RateName
                      style={{
                        marginBottom: 5,
                        fontWeight: "600",
                        width: "fit-content",
                        boxShadow: "inset 0 -8px 0 #d9fcdb",
                      }}
                    >
                      {item?.name}
                    </RateName>
                    <RateDiscount>
                      {data?.case === 0 ? (
                        <>
                          {Math.floor(
                            ((ValidateFilter?.telecom_product.dc_sales_price +
                              ValidateFilter?.telecom_product
                                .dc_sales_change_price) /
                              data?.marketprice) *
                              100
                          ).toLocaleString()}
                          %
                          <ArrowIcon
                            style={{ marginLeft: 2 }}
                            url={Arrow_Bottom}
                          ></ArrowIcon>
                        </>
                      ) : (
                        <>
                          {Math.floor(
                            ((ValidateFilter?.telecom_product.dc_choose_price +
                              ValidateFilter?.telecom_product
                                .dc_choose_change_price) /
                              data?.marketprice) *
                              100
                          ).toLocaleString()}
                          %
                          <ArrowIcon
                            style={{ marginLeft: 2 }}
                            url={Arrow_Bottom}
                          ></ArrowIcon>
                        </>
                      )}
                      {/* {Math.floor(
                        ((ValidateFilter?.telecom_product.dc_sales_price +
                          ValidateFilter?.telecom_product
                            .dc_sales_change_price) /
                          data?.marketprice) *
                          100
                      ).toLocaleString()}
                      %
                      <ArrowIcon
                        style={{ marginLeft: 2 }}
                        url={Arrow_Bottom}
                      ></ArrowIcon> */}
                    </RateDiscount>
                  </RateFlexBox>

                  <Link
                    onClick={() => {
                      openMenu();
                    }}
                    to={{
                      pathname: "/product",
                      state: {
                        id: data.id, //상품아이디
                        type: "changePhone",
                        tel: item?.name, //선택 텔레콤네임
                        newtel: item?.name, //기기만 변경이기에 기존 텔레콤을상속
                        chooseBase: ValidateFilter.id, //선택요금제 데이터아이디
                        discount: "공시지원금",
                        //프로덕트 텔레콤의 데이터 아이디를 넣어준다
                      },
                    }}
                  >
                    <PhoneRate>
                      <RateName>기기변경 구매가</RateName>
                      <PhonePrice
                        style={{
                          boxShadow: "inset 0 -3px 0 red",
                        }}
                      >
                        {data?.case === 0 ? (
                          <>
                            {(
                              data?.marketprice -
                              ValidateFilter?.telecom_product.dc_sales_price -
                              ValidateFilter?.telecom_product
                                .dc_sales_change_price
                            ).toLocaleString()}
                          </>
                        ) : (
                          <>
                            {(
                              data?.marketprice -
                              ValidateFilter?.telecom_product.dc_choose_price -
                              ValidateFilter?.telecom_product
                                .dc_choose_change_price
                            ).toLocaleString()}
                          </>
                        )}
                        {/* {(
                          data?.marketprice -
                          ValidateFilter?.telecom_product.dc_sales_price -
                          ValidateFilter?.telecom_product.dc_sales_change_price
                        ).toLocaleString()} */}
                      </PhonePrice>
                    </PhoneRate>
                  </Link>

                  <Link
                    onClick={() => {
                      openMenu();
                    }}
                    to={{
                      pathname: "/product",
                      state: {
                        id: data.id,
                        type: "changeNum",
                        chooseBase: ValidateFilter.id, //선택요금제 데이터아이디
                        tel: item?.name === "SKT" ? "LGU+" : "SKT",
                        newtel: item?.name,
                        discount: "공시지원금",
                      },
                    }}
                  >
                    <PhoneRate>
                      <RateName>번호이동 구매가</RateName>
                      <PhonePrice
                        style={{
                          boxShadow: "inset 0 -3px 0 #d9fcdb",
                        }}
                      >
                        {data?.case === 0 ? (
                          <>
                            {(
                              data?.marketprice -
                              ValidateFilter?.telecom_product.dc_sales_price -
                              ValidateFilter?.telecom_product.dc_sales_mnp_price
                            ).toLocaleString()}
                          </>
                        ) : (
                          <>
                            {(
                              data?.marketprice -
                              ValidateFilter?.telecom_product.dc_choose_price -
                              ValidateFilter?.telecom_product
                                .dc_choose_mnp_price
                            ).toLocaleString()}
                          </>
                        )}
                        {/* {(
                          data?.marketprice -
                          ValidateFilter?.telecom_product.dc_sales_price -
                          ValidateFilter?.telecom_product.dc_sales_mnp_price
                        ).toLocaleString()} */}
                      </PhonePrice>
                    </PhoneRate>
                  </Link>
                </>
              );
            })}
          </PhoneRatePlan>
          <Line />
          <ReleasePriceWrapper>
            <ReleasePriceText>출고가</ReleasePriceText>
            <ReleasePrice style={{ textDecoration: "none", color: "red" }}>
              {/* {Number(data?.marketprice)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")} */}
              {ConsumePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}

              {/* {data?.marketprice.toLocaleString()}원 */}
            </ReleasePrice>
          </ReleasePriceWrapper>
          {/*    <div
            className="buttonTag"
            onClick={() => setToggle(false)}
            style={{
              width: "100%",
              height: 40,
              marginTop: 25,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "orange",
              color: "#fff",
              margin: "10px 0px",
              fontSize: 13,
              fontWeight: "600",
              borderRadius: 100,
            }}
          >
            뒤로가기
          </div> */}
        </PhoneInfo>
      ) : (
        <>
          <PhoneImg>
            <img alt="" src={data?.images} style={{ borderRadius: 7 }} />
          </PhoneImg>

          <PhoneInfo style={{ width: "100%", padding: "0px 15px" }}>
            <PhoneInfo>
              <Phone5G> {data?.giga}</Phone5G>
              <PhoneNameWrapper>
                <PhoneName>{data?.name}</PhoneName>
                {/* <Phone5G> {data?.giga}</Phone5G> */}
              </PhoneNameWrapper>
            </PhoneInfo>
            <div
              onClick={() => setToggle(true)}
              className="buttonTag"
              style={{
                width: "100%",
                height: 40,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                backgroundColor: "orange",
                color: "#fff",
                margin: "10px 0px",
                fontSize: 13,
                fontWeight: "600",
                borderRadius: 100,
              }}
            >
              가격 보러가기
            </div>
          </PhoneInfo>
        </>
      )}

      {/* <PhoneImg>
        <img alt="" src={data?.images} />
      </PhoneImg>
      <PhoneInfo>
        <PhoneNameWrapper>
          <PhoneName> {data?.name}</PhoneName>
          <Phone5G> {data?.giga}</Phone5G>
        </PhoneNameWrapper>
        <PhoneRatePlan>
          {data?.telecom_product_key_product?.map((item, idx) => {
            return (
              <Link
                to={{
                  pathname: "/product",
                  state: {
                    id: data.id,
                  },
                }}
              >
                <PhoneRate>
                  <RateName>
                    {item?.telecom_product?.dc_change_price &&
                      `${item.name} 번호이동`}
                  </RateName>
                  <PhonePrice>
                    {item?.telecom_product?.dc_change_price &&
                      `${item?.telecom_product?.dc_change_price}`}
                  </PhonePrice>
                </PhoneRate>
                <PhoneRate>
                  <RateName>
                    {item?.telecom_product?.dc_move_price &&
                      `${item.name} 기기변경`}
                  </RateName>
                  <PhonePrice>
                    {item?.telecom_product?.dc_move_price &&
                      item?.telecom_product?.dc_move_price}
                  </PhonePrice>
                </PhoneRate>
              </Link>
            );
          })}
        </PhoneRatePlan>

        <Line />
        <ReleaseAndBuyPriceWrapper>
          <ReleasePriceWrapper>
            <ReleasePriceText>출고가</ReleasePriceText>
            <ReleasePrice>{data?.marketprice}원</ReleasePrice>
          </ReleasePriceWrapper>
          <BuyPriceWrapper>
            <BuyPriceText>구매가</BuyPriceText>
            <BuyPrice>{data?.price}원</BuyPrice>
          </BuyPriceWrapper>
        </ReleaseAndBuyPriceWrapper>
      </PhoneInfo> */}
    </Wrapper>
  );
};

export default Phone;
