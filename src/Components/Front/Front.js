import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Phone from "Components/Front/Phone.js";

import { GetproductList, GetPromotionList } from "Datas/swr";

import { phoneProducts } from "Settings/mockData";
import Sliders from "Components/Sliders";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";

import { withRouter } from "react-router";
import StartTalkBox from "Components/StartTalk/StartTalkBox";
import CustomerReviewMain from "Components/CustomerReviewMain/CustomerReviewMain";
const breakpoints = [450, 768, 992, 1200]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const Wrapper = styled.div``;

const Leaderboard = styled.img`
  width: 100%;
  margin-top: 100px;
  height: 550px;
  background: #f0f0f0;

  @media screen and (max-width: 768px) {
    height: 320px;
  }

  @media screen and (max-width: 375px) {
    height: 207px;
  }
`;

const Main = styled.div`
  display: flex;
  max-width: 1610px;
  padding: 0 20px;
  margin: 0 auto;
  width: 100%;
  flex-direction: column;
`;

const PhoneTechTop6 = styled.div`
  display: flex;
  color: #feb43c;
  font-size: 25px;

  margin-top: 96px;

  @media screen and (max-width: 768px) {
    font-size: 22px;
  }

  @media screen and (max-width: 375px) {
    font-size: 18px;
  }
`;

const WhichPhoneHot = styled.div`
  display: flex;
  font-size: 35px;
  font-weight: 600;
  margin-top: 50px;

  width: fit-content;
  box-shadow: inset 0 -8px 0 #ffcea3;
  @media screen and (max-width: 768px) {
    font-size: 26px;
    font-weight: 600;
    margin-left: 2%;
    margin-top: 40px;
  }
  @media screen and (max-width: 375px) {
    font-size: 20px;
    font-weight: 600;
    margin-left: 2%;
    margin-top: 30px;
  }
`;

const PhoneWrapper = styled.div`
  display: flex;
  //justify-content: space-between;
  flex-wrap: wrap;
  position: relative;
`;

const CustomerReviewBox = styled.div`
  margin: 0px auto;
  max-width: 1640px;
  padding: 0px 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  ${mq[0]} {
    align-items: flex-start;
    justify-content: flex;
  }

  ${mq[1]} {
    align-items: flex-start;
    justify-content: flex;
  }
`;

const CustomerReivewText = styled.div`
  box-shadow: inset 0 -8px 0 #ffcea3;
  font-size: 28px;
  font-weight: 600;
`;

const TitleFlexBox = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const AlgnBox = styled.div`
  margin-top: 50px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 120px;
  height: 35px;
`;

const Algn = styled.div`
  width: 55px;
  height: 33px;
  border-radius: 8px;
  background-color: orange;
  color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

const Promotion = ({ promotion }) => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  const [filterValue, setFilterValue] = useState(0);

  const { ProductListData } = GetproductList(
    "promotion",
    promotion?.id,
    0,
    offset,
    filterValue
  );

  // const _handleSortProduct = (type) => { 한번해봅시다
  //   if (type === "ascending") {
  //     const arr = ProductListData?.rows.map((el, idx) => el?.marketprice);
  //     arr.sort();
  //     console.log(arr);
  //     return arr;
  //   } else if (type === "descending") {
  //     const arr = ProductListData?.rows.map((el, idx) => el?.marketprice);
  //     arr.sort((a, b) => {
  //       return arr;
  //     });
  //     console.log(arr);
  //   }
  // };

  //수리시작
  return (
    <Main>
      <TitleFlexBox>
        <WhichPhoneHot>{promotion?.title}</WhichPhoneHot>
        <AlgnBox>
          <Algn
            onClick={() => {
              setFilterValue(0);
            }}
          >
            낮은순
          </Algn>
          <Algn
            onClick={() => {
              setFilterValue(1);
            }}
          >
            높은순
          </Algn>
        </AlgnBox>
      </TitleFlexBox>
      <PhoneWrapper style={{ marginTop: 50 }}>
        {ProductListData?.rows.map((el, idx) => {
          return (
            <>
              <Phone data={el} />
            </>
          );
        })}
        <CustomPagination
          data={ProductListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </PhoneWrapper>
    </Main>
  );
};

const Front = () => {
  const { PromotionListData } = GetPromotionList(0);

  return (
    <>
      <Fade Button>
        <Wrapper>
          <Sliders />
          {PromotionListData !== undefined &&
            PromotionListData?.rows?.length !== 0 &&
            PromotionListData?.rows?.map((el, idx) => {
              if (!el?.type) {
                return;
              }
              return <Promotion promotion={el} />;
            })}
          {/* mockup data */}
          {/* <Main>
          <WhichPhoneHot>폰테크 최고할인 상품</WhichPhoneHot>
          <PhoneWrapper style={{ marginTop: 50 }}>
            {phoneProducts.map((el, idx) => {
              return <Phone data={el} />;
            })}
          </PhoneWrapper>
        </Main> */}
          {/* mockup data */}
          {/* <CustomerReviewBox>
          <CustomerReivewText>고객후기</CustomerReivewText>
        </CustomerReviewBox> */}
          <StartTalkBox />
        </Wrapper>
      </Fade>
    </>
  );
};

export default withRouter(Front);
