import React, { useEffect, useState } from "react";
import styled from "styled-components";

import { GetBoardList, GetBoardrecList, GetTermList } from "Datas/swr";
import { Link } from "react-router-dom";
import { deleteBoardRec, deletePost, DestroyTerms } from "Datas/api";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";
import Select from "react-select";
import { useConfirm } from "Settings/util";

const Wrapper = styled.div``;

const TopLabel = styled.div``;

const Top = styled.div`
  border: 1px solid #eee;
  height: 70px;
  display: flex;
  align-items: center;
  padding: 0 50px 0 20px;
`;

const TopButton = styled.div`
  display: flex;
  margin-left: auto;
`;

const Add = styled.div`
  display: flex;
  background: #6091ed;
  padding: 10px 40px;
  color: #fff;
  margin-right: 20px;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const Delete = styled.div`
  display: flex;
  background: #ed6060;
  padding: 10px 40px;
  color: #fff;
  border-radius: 10px;
  :hover {
    opacity: 0.5;
  }
  cursor: pointer;
`;

const DataTable = styled.table`
  width: 100%;
  margin-top: 30px;

  thead tr th {
    font-weight: 400;
    border-bottom: 2px solid #eee;
    padding: 20px 0;
  }
  tbody tr td input {
    -webkit-appearance: auto;
  }
  tbody tr td {
    padding: 20px 0;
    text-align: center;
  }
`;

const HoverTag = styled.td`
  border-bottom: 1.5px solid #eee;
  &:hover {
    opacity: 0.5;
  }
`;

const LinedTag = styled.td`
  border-bottom: 1.5px solid #eee;
`;

const Imgskeleton = styled.img`
  max-width: 200px;
  height: 100px;
  border: none;
  resize: both;
  margin: 0 auto;
  border-radius: 5px;
  object-fit: contain;
`;

const Terms = () => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  const TermsType = [
    {
      value: 0,
      label: "회원가입",
    },
    { value: 1, label: "개통신청" },
  ];
  //SWR
  const [terms, setTerms] = useState({ value: false, label: "회원가입" });

  const { TermListData, TermListMutate, isLoading } = GetTermList(
    offset,
    terms.value
  );

  const [BoardData, setBoardData] = useState([]);
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    let checkd = BoardData.every((item) => {
      return item.validate;
    });
    setChecked(checkd);
  }, [BoardData]);

  const _handleAllSelct = () => {
    //전체샐렉트동기화

    let data = [...BoardData];
    let resdata;
    if (checked) {
      resdata = data.map((item) => {
        return { ...item, validate: false };
      });
    } else {
      resdata = data.map((item) => {
        return { ...item, validate: true };
      });
    }

    setBoardData(resdata);
    setChecked(true);
  };

  useEffect(() => {
    if (!isLoading) {
      setBoardData(TermListData?.rows);
    }
  }, [TermListData]);

  const _handleSelect = (id) => {
    let temp = [...BoardData];
    BoardData?.map((el, idx) => {
      if (el?.id === id) {
        let data = {
          ...el,
          validate: !el?.validate,
        };
        temp[idx] = data;
        console.log(data);
      }
    });
    setBoardData(temp);
  };
  const desk = useConfirm(
    "삭제하시겠습니까?",
    async () => {
      let bodys = [];
      for (let index = 0; index < BoardData.length; index++) {
        if (BoardData[index]?.validate) {
          let id = BoardData[index]?.id;
          bodys.push(id);
        }
      }

      await DestroyTerms(bodys);
      await TermListMutate();
    },
    () => {
      alert("삭제 를 취소하였습니다.");
    }
  );
  const _handleDelete = async () => {
    await desk();
  };

  return (
    <Fade Button>
      <Wrapper>
        <Top>
          <TopLabel>이용약관 관리</TopLabel>
          <div style={{ width: 150, marginLeft: 12 }}>
            <Select
              onChange={(e) => {
                setTerms(e);
              }}
              value={terms}
              options={TermsType}
            />
          </div>
          <TopButton>
            <Link to="/admin/terms/detail/add">
              <Add>추가</Add>
            </Link>
            <Delete onClick={_handleDelete}>삭제</Delete>
          </TopButton>
        </Top>
        <DataTable>
          <thead>
            <tr>
              <LinedTag style={{ width: 80, textAlign: "center" }}>
                <input
                  type="checkbox"
                  checked={checked}
                  onClick={() => {
                    _handleAllSelct();
                  }}
                  style={{ WebkitAppearance: "auto" }}
                />
              </LinedTag>
              <th>제목</th>
              <th>순서</th>
              <th>분류</th>
              <th>더보기</th>
            </tr>
          </thead>
          <tbody>
            {BoardData?.length !== 0 &&
              BoardData?.map((el, idx) => {
                return (
                  <tr>
                    <LinedTag>
                      <input
                        checked={el?.validate}
                        type="checkbox"
                        onClick={() => _handleSelect(el?.id)}
                      />
                    </LinedTag>
                    <LinedTag>{el?.title}</LinedTag>
                    <LinedTag>{el?.order}</LinedTag>
                    <LinedTag>
                      {el?.termstype === false ? "이용약관" : "개통관리"}
                    </LinedTag>
                    <HoverTag>
                      <Link
                        to={{
                          pathname: `/admin/terms/detail/${el?.id}`,
                          state: el,
                        }}
                      >
                        수정
                      </Link>
                    </HoverTag>
                  </tr>
                );
              })}
          </tbody>
        </DataTable>
        <CustomPagination
          data={TermListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
};

export default Terms;
