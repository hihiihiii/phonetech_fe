import React, { Component, useState, useEffect } from "react";
import styled from "styled-components";

import { getStoarge, setStoarge, useAuth } from "Datas/storage";
import { CheckPhone, validatePassword } from "Settings/common";
import { Redirect, withRouter } from "react-router";
import { LoginUser } from "Datas/api";
import { Link } from "react-router-dom";
import { Fade } from "react-reveal";
//import { GetFingerPrint } from "Settings/fingerprint";
import FingerprintJS from "@fingerprintjs/fingerprintjs";
import { GetInforMation } from "Datas/swr";

const Wrapper = styled.div``;

const Inner = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 570px;
  padding: 0 20px;
`;

const LoginText = styled.div`
  font-size: 35px;
  @media screen and (max-width: 450px) {
    font-size: 25px;
  }
  margin-bottom: 15px;
  font-weight: 700;
`;

const CredentialsInput = styled.div`
  width: 100%;
  margin-top: 15px;
`;

const CredentialsInputText = styled.div`
  font-size: 25px;
  color: #838383;

  @media screen and (max-width: 450px) {
    font-size: 17px;
  }
`;

const CredentialsInputInput = styled.input`
  width: 100%;
  margin-top: 5px;
  height: 65px;
  border: 2px solid #f0f0f0;
  border-radius: 10px;
  color: #000;
  font-size: 16px;
  padding: 10px;

  @media screen and (max-width: 450px) {
    height: 45px;
  }
`;

const LoginBtn = styled.div`
  width: 100%;
  cursor: pointer;
  height: 65px;
  color: #fff;
  background: #feb43c;
  border: 1px solid #feb43c;
  border-radius: 10px;
  margin-top: 15px;
  :hover {
    opacity: 0.5;
  }
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 25px;
  @media screen and (max-width: 450px) {
    height: 45px;
    font-size: 18px;
  }
`;

const SignupBtn = styled.div`
  width: 100%;
  height: 65px;
  color: #feb43c;
  :hover {
    opacity: 0.5;
  }
  border: 1px solid #feb43c;
  cursor: pointer;
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 15px;
  font-size: 25px;
  @media screen and (max-width: 450px) {
    height: 45px;
    font-size: 18px;
  }
`;

const FindIdPW = styled.div`
  font-size: 17px;
  color: #feb43c;
  text-align: center;
  cursor: pointer;
  margin-top: 10px;
  @media screen and (max-width: 450px) {
    height: 45px;
    font-size: 15px;
  }
`;

const TintBox = styled.p`
  color: red;
  margin-top: 12px;
`;
const MainLogo = styled.img`
  max-width: 278px;
  max-height: 57px;
  @media screen and (max-width: 1300px) {
    max-width: 238px;
    max-height: 43px;
  }
  @media screen and (max-width: 600px) {
    max-width: 175px;
    max-height: 35px;
  }
`;

//셋로그인 진행해주기
const Login = ({ history }) => {
  const { InfoData, InfoDataMutate } = GetInforMation();
  const { setLogin } = useAuth();
  //const fingerPrint = GetFingerPrint();
  const fpPromise = FingerprintJS.load();

  const [data, setData] = useState({
    email: "",
    password: "",
    finger: "",
    fcm: "",
  });

  //로그인날려주기
  const _handleLogin = async () => {
    const fp = await fpPromise;
    let fcm = await getStoarge("fcmToken");
    fcm = await JSON.parse(fcm);
    const result = await fp.get();
    const visitorId = result.visitorId;
    data.finger = visitorId;
    data.fcm = fcm?.fcmToken;

    if (!CheckPhone || data.password === "") {
      alert("아이디나 패스워드를 확인해주세요");
      return;
    } else {
      let res = await LoginUser(data, "user");
      console.log(res);
      if (!res) {
        return;
      } else {
        if (res?.data?.status == 400) {
          alert(res.data.message);
          return;
        }
        if (res?.data?.status == 403) {
          history.push(`/resetFinger/${res?.data?.id}`);
          alert(res.data.message);
          return;
        }
        await setStoarge("jwt", res?.data?.jwt); //로그인후 저장 메인이동
        await setLogin(res.data);

        history.push("/");
      }
    }
  };

  return (
    <Wrapper>
      <Inner>
        <MainLogo
          src={InfoData !== undefined && InfoData[0].image}
          style={{ marginBottom: 24 }}
        />
        <LoginText>로그인</LoginText>
        <CredentialsInput>
          <CredentialsInputText>아이디</CredentialsInputText>
          <CredentialsInputInput
            value={data.email}
            onChange={(e) => setData({ ...data, email: e.target.value })}
            placeholder="아이디를 입력해주세요"
          ></CredentialsInputInput>
        </CredentialsInput>
        {!CheckPhone(data.email) && (
          <TintBox>휴대폰 번호 형식이 아닙니다.</TintBox>
        )}
        <CredentialsInput>
          <CredentialsInputText>비밀번호</CredentialsInputText>
          <CredentialsInputInput
            value={data.password}
            type="password"
            onChange={(e) => setData({ ...data, password: e.target.value })}
            placeholder="비밀번호를 입력해주세요"
          ></CredentialsInputInput>
          <TintBox>{validatePassword(data.password)}</TintBox>
        </CredentialsInput>
        <LoginBtn onClick={() => _handleLogin()}>로그인</LoginBtn>
        <Link to="/register">
          <SignupBtn>회원가입</SignupBtn>
        </Link>
        <Link to="/findpw">
          <FindIdPW>비밀번호 찾기</FindIdPW>
        </Link>
      </Inner>
    </Wrapper>
  );
};

export default withRouter(Login);
