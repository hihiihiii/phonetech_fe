import { SuccessIcon } from "assets";
import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const breakpoints = [450, 768, 992, 1200]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const Wrapper = styled.div``;

const SuccessImage = styled.img`
  width: 550px;
  resize: both;
  object-fit: contain;

  @media screen and (max-width: 450px) {
    width: 330px;
  }
`;

const SuccessTextBox = styled.div`
  font-size: 32px;
  font-weight: 500;

  ${mq[0]} {
    font-size: 26px;
  }
  ${mq[1]} {
    font-size: 28px;
  }
`;

const SuccessTextDesc = styled.div`
  text-align: center;
  margin-top: 24px;
  word-break: keep-all;
`;

const Success = () => {
  return (
    <Wrapper
      style={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <SuccessTextBox>개통신청 완료!</SuccessTextBox>
      <SuccessTextDesc>
        수정사항 및 개통 취소는 카카오톡 고객센터 <br /> 및 전화 부탁드려요
        [개통 완료 페이지]
      </SuccessTextDesc>

      <SuccessImage src={SuccessIcon} />
      <div style={{ display: "flex", alignItems: "center" }}>
        <div
          style={{
            padding: "8px 16px",
            borderRadius: 100,
            backgroundColor: "orange",
            color: "#fff",
            marginRight: 25,
          }}
        >
          <Link to="/">홈으로 이동하기</Link>
        </div>
        <div
          style={{
            padding: "8px 16px",
            borderRadius: 100,
            backgroundColor: "orange",
            color: "#fff",
          }}
        >
          <Link to="/contractList">주문내역 조회하기</Link>
        </div>
      </div>
    </Wrapper>
  );
};

export default Success;
