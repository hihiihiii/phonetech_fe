import React, { useState } from "react";
import styled from "styled-components";
import Phone from "Components/Front/Phone.js";

import KakaoIcon from "kakaotalk.png";
import CallIconPng from "call.png";
import { phoneProducts } from "Settings/mockData";
import { GetproductList, GetPromotionList } from "Datas/swr";
import { withRouter } from "react-router";
import mockBanner from "../../mockBanner.png";
import Sliders from "Components/Sliders";
import { Fade } from "react-reveal";
import CustomPagination from "Settings/pagination";

const Wrapper = styled.div``;

const Leaderboard = styled.img`
  width: 100%;
  margin-top: 100px;
  height: 550px;
  background: #f0f0f0;

  @media screen and (max-width: 768px) {
    height: 320px;
  }

  @media screen and (max-width: 375px) {
    height: 207px;
  }
`;

const Main = styled.div`
  display: flex;
  max-width: 1610px;
  padding: 0 10px;
  margin: 0 auto;
  width: 100%;
  flex-direction: column;
`;

const PhoneTechTop6 = styled.div`
  display: flex;
  color: #feb43c;
  font-size: 25px;

  margin-top: 96px;

  @media screen and (max-width: 768px) {
    font-size: 22px;
  }

  @media screen and (max-width: 375px) {
    font-size: 18px;
  }
`;

const WhichPhoneHot = styled.div`
  display: flex;
  font-size: 35px;
  font-weight: 600;

  width: fit-content;
  box-shadow: inset 0 -8px 0 #ffcea3;
  @media screen and (max-width: 768px) {
    font-size: 26px;
    font-weight: 600;
    margin-left: 2%;
  }
  @media screen and (max-width: 375px) {
    font-size: 20px;
    font-weight: 600;
    margin-left: 2%;
  }
`;

const PhoneWrapper = styled.div`
  display: flex;
  //justify-content: space-between;
  flex-wrap: wrap;
  position: relative;
`;

const StartTalk = styled.div`
  position: absolute;
  right: 0px;
  top: 50px;
`;

const KakaoTalk = styled.div`
  display: flex;
  width: 256px;
  height: 60px;
  align-items: center;
  background: #f5f5f5;
  border-radius: 30px;
  & > div {
    margin-left: 15px;
  }
  font-size: 20px;
  @media screen and (max-width: 450px) {
    width: 105px;
    height: 30px;
    font-size: 12px;
  }
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
`;

const KakaoTalkIcon = styled.div`
  height: 60px;
  width: 60px;
  margin-left: auto !important;
  border-radius: 100%;

  @media screen and (max-width: 450px) {
    height: 30px;
    width: 30px;
  }

  & > img {
    width: 100%;
    height: 100%;
    border-radius: 100%;
  }
`;

const CallTalk = styled.div`
  display: flex;
  width: 256px;
  height: 60px;
  align-items: center;
  margin-top: 15px;
  background: #f5f5f5;
  border-radius: 30px;
  & > div {
    margin-left: 15px;
  }
  font-size: 20px;
  @media screen and (max-width: 450px) {
    width: 105px;
    height: 30px;
    font-size: 12px;
  }
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
`;

const CallIcon = styled.div`
  height: 60px;
  width: 60px;
  margin-left: auto !important;
  border-radius: 100%;

  @media screen and (max-width: 450px) {
    height: 30px;
    width: 30px;
  }

  & > img {
    width: 100%;
    height: 100%;
    border-radius: 100%;
  }
`;

const PhoneOptionCircle = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${(props) => (props.selected === true ? "#feb43c" : "#f0f0f0")};
  color: ${(props) => (props.selected === true ? "#fff" : "333")};
  padding: 7px 21px;
  font-weight: 500;
  margin-right: 20px;
  font-size: 20px;
  cursor: pointer;
  border-radius: 30px;

  @media screen and (max-width: 768px) {
    padding: 7px 16px;
    font-size: 14px;
  }
`;

const OptionWrapper = styled.div`
  display: flex;
  margin: 55px 0;
`;

const TitleFlexBox = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const AlgnBox = styled.div`
  margin-top: 50px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 120px;
  height: 35px;
`;

const Algn = styled.div`
  width: 55px;
  height: 33px;
  border-radius: 8px;
  background-color: orange;
  color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

const Category = ({ match }) => {
  const [offset, setOffset] = useState(0);
  const [page, setPage] = useState([]);
  const [filtervalue, setFilterValue] = useState(1);
  const { PromotionListData } = GetPromotionList(10);
  const { ProductListData } = GetproductList(
    "category",
    0,
    match?.params?.id,
    offset,
    filtervalue
  );

  console.log("ProductListData");
  console.log(ProductListData);
  console.log(PromotionListData);

  /* if (match.params.type) { */
  return (
    <Fade Button>
      <Wrapper>
        <Sliders />

        <Main style={{ marginTop: 50 }}>
          {/* <OptionWrapper>
          <PhoneOptionCircle selected={false}>SAMSUNG</PhoneOptionCircle>
          <PhoneOptionCircle selected={true}>APPLE</PhoneOptionCircle>
          <PhoneOptionCircle selected={false}>LG</PhoneOptionCircle>
        </OptionWrapper> */}
          {/* <PhoneTechTop6>폰테크 애플 상품</PhoneTechTop6> */}
          {/* {match?.params?.type === "one" && (
          <WhichPhoneHot>번호이동 상품</WhichPhoneHot>
        )}
        {match?.params?.type === "two" && (
          <WhichPhoneHot>기기변경 상품</WhichPhoneHot>
        )}
        {match?.params?.type === "three" && (
          <WhichPhoneHot>증정 사은품</WhichPhoneHot>
        )} */}

          <TitleFlexBox>
            <WhichPhoneHot>{match?.params?.type}</WhichPhoneHot>
            <AlgnBox>
              <Algn
                onClick={() => {
                  setFilterValue(0);
                }}
              >
                낮은순
              </Algn>
              <Algn
                onClick={() => {
                  setFilterValue(1);
                }}
              >
                높은순
              </Algn>
            </AlgnBox>
          </TitleFlexBox>
          <PhoneWrapper>
            {/* {PromotionListData !== undefined &&
              PromotionListData?.length !== 0 &&
              PromotionListData?.map((el, idx) => {
                if (!el?.type) {
                  return;
                }
                return (
                  <Main>
                    <PhoneWrapper style={{ marginTop: 50 }}>
                      {phoneProducts.map((el, idx) => {
                        return <Phone data={el} />;
                      })}
                    </PhoneWrapper>
                  </Main>
                );
              })} */}
            {/* mockup data */}
            <Main>
              <PhoneWrapper style={{ marginTop: 50 }}>
                {ProductListData?.rows?.map((el, idx) => {
                  console.log(el);
                  return <Phone data={el} />;
                })}
              </PhoneWrapper>
            </Main>
            {/* mockup data */}
            {/* <Phone />
              <Phone />
              <Phone />
              <Phone /> */}
            {/* <StartTalk>
            <KakaoTalk>
              <div>카카오톡상담</div>
              <KakaoTalkIcon>
                <img alt="" src={KakaoIcon} />
              </KakaoTalkIcon>
            </KakaoTalk>
            <CallTalk>
              <div>전화 상담</div>
              <CallIcon>
                <img alt="" src={CallIconPng} />
              </CallIcon>
            </CallTalk>
          </StartTalk> */}
          </PhoneWrapper>
        </Main>
        <CustomPagination
          data={ProductListData}
          setOffset={setOffset}
          page={page}
          setPage={setPage}
        />
      </Wrapper>
    </Fade>
  );
  /* } else {
    return (
      <Wrapper>
     
        <Leaderboard />
        <Main>
          <PhoneTechTop6>폰테크 애플 상품</PhoneTechTop6>
          <WhichPhoneHot>Apple 아이폰</WhichPhoneHot>
          <PhoneWrapper>
            <Phone />
            <Phone />
            <Phone />
            <Phone />
            <StartTalk>
              <KakaoTalk>
                <div>카카오톡상담</div>
                <KakaoTalkIcon>
                  <img alt="" src={KakaoIcon} />
                </KakaoTalkIcon>
              </KakaoTalk>
              <CallTalk>
                <div>전화 상담</div>
                <CallIcon>
                  <img alt="" src={CallIconPng} />
                </CallIcon>
              </CallTalk>
            </StartTalk>
          </PhoneWrapper>
        </Main>
      </Wrapper>
    );
  } */
};

export default withRouter(Category);
