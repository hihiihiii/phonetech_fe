import HomeIcon from "./files/home.png";
import OutIcon from "./files/out.png";
import SuccessIcon from "./files/success.png";
import StarIcon from "./files/star.png";
import Arrow_Bottom from "./files/arrow_bottom.png";
import Arrow_Up from "./files/arrow_up.png";

export { HomeIcon, OutIcon, SuccessIcon, StarIcon, Arrow_Bottom, Arrow_Up };
